const _ = require('lodash');

const formated = (value, cifrao) => {
  const currency = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
  });

  const c = currency.format(value);

  if (cifrao) {
    return value > 0 ? c : `-${c}`;
  }

  const str = c.split('$');

  return value > 0 ? str[1] : `-${str[1]}`;
};

const formatedDecimal = (value) => {
  const currency = new Intl.NumberFormat('pt-BR', {
    style: 'decimal',
  });

  return currency.format(value);
};

const test = async (props) => {
  const value = props;
  const containVirgula = /([,])/g;
  const c = containVirgula.test(props);
  if (c) {
    const decimal = parseFloat(
      value.replace('.', '').replace(',', '.')
    ).toFixed(2);
    return decimal;
  }
  const decimal = parseFloat(value).toFixed(2);
  return decimal;
};

const main = async () => { };

main();
