import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useUser } from '../global';

const RouterWrapper = ({ component: Component, isPrivate, ...rest }) => {
  const { user } = useUser();
  // const user = true;
  if (!user && isPrivate) {
    return <Redirect to="/" />;
  }

  // REMEMBER:  default /companie/list
  // /withdrawl/create
  if (user && !isPrivate) {
    return <Redirect to="/companie/list" />;
  }

  return <Route {...rest} component={Component} />;
};

export default RouterWrapper;
