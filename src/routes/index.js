import React from 'react';
import { Switch } from 'react-router-dom';

import AuthRoutes from '../modules/Auth/routes';
import CompaniesRoutes from '../modules/Companies/routes';
import CollaboratorsRoutes from '../modules/Collaborators/routes';
import ContractRoutes from '../modules/Contracts/routes';
import PositionRoutes from '../modules/Positions/routes';
import RepactuationRoutes from '../modules/Repactuation/routes';
import DepositRoutes from '../modules/Deposits/routes';
import WithdrawalsRoutes from '../modules/Withdrawals/routes';
import MovimentsRoutes from '../modules/Moviments/routes';
import SettingsRoutes from '../modules/Settings/routes';

const Routes = () => {
  return (
    <Switch>
      <>
        <AuthRoutes />
        <CompaniesRoutes />
        <CollaboratorsRoutes />
        <ContractRoutes />
        <PositionRoutes />
        <RepactuationRoutes />
        <DepositRoutes />
        <WithdrawalsRoutes />
        <MovimentsRoutes />
        <SettingsRoutes />
      </>
    </Switch>
  );
};

export default Routes;
