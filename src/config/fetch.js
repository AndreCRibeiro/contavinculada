import api from '../services/api';

export const fetcher = (url) => api.get(url).then((res) => res.data);
