/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, color, border, variant, layout } from 'styled-system';
import * as Atom from '../../Atoms';

const Button = styled.button.attrs((props) => ({
  type: props.disableSubmit ? 'button' : 'submit',
}))`
  ${space}
  ${color}
  ${border}
  ${layout}
  ${variant({
    variants: {
      delete: {
        bg: 'RED',
      },
      outline: {
        border: '1px solid',
        borderColor: 'PRIMARY',
        bg: 'WHITE',
      },
      outlineModal: {
        border: '1px solid',
        borderColor: 'PRIMARY',
        bg: 'WHITE',
        width: 280,
      },
      blue: {
        bg: 'SECONDARY',
        height: 60,
        flex: 1,
      },
      blueModal: {
        bg: 'SECONDARY',
        flexDirection: 'column',
        width: 280,
      },
      black: {
        bg: 'PRIMARY',
      },
      blackModal: {
        bg: 'PRIMARY',
        width: 280,
      },
      login: {
        width: 351,
      },
    },
  })}
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

Button.defaultProps = {
  bg: 'SECONDARY',
  height: 40,
  borderRadius: 10,
  border: '0px solid',
  px: 'MEDIUM',
};

export default ({ text, onClick, children, secondaryText, ...props }) => {
  return (
    <Button {...props} onClick={onClick}>
      <Atom.Text
        variant={
          props.variant === 'outlineModal' || props.variant === 'outline'
            ? 'buttonOutline'
            : 'button'
        }
      >
        {text}
      </Atom.Text>
      {secondaryText && (
        <Atom.Text variant="secondary">{secondaryText}</Atom.Text>
      )}
    </Button>
  );
};
