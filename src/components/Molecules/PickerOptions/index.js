/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

// import styled from 'styled-components';
// import { color, border, layout, space, typography } from 'styled-system';
import * as Atom from '../../Atoms';

export default ({
  name,
  error,
  group,
  label,
  value,
  showId,
  options,
  register,
  onChange,
  placeholderDependency,
  list,
  teste,
}) => (
  <Atom.Container flexDirection="column" width={list ? '25%' : ''}>
    <Atom.PickerOptions
      mt="XXSMALL"
      name={name}
      showId={showId}
      options={options}
      register={register}
      selected={value}
      placeholderDependency={placeholderDependency}
      teste={teste}
      onChange={onChange}
    />
    {error && (
      <Atom.Text variant="error" ml="XSMALL">
        {error}
      </Atom.Text>
    )}
  </Atom.Container>
);
