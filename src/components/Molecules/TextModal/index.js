/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { color, border, layout, space } from 'styled-system';
import * as Atom from '../../Atoms';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  justify-content: center;
  width: 100%;
`;

Container.defaultProps = {
  bg: 'WHITE',
  pl: 'XXSMALL',
  pt: 'XXSMALL',
};

export default ({
  label,
  percent,
  infoText,
  labelProps,
  infoProps,
  center,
}) => (
  <Atom.Container
    flexDirection="column"
    width={`${percent}%`}
    {...(center
      ? {
        justifyContent: 'center',
        alignItems: 'center',
      }
      : {})}
  >
    <Atom.Container alignItems="flex-end">
      <Atom.Text variant="modalLabelText" ml="XXSMALL" {...labelProps}>
        {label}
      </Atom.Text>
    </Atom.Container>
    <Container>
      <Atom.Text
        variant="modalText"
        textAlign={center ? 'center' : 'left'}
        {...infoProps}
      >
        {infoText}
      </Atom.Text>
    </Container>
  </Atom.Container>
);
