/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

// import styled from 'styled-components';
// import { color, border, layout, space, typography } from 'styled-system';
import * as Atom from '../../Atoms';

export default ({
  name,
  error,
  group,
  label,
  value,
  showId,
  options,
  percent,
  obsLabel,
  register,
  onChange,
  placeholderDependency,
}) => (
  <Atom.Container flexDirection="column" width={`${percent}%`}>
    <Atom.Container alignItems="flex-end">
      <Atom.Text ml="XXSMALL">{label}</Atom.Text>
      {obsLabel && (
        <Atom.Text ml="XXSMALL" variant="details" mb="2px">
          {obsLabel}
        </Atom.Text>
      )}
    </Atom.Container>
    {group ? (
      <Atom.PickerGroup
        mt="XXSMALL"
        options={options}
        selected={value}
        onChange={onChange}
        placeholderDependency={placeholderDependency}
      />
    ) : (
      <Atom.PickerItem
        mt="XXSMALL"
        name={name}
        showId={showId}
        options={options}
        register={register}
        selected={value}
        placeholderDependency={placeholderDependency}
      />
    )}
    {error && (
      <Atom.Text variant="error" ml="XSMALL">
        {error}
      </Atom.Text>
    )}
  </Atom.Container>
);
