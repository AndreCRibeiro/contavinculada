import React from 'react';
import * as Atom from '../../Atoms';

export default ({ active, label }) => (
  <Atom.Container flexDirection="column">
    {label && (
      <Atom.Text variant="modalLabelText" ml="XXSMALL">
        {label}
      </Atom.Text>
    )}
    <Atom.Container
      variant="status"
      bg={active ? 'GREEN_TRANSPARENT' : 'RED_TRANSPARENTE'}
      width="84px"
      mt={label ? 'XXSMALL' : '0'}
    >
      <Atom.Text variant={active ? 'active' : 'noActive'}>
        {active ? 'ATIVO' : 'INATIVO'}
      </Atom.Text>
    </Atom.Container>
  </Atom.Container>
);
