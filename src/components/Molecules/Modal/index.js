/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import * as Atom from '../../Atoms';
import * as Molecules from '..';

export default ({
  title,
  subTitle,
  userModal,
  img,
  show,
  list,
  variant,
  alert,
  confirm,
  textBlueButton,
  secondaryTextBlueButton,
  handleCloseModal,
  handleNavigate,
  handleNavigateToList,
  handleDelete,
  blackButton,
  textBlackButton,
  textBack,
  threeLinesModal,
  contractModal,
  depositModal,
  positionModal,
  collaboratorsModal,
  textTitle1,
  text1,
  textTitle2,
  text2,
  textTitle3,
  text3,
  textTitle4,
  text4,
  textTitle5,
  text5,
  textTitle6,
  text6,
  textTitle7,
  text7,
  textTitle8,
  text8,
  textTitle9,
  text9,
  textTitle10,
  text10,
  textTitle11,
  text11,
  textTitle12,
  text12,
  textTitle13,
  text13,
  textTitle14,
  text14,
  textTitle15,
  text15,
  textTitle16,
  text16,
  textTitle17,
  text17,
  textTitle18,
  text18,
  textTitle19,
  text19,
  textTitle20,
  text20,
  textTitle21,
  text21,
  textTitle22,
  text22,
}) => {
  return (
    <Atom.Modal show={show} list={list}>
      <Atom.Box variant={variant}>
        {img && <Atom.Image img={img} />}
        <Atom.Text variant="modal" mb={confirm ? 'XXSMALL' : '0px'}>
          {title}
        </Atom.Text>
        {subTitle && <Atom.Text mt="XXSMALL">{subTitle}</Atom.Text>}
        {alert && (
          <Atom.Flex>
            <Molecules.Button
              mb="XXSMALL"
              mr="XXSMALL"
              variant="outline"
              text="CANCELAR"
              onClick={handleCloseModal}
              expand={false}
            />

            <Molecules.Button
              mb="XXSMALL"
              ml="XXSMALL"
              variant="delete"
              text="DELETAR"
              onClick={handleDelete}
            />
          </Atom.Flex>
        )}
        {confirm && (
          <Atom.Flex variant="modalConfirm">
            <Molecules.Button
              mb="XXSMALL"
              variant="blueModal"
              text={textBlueButton}
              secondaryText={secondaryTextBlueButton}
              onClick={handleNavigate}
            />
            {blackButton && (
              <Molecules.Button
                mb="XXSMALL"
                variant="blackModal"
                text={textBlackButton}
                onClick={handleCloseModal}
              />
            )}

            <Molecules.Button
              mb="XXSMALL"
              variant="outlineModal"
              text={textBack}
              onClick={handleNavigateToList}
            />
          </Atom.Flex>
        )}
        {userModal && (
          <>
            <Atom.Container variant="lineCreate" mt="50px">
              <Molecules.TextModal
                center
                label={textTitle1}
                infoText={text1}
                percent={50}
              />
              <Molecules.TextModal
                center
                label={textTitle2}
                infoText={text2}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                center
                label={textTitle3}
                infoText={text3}
                percent={50}
              />
              <Molecules.TextModal
                center
                label={textTitle4}
                infoText={text4}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate" mt="20px">
              <Molecules.TextModal
                center
                labelProps={{
                  fontSize: 3,
                }}
                infoProps={{
                  fontSize: 2,
                  color: 'SECONDARY',
                }}
                label={textTitle5}
                infoText={text5}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate" mt="20px">
              <Molecules.Button
                mt="XSMALL"
                text="FECHAR"
                onClick={handleCloseModal}
              />
            </Atom.Container>
          </>
        )}
        {threeLinesModal && (
          <>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle1}
                infoText={text1}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle2}
                infoText={text2}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle3}
                infoText={text3}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle4}
                infoText={text4}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle5}
                infoText={text5}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle6}
                infoText={text6}
                percent={50}
              />
              <Molecules.TextModal
                modal
                label={textTitle7}
                infoText={text7}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                modal
                label={textTitle8}
                infoText={text8}
                percent={100}
              />
              <Molecules.TextModal
                modal
                label={textTitle9}
                infoText={text9}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                modal
                label={textTitle10}
                infoText={text10}
                percent={100}
              />
              <Molecules.TextModal
                modal
                label={textTitle11}
                infoText={text11}
                percent={100}
              />
            </Atom.Container>
            <Molecules.Button
              mt="XSMALL"
              text="FECHAR"
              onClick={handleCloseModal}
            />
          </>
        )}
        {contractModal && (
          <>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle1}
                infoText={text1}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle2}
                infoText={text2}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle3}
                infoText={text3}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle4}
                infoText={text4}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle5}
                infoText={text5}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle6}
                infoText={text6}
                percent={50}
              />
              <Molecules.TextModal
                modal
                label={textTitle7}
                infoText={text7}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                modal
                label={textTitle8}
                infoText={text8}
                percent={100}
              />
              <Molecules.TextModal
                modal
                label={textTitle9}
                infoText={text9}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                modal
                label={textTitle12}
                infoText={text12}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                modal
                label={textTitle10}
                infoText={text10}
                percent={100}
              />
              <Molecules.TextModal
                modal
                label={textTitle11}
                infoText={text11}
                percent={100}
              />
            </Atom.Container>
            <Molecules.Button
              mt="XSMALL"
              text="FECHAR"
              onClick={handleCloseModal}
            />
          </>
        )}
        {depositModal && (
          <>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle1}
                infoText={text1}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle2}
                infoText={text2}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle3}
                infoText={text3}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle4}
                infoText={text4}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle5}
                infoText={text5}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle6}
                infoText={text6}
                percent={50}
              />
              <Molecules.TextModal
                modal
                label={textTitle7}
                infoText={text7}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                modal
                label={textTitle8}
                infoText={text8}
                percent={100}
              />
              <Molecules.TextModal
                modal
                label={textTitle9}
                infoText={text9}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                modal
                label={textTitle10}
                infoText={text10}
                percent={100}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.StatusTag
                label={textTitle11}
                active={text11 === true}
              />
            </Atom.Container>
            <Molecules.Button
              mt="XSMALL"
              text="FECHAR"
              onClick={handleCloseModal}
            />
          </>
        )}
        {positionModal && (
          <>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle1}
                infoText={text1}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle2}
                infoText={text2}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle3}
                infoText={text3}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle4}
                infoText={text4}
                percent={50}
              />
            </Atom.Container>
            <Molecules.Button
              mt="XSMALL"
              text="FECHAR"
              onClick={handleCloseModal}
            />
          </>
        )}
        {collaboratorsModal && (
          <>
            <Atom.Container variant="lineCreate">
              <Atom.Avatar
                variant="avatarModal"
                mr="SMALL"
                src="https://images.fastcompany.net/image/upload/w_1280,f_auto,q_auto,fl_lossy/wp-cms/uploads/2019/12/p-1-greta-thunberg-was-just-named-time-magazines-2019-person-of-the-year.jpg"
              />
              <Atom.Container variant="columnCreate">
                <Atom.Container variant="lineCreate">
                  <Molecules.TextModal
                    label={textTitle1}
                    infoText={text1}
                    percent={50}
                  />
                  <Molecules.TextModal
                    label={textTitle2}
                    infoText={text2}
                    percent={50}
                  />
                  <Molecules.TextModal
                    label={textTitle22}
                    infoText={text22}
                    percent={50}
                  />
                </Atom.Container>
                <Atom.Container variant="lineCreate">
                  <Molecules.TextModal
                    label={textTitle3}
                    infoText={text3}
                    percent={50}
                  />
                  <Molecules.TextModal
                    label={textTitle4}
                    infoText={text4}
                    percent={50}
                  />
                  <Molecules.TextModal
                    label={textTitle5}
                    infoText={text5}
                    percent={50}
                  />
                </Atom.Container>
              </Atom.Container>
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle6}
                infoText={text6}
                percent={20}
              />
              <Molecules.TextModal
                label={textTitle7}
                infoText={text7}
                percent={20}
              />
              <Molecules.TextModal
                label={textTitle8}
                infoText={text8}
                percent={20}
              />
              <Molecules.TextModal
                label={textTitle9}
                infoText={text9}
                percent={20}
              />
              <Molecules.TextModal
                label={textTitle10}
                infoText={text10}
                percent={20}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle11}
                infoText={text11}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle12}
                infoText={text12}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle13}
                infoText={text13}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle14}
                infoText={text14}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle15}
                infoText={text15}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle16}
                infoText={text16}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle17}
                infoText={text17}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle18}
                infoText={text18}
                percent={50}
              />
              <Molecules.TextModal
                label={textTitle19}
                infoText={text19}
                percent={50}
              />
            </Atom.Container>
            <Atom.Container variant="dividerModal" />
            <Atom.Container variant="lineCreate">
              <Molecules.StatusTag
                label={textTitle20}
                active={text20 === 'ativo'}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextModal
                label={textTitle21}
                infoText={text21}
                percent={100}
              />
            </Atom.Container>
            <Molecules.Button
              mt="XSMALL"
              text="FECHAR"
              onClick={handleCloseModal}
            />
          </>
        )}
      </Atom.Box>
    </Atom.Modal>
  );
};
