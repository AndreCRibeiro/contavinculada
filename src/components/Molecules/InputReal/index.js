/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import IntlCurrencyInput from 'react-intl-currency-input';
import { space, color, border, layout, typography } from 'styled-system';
import * as Atom from '../../Atoms';

const Input = styled(IntlCurrencyInput)`
  ${space}
  ${color}
  ${border}
  ${layout}
  ${typography}
  align-items: center;
  justify-content: center;
  font-family: 'Montserrat-Regular';
  ::-webkit-input-placeholder{
    color: ${({ theme }) => theme.colors.PLACEHOLDER}
  }`;

Input.defaultProps = {
  width: 300,
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  bg: 'WHITE_ICE',
};

const currencyConfig = {
  locale: 'pt-BR',
  formats: {
    number: {
      BRL: {
        style: 'currency',
        currency: 'BRL',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      },
    },
  },
};

export default ({
  name,
  mask,
  label,
  error,
  percent,
  typeMask,
  register,
  withMask,
  obsLabel,
  multiLine,
  placeholder,
  value,
  onChange,
}) => (
    <Atom.Container flexDirection="column" width={`${percent}%`}>
      <Atom.Container alignItems="flex-end">
        <Atom.Text ml="XXSMALL">{label}</Atom.Text>
        {obsLabel && (
          <Atom.Text ml="XXSMALL" variant="details" mb="2px">
            {obsLabel}
          </Atom.Text>
        )}
      </Atom.Container>
      <Input
        currency="BRL"
        config={currencyConfig}
        name={name}
        ref={register}
        register={register}
        placeholder={placeholder}
        px="XSMALL"
        width="100%"
        mt="XXSMALL"
        fontSize={2}
        multiLine={multiLine}
        height={multiLine && '90px'}
        onChange={onChange}
        value={value}
      />
      {error && (
        <Atom.Text variant="error" ml="XSMALL">
          {error}
        </Atom.Text>
      )}
    </Atom.Container>
  );
