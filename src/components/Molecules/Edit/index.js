import React from 'react';
import styled from 'styled-components';
import { color, border, layout, space } from 'styled-system';
import { formatedFloat } from '../../../utils/index';
import * as Atom from '../../Atoms';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  justify-content: center;
  width: 100%;
`;

Container.defaultProps = {
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  bg: 'WHITE_ICE',
  pl: 'XSMALL',
  pr: 'XSMALL',
  pt: 'XSMALL',
  mt: 'XXSMALL',
};

export default ({ line, column, value, percent, border, onChange, limit }) => {
  const floatValue = formatedFloat(value);

  const block = (!!limit || limit === 0) && floatValue > limit;

  return (
    <Atom.Input
      onKeyDown={(e) => {
        if (e.key === 'Enter') {
          e.preventDefault();
          e.stopPropagation();
        }
      }}
      autofocus
      onChange={(e) => {
        onChange(line, column, e.target.value);
      }}
      value={value || ''}
      textAlign="center"
      width={percent}
      height={32}
      border="1px solid"
      borderRight={(border && '1px solid') || (block && '2px solid')}
      borderColor="white"
      borderRadius={5}
      bg={block ? 'RED_TRANSPARENT2' : 'white'}
      fontWeight={block ? 'bold' : 'normal'}
      color={block ? 'DELETE' : 'black'}
    />
  );
};
