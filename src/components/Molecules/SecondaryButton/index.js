/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import {
  space,
  color,
  border,
  flexbox,
  variant,
  typography,
} from 'styled-system';
import * as Atom from '../../Atoms';

const SecondaryButton = styled.button`
  ${space}
  ${color}
  ${border}
  ${typography}
  ${flexbox}
  ${variant({
    variants: {
      normal: {
        alignItems: 'center',
      },
    },
    forgot: {},
  })}
  align-items: center;
  justify-content: center;
  border: none;
  display: inline-block;
  background-color: inherit;
  width: 340px;
  cursor: pointer;
`;

SecondaryButton.defaultProps = {};

export default ({ normal, text, ...props }) => {
  return (
    <SecondaryButton cancel={normal} {...props}>
      <Atom.Text
        variant={normal ? 'buttonCancel' : 'buttonForget'}
        textAlign={normal ? 'center' : 'end'}
      >
        {text}
      </Atom.Text>
    </SecondaryButton>
  );
};
