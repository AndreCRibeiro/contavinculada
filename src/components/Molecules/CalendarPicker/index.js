import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { color, border, layout, space } from 'styled-system';
import { format } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import * as Atom from '../../Atoms';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  align-items: center;
  justify-content: flex-start;
  padding-left: 5px;
  display: flex;
  width: 100%;
  background: transparent;
  border-radius: 10px;
  border: 1px solid #BDBDBD;
  height: 35px;
  font-family: 'Montserrat-Regular';
  color: 'grey';
  cursor: pointer;
`;

export default ({
  percent,
  label,
  onChange,
  value,
  viewYearCalendar,
  minDate,
  defaultValue,
  spaced,
}) => {
  const [show, toggleShow] = useState(false);
  const [selectDate, setSelectDate] = useState(new Date());

  useEffect(() => {
    if (defaultValue) {
      onChange(defaultValue);
      setSelectDate(defaultValue);
    } else {
      onChange(new Date());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [defaultValue]);

  const handleSelect = (value, _) => {
    toggleShow(false);
    onChange(value);
    setSelectDate(value);
  };

  return (
    <Atom.Container flexDirection="column" width={`${percent}%`}>
      <Atom.Container>
        <Atom.Text ml="XSMALL" mb="XXSMALL" mt={spaced ? 'XSMALL' : ''}>
          {label}
        </Atom.Text>
      </Atom.Container>
      <Atom.Container pl="XXSMALL" pr="XSMALL">
        <Container noSubmit onClick={() => toggleShow(!show)}>
          <Atom.Text>
            {viewYearCalendar
              ? format(selectDate || new Date(), 'yyyy', { locale: ptBR })
              : format(selectDate || new Date(), 'MMMM yyyy', { locale: ptBR })}
          </Atom.Text>
        </Container>
      </Atom.Container>

      {show && (
        <Atom.Calendar
          onChange={handleSelect}
          value={value}
          viewYearCalendar={viewYearCalendar}
          minDate={minDate}
          defaultValue={defaultValue}
        />
      )}
    </Atom.Container>
  );
};
