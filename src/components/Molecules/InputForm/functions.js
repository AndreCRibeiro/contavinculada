const beforeMaskedStateChangeRegistration = ({ nextState }) => {
  const { value } = nextState;
  const [value1, value2] = value.split('/');
  /* if (value1 !== '____' && value2 !== '__') {
    value = `${value1}${value2}`;
  } */
  return {
    ...nextState,
    value,
  };
};

export default {
  registration: beforeMaskedStateChangeRegistration,
};
