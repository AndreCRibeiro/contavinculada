/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import * as Atom from '../../Atoms';

export default ({
  name,
  mask,
  label,
  error,
  percent,
  register,
  withMask,
  obsLabel,
  onChange,
  multiLine,
  placeholder,
  value,
  typeMask,
  ...props
}) => (
  <Atom.Container flexDirection="column" width={`${percent}%`}>
    <Atom.Container alignItems="flex-end">
      <Atom.Text ml="XXSMALL">{label}</Atom.Text>
      {obsLabel && (
        <Atom.Text ml="XXSMALL" variant="details" mb="2px">
          {obsLabel}
        </Atom.Text>
      )}
    </Atom.Container>
    {withMask ? (
      <Atom.InputMask
        name={name}
        register={register}
        px="XSMALL"
        mt="XXSMALL"
        mask={mask}
        width="100%"
        height={multiLine && '90px'}
        fontSize={2}
        onChange={onChange}
        multiLine={multiLine}
        placeholder={placeholder}
        // beforeMaskedStateChange={beforeMask[typeMask]}
      />
    ) : (
      <Atom.Input
        name={name}
        register={register}
        placeholder={placeholder}
        px="XSMALL"
        width="100%"
        mt="XXSMALL"
        fontSize={2}
        multiLine={multiLine}
        height={multiLine && '90px'}
        onChange={onChange}
        {...props}
      />
    )}
    {error && (
      <Atom.Text variant="error" ml="XSMALL">
        {error}
      </Atom.Text>
    )}
  </Atom.Container>
);
