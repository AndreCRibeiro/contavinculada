import React, { useEffect, useState } from 'react';
import { format } from 'date-fns';
import * as Atom from '../../Atoms';
import * as Molecules from '../../Molecules';

const HeaderOptions = ({
  title,
  onClickAdd,
  download,
  openModal,
  onClickExclude,
  showButton,
  infos,
}) => {
  const [params, setParams] = useState();

  useEffect(() => {
    if (infos) setParams(infos[0]);
  }, [infos]);

  return (
    <Atom.Container flexDirection="column" width="100%">
      <Atom.Container
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        width="100%"
        mt="XBIG"
      >
        <Atom.Text variant="title">{title}</Atom.Text>
        {onClickAdd ? (
          <Atom.Container flexDirection="row" alignItems="center">
            <Molecules.Button
              onClick={onClickAdd}
              ml="XSMALL"
              text="ADICIONAR"
            />
          </Atom.Container>
        ) : null}
      </Atom.Container>
      {showButton && (
        <>
          <Atom.Container>
            {/* {<Molecules.Button
            onClick={openModal}
            ml="XSMALL"
            mt="MEDIUM"
            text="NOVO FILTRO"
          />} */}
            <Molecules.Button
              onClick={download}
              ml="XSMALL"
              mt="MEDIUM"
              text="BAIXAR RELATÓRIO"
            />
          </Atom.Container>
          {params?.rubrica === '13salario' && (
            <Atom.Container mt={30} ml={13}>
              <Atom.Text>
                <b>Rúbrica:</b> 13º salário <br />
                <b>Contrato:</b> {params.dadosContrato.numero_contrato} <br />
                <b>Ano:</b>{' '}
                {params?.createdAt || params?.data
                  ? format(new Date(params?.createdAt || params?.data), 'yyyy')
                  : params?.ano_retirada}
              </Atom.Text>
            </Atom.Container>
          )}
          {params?.rubrica === 'rescisao' && (
            <Atom.Container mt={30} ml={13}>
              <Atom.Text>
                <b>Rúbrica:</b> Rescisão <br />
                <b>Contrato:</b> {params.dadosContrato.numero_contrato} <br />
              </Atom.Text>
            </Atom.Container>
          )}
          {params?.rubrica === 'ferias' && (
            <Atom.Container mt={30} ml={13}>
              <Atom.Text>
                <b>Rúbrica:</b> Férias <br />
                <b>Contrato:</b> {params.dadosContrato.numero_contrato} <br />
                <b>Ano inicial:</b> {params?.inicio_per_aquisitivo} <br />
                <b>Ano final:</b> {params?.fim_per_aquisitivo}
              </Atom.Text>
            </Atom.Container>
          )}
        </>
      )}
    </Atom.Container>
  );
};

export default HeaderOptions;
