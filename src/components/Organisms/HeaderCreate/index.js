import React from 'react';
import * as Atom from '../../Atoms';
import * as Molecules from '../../Molecules';

const HeaderCreate = ({ title, onClickAdd, onClickCancel }) => {
  return (
    <Atom.Container
      flexDirection="row"
      justifyContent="space-between"
      width="100%"
    >
      <Atom.Text variant="title">{title}</Atom.Text>
      <Atom.Container flexDirection="row" alignItems="center">
        <Molecules.Button
          onClick={onClickCancel}
          variant="outline"
          text="CANCELAR"
        />
        <Molecules.Button
          type="submit"
          onClick={onClickAdd}
          ml="XSMALL"
          text="SALVAR"
        />
      </Atom.Container>
    </Atom.Container>
  );
};

export default HeaderCreate;
