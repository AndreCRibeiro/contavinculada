import React from 'react'; // ui

import * as Atom from '../../Atoms';

const Result = ({ data, label, dontShowLabels }) => {
  return (
    <>
      <Atom.Container justifyContent="space-between" width="100%" mb={2}>
        {!dontShowLabels && (
          <Atom.Text width="15%" mr="SMALL" variant="labelTable">
            Rubricas
          </Atom.Text>
        )}
        <Atom.Container
          flexDirection="row"
          width={dontShowLabels ? '100%' : '85%'}
          alignItems="center"
          justifyContent="space-between"
          px={dontShowLabels ? 'XBIG' : 'SMALL'}
        >
          {label?.map((text) => (
            <Atom.Text mr="SMALL" variant="labelTable">
              {text}
            </Atom.Text>
          ))}
        </Atom.Container>
      </Atom.Container>
      <Atom.Container variant="dividertable" />

      {data.map((line) => (
        <Atom.Container
          flexDirection="row"
          width="100%"
          alignItems="center"
          justifyContent="space-between"
        >
          {!dontShowLabels && (
            <Atom.Text
              variant={line.bold && 'total'}
              width="15%"
              mr="SMALL"
              mb={5}
            >
              {line.title}
            </Atom.Text>
          )}

          <Atom.Line variant="result" width={dontShowLabels ? '100%' : '85%'}>
            {line.value.map((value) => (
              <Atom.Text
                variant={line.bold && 'total'}
                percent={100 / line.length - 1}
              >
                {value}
              </Atom.Text>
            ))}
          </Atom.Line>
        </Atom.Container>
      ))}
    </>
  );
};

export default Result;
