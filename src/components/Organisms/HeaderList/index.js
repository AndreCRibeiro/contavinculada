import React, { useState, useEffect } from 'react';
import * as Atom from '../../Atoms';
import * as Molecules from '../../Molecules';

const HeaderList = ({
  searchLabel,
  searchLabel2,
  title,
  onClickAdd,
  onInputChange,
  deposit,
  onInputChange2,
}) => {
  const [filterText, setFilterText] = useState('');
  const [filterText2, setFilterText2] = useState('');

  const handleChange = (e) => {
    setFilterText(e.target.value);
  };

  const handleChangeSecond = (e) => {
    setFilterText2(e.target.value);
  };

  useEffect(() => {
    if (onInputChange) {
      onInputChange(filterText);
    }
  }, [filterText, onInputChange]);

  useEffect(() => {
    if (deposit) onInputChange2(filterText2);
  }, [filterText2, onInputChange2, deposit]);

  return (
    <Atom.Container flexDirection="column" width="100%">
      <Atom.Text variant="title">{title}</Atom.Text>
      <Atom.Container
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        width="100%"
        mt="XBIG"
      >
        <Atom.Input
          px="SMALL"
          placeholder={searchLabel}
          onChange={handleChange}
        />
        {deposit && (
          <Atom.Input
            px="SMALL"
            placeholder={searchLabel2}
            onChange={handleChangeSecond}
          />
        )}
        <Atom.Container flexDirection="row" alignItems="center">
          <Molecules.Button onClick={onClickAdd} ml="XSMALL" text="ADICIONAR" />
        </Atom.Container>
      </Atom.Container>
    </Atom.Container>
  );
};

export default HeaderList;
