import styled from 'styled-components';
import theme from '../../../styles/theme';

export const Button = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  max-width: 210px;
  height: 35px;
  border: 0px;
  background: none;
  cursor: pointer;
`;

export const TextButton = styled.p`
  font-size: 14px;
  font-weight: bold;
  font-family: 'Montserrat-Regular';
  color: ${theme.colors.WHITE};

  &:hover {
    text-decoration: underline;
  }
`;
