export default [
  {
    screen: '/companie/list',
    label: 'Empresas',
    icon: 'EMPRESA_WHITE',
    iconSelected: 'EMPRESA_BLACK',
  },
  {
    screen: '/contract/list',
    label: 'Contratos',
    icon: 'CONTRATOS_WHITE',
    iconSelected: 'CONTRATOS_BLACK',
  },
  {
    screen: '/position/list',
    label: 'Cargos',
    icon: 'CARGOS_WHITE',
    iconSelected: 'CARGOS_BLACK',
  },
  {
    screen: '/collaborator/list',
    label: 'Colaboradores',
    icon: 'COLABORADOR_WHITE',
    iconSelected: 'COLABORADOR_BLACK',
  },
  {
    screen: '/deposit/list',
    label: 'Retenção',
    icon: 'DEPOSITO_WHITE',
    iconSelected: 'DEPOSITO_BLACK',
  },
  {
    screen: '/withdrawl/list',
    label: 'Liberação',
    icon: 'RETIRADA_WHITE',
    iconSelected: 'RETIRADA_BLACK',
  },
  {
    screen: '/reports/list',
    label: 'Relatórios',
    icon: 'MOVIMENTACAO_WHITE',
    iconSelected: 'MOVIMENTACAO_BLACK',
  },
];
