/* eslint-disable react/jsx-no-duplicate-props */
import React from 'react';
import * as Atom from '../../Atoms';

const renderNumbers = (currentPage, pages, onClick) => {
  const tmp = [];
  for (let i = 1; i <= pages; i++) {
    tmp.push(i);
  }
  return tmp.map((item) => (
    <Atom.Touch
      onClick={() => {
        onClick(item);
      }}
      noSubmit
    >
      <Atom.Text variant={currentPage === item ? 'currentPage' : 'page'}>
        {item}
      </Atom.Text>
    </Atom.Touch>
  ));
};

const FooterList = ({
  pages,
  currentPage,
  onNextPage,
  onPrevPage,
  onSelectPage,
}) => (
  <Atom.Container
    width="100%"
    height="25px"
    justifyContent="center"
    alignItems="center"
  >
    {currentPage > 1 && (
      <Atom.Touch onClick={onPrevPage} noSubmit>
        <Atom.Icon width="3%" icon="ARROW_BLACK" height="30px" width />
      </Atom.Touch>
    )}

    {renderNumbers(currentPage, pages, onSelectPage)}
    {pages > 1 && currentPage < pages && (
      <Atom.Touch onClick={onNextPage} noSubmit>
        <Atom.Icon width="3%" invert icon="ARROW_BLACK" height="30px" width />
      </Atom.Touch>
    )}
  </Atom.Container>
);

export default FooterList;
