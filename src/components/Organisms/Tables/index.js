/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react/prop-types */
import React from 'react';
import * as Atom from '../../Atoms';
import * as Molecules from '../../Molecules';
import MenuOptions from '../MenuOptions';
import { formatedFloat } from '../../../utils';

/**
 * @param {array} labels - titulo das colunas
 * @param {array} data - data das colucas
 */
const Table = ({
  offset,
  labels,
  data,
  noSelect,
  noOptions,
  selectAll,
  mt,
  onChangeValue,
  onToggle,
  onClickSelectAll,
  onClickSelectItem,
  deposit,
}) => {
  return (
    <>
      <Atom.Container
        width="100%"
        flex="1 1 auto"
        mt={mt || 'XXBIG'}
        flexDirection="column"
      >
        <Atom.Container
          px="XSMALL"
          mb="XSMALL"
          ml={2}
          width="100%"
          justifyContent="space-between"
          alignItems="flex-end"
        >
          {!noSelect && (
            <Atom.Container width="3%" alignItems="center">
              <Atom.Select
                variant={selectAll && 'selected'}
                onClick={onClickSelectAll}
              />
            </Atom.Container>
          )}
          {labels.map((item) => (
            <Atom.Text variant="labelTable" width={item.width} pl={item.pl}>
              {item.value}
            </Atom.Text>
          ))}
          {!noOptions && (
            <Atom.Text width="2.5%">{!noOptions ? 'Opt.' : ''}</Atom.Text>
          )}
        </Atom.Container>
        {/* --------------- content -------------*/}
        {data.map((line, indexLine) => (
          <Atom.Line
            flexDirection="row"
            alignItems="center"
            justifyContent="space-between"
            px="XSMALL"
            selected={line.selected}
          >
            {!noSelect && (
              <Atom.Container width="3%" alignItems="center">
                <Atom.Select
                  variant={line.selected && 'selected'}
                  onClick={() => onClickSelectItem(indexLine)}
                />
              </Atom.Container>
            )}
            {line.columns.map((column, indexColumn) => {
              if (column.type === 'text') {
                let formatedValue = column.value;
                if (column.noNegative) {
                  const isNegative = column.value[0] === '-';
                  const cache = isNegative
                    ? column.value.substring(1)
                    : column.value;
                  formatedValue =
                    parseFloat(formatedFloat(cache)) * (isNegative ? -1 : 1);
                }
                return (
                  <Atom.Text
                    width={column.width}
                    borderRight={column.border && '1px solid'}
                    variant={column.border && 'resultTable'}
                    pl={column.pl}
                    color={column.noNegative && formatedValue < 0 && 'red'}
                  >
                    {column.dependent ? column.value : column.value}
                  </Atom.Text>
                );
              }
              if (column.type === 'image') {
                return (
                  <Atom.Container
                    width={column.width}
                    justifyContent="center"
                    alignItems="center"
                  >
                    <Atom.Avatar variant="avatar" src={column.value} />
                  </Atom.Container>
                );
              }
              if (column.type === 'status') {
                return (
                  <Atom.Container width={column.width} alignItems="center">
                    <Molecules.StatusTag active={column.value} />
                  </Atom.Container>
                );
              }
              if (column.type === 'id') {
                return (
                  <Atom.Container
                    bg="WHITE"
                    width={column.width}
                    alignItems="center"
                    height="99%"
                    pl="XXSMALL"
                  >
                    <Atom.Text>{column.value}</Atom.Text>
                  </Atom.Container>
                );
              }
              if (column.type === 'edit') {
                return (
                  <Molecules.Edit
                    offset={offset}
                    line={indexLine}
                    column={indexColumn}
                    percent={column.width}
                    value={column.value}
                    border={column.border}
                    onChange={onChangeValue}
                    limit={column.limit}
                  />
                );
              }
              if (column.type === 'toggle') {
                return (
                  <Atom.Container
                    width={column.width}
                    alignItems="center"
                    justifyContent="center"
                  >
                    <Atom.ToggleSwitch
                      line={indexLine}
                      column={indexColumn}
                      handleToggle={onToggle}
                      value={column.value}
                    />
                  </Atom.Container>
                );
              }

              return <Atom.Text width={column}>Not Text</Atom.Text>;
            })}
            {!noOptions && <MenuOptions deposit={deposit} data={line.data} />}
          </Atom.Line>
        ))}
      </Atom.Container>
    </>
  );
};

export default Table;
