import React from 'react';
import * as Atom from '../../Atoms';

const ErrorBoundary = ({ children, error }) => {
  if (error) {
    return (
      <Atom.Text width="100%" textAlign="center" fontSize={3}>
        Vish... Algo deu errado, tente novamente
      </Atom.Text>
    );
  }
  return children;
};

export default ErrorBoundary;
