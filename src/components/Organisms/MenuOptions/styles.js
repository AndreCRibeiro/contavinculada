import styled from 'styled-components';

export const ModalContainer = styled.div`
  width: 100vw;
  height: 10vh;
  position: flex;
`;

export const MenuContainer = styled.div`
  position: absolute;
  margin-top: 15px;
  right: 32px;
  width: 130px;
  border-radius: 4px;
  background-color: white;
  box-shadow: 0 -2px 5px 4px grey;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 10px 0 10px 0;
  z-index: 99;

  &::before {
    content: '';
    position: absolute;
    left: ${(props) => (props.deposit ? 'calc(50%)' : 'calc(50% + 20px)')};
    top: -7px;
    width: 0;
    height: 0;
    border-left: 7px solid transparent;
    border-right: 7px solid transparent;
    border-bottom: 7px solid white;
  }
`;

export const Button = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: ${({ big }) => (big ? 200 : 110)}px;
  height: 35px;
  border: 0px;
  border-bottom: ${({ border }) => (border ? 1 : 0)}px solid
    ${({ theme }) => theme.colors.GREY_LIGHT};
  background-color: white;
  cursor: pointer;
`;

export const TextButton = styled.p`
  font-size: 16px;
  font-family: 'Montserrat-Regular';
  color: ${({ theme }) => theme.colors.PRIMARY};
  margin: 2px 0 0 0;
`;

export const Title = styled.p`
  font-size: ${(props) => (props.header ? 18 : 16)}px;
  font-family: ${(props) => (props.header ? 'Roboto-Bold' : 'Roboto-Regular')};
  color: ${({ theme }) => theme.colors.PRIMARY};
`;
