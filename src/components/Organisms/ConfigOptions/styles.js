import styled from 'styled-components';

export const MenuContainer = styled.div`
  position: absolute;
  margin-top: 15px;
  bottom: 90px;
  width: 190px;
  border-radius: 4px;
  background-color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 10px 0 10px 0;
  z-index: 99;

  &::before {
    content: '';
    position: absolute;
    left: calc(0% + 20px);
    bottom: -7px;
    width: 0;
    height: 0;
    border-left: 7px solid transparent;
    border-right: 7px solid transparent;
    border-top: 7px solid white;
  }
`;

export const Button = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: ${({ big }) => (big ? 200 : 170)}px;
  height: 35px;
  border: 0px;
  border-bottom: ${({ border }) => (border ? 1 : 0)}px solid
    ${({ theme }) => theme.colors.GREY_LIGHT};
  background-color: white;
  cursor: pointer;
  background: none;
`;

export const TextButton = styled.p`
  font-size: 16px;
  font-weight: bold;
  font-family: 'Montserrat-Regular';
  color: ${(props) =>
    props.out ? '#AE2E2E' : `${(theme) => theme.colors.PRIMARY}`};
  margin: 5px 0 0 0;
`;

export const Title = styled.p`
  font-size: ${(props) => (props.header ? 18 : 16)} px;
  font-family: ${(props) => (props.header ? 'Roboto-Bold' : 'Roboto-Regular')};
  color: ${({ theme }) => theme.colors.PRIMARY};
`;
