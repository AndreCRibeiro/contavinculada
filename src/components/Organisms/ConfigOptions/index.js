/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useCallback } from 'react';
import { Link } from 'react-router-dom';
import * as Atom from '../../Atoms';
import { MenuContainer, Button, TextButton } from './styles';
import { useUser } from '../../../global';

const ConfigOptions = () => {
  const { setUser, user } = useUser();
  const [show, setShow] = useState(false);

  const handleLogout = useCallback(() => {
    setUser(null);
    localStorage.clear();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Atom.Touch
      width="2%"
      onClick={() => setShow(!show)}
      justifyContent="center"
    >
      <Atom.Flex mr="XSMALL">
        <Atom.Icon icon="SETTIGNS_WHITE" mr="XSMALL" />
        <Atom.Text variant="menu">Configurações</Atom.Text>
      </Atom.Flex>

      {show && (
        <MenuContainer>
          {(user.type === 'admin' || user.type === 'gestor') && (
            <Link
              to="/settings/list-users"
              style={{ textDecoration: 'none', color: '#333' }}
            >
              <Button>
                <TextButton>Niveis de Permissão</TextButton>
              </Button>
            </Link>
          )}
          {/*  <Button onClick={() => { }}>
            <TextButton>Editar Perfil</TextButton>
          </Button> */}
          <Button onClick={handleLogout}>
            <TextButton out>Sair</TextButton>
          </Button>
        </MenuContainer>
      )}
    </Atom.Touch>
  );
};

export default ConfigOptions;
