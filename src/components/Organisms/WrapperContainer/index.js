/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import * as Atom from '../../Atoms';
import DrawerMenu from '../DrawerMenu';

// 4% from extends and 17% retract
export default ({ children }) => {
  return (
    <Atom.Container width="100vw" height="100vh" p="XSMALL">
      <DrawerMenu />
      <Atom.Box variant="main">{children}</Atom.Box>
    </Atom.Container>
  );
};
