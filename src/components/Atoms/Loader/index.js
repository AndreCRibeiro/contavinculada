import React from 'react';
import Loader from 'react-loader-spinner';
import styled from 'styled-components';
import { space, layout } from 'styled-system';

const LoaderComoponent = styled(Loader)`
  ${space}
  ${layout}
`;

export default ({ size, color, ...props }) => (
  <LoaderComoponent
    type="Oval"
    color={color ? '#fff' : '#1700FE'}
    height={size}
    width={size}
    timeout={1500} // 3 secs
    {...props}
  />
);
