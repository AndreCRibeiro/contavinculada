import React, { useState } from 'react';
import styled from 'styled-components';
import { color, border, layout, space } from 'styled-system';
import { Multiselect } from 'multiselect-react-dropdown';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  align-items: center;
  justify-content: center;
`;

Container.defaultProps = {
  // height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  aligItems: 'center',
  bg: 'WHITE_ICE',
  pl: 'XXSMALL',
  pr: 'XSMALL',
  mt: 'XXSMALL',
};

export const PickerGroup = styled(Multiselect)``;

export default ({ options, selected, onChange, placeholderDependency }) => {
  const [selectAll, setSelectAll] = useState(false);

  options = [{ name: 'selecionar todos', id: 9999999999999 }, ...options];

  const handleSelect = (selectedList, selectedItem) => {
    // caso para selecionar todos
    if (selectedItem.id === 9999999999999) {
      setSelectAll(true);
      onChange(options.filter((item) => item.id !== 9999999999999 && item));
      return;
    }
    onChange(selectedList);
  };

  const handleRemove = (selectedList, selectedItem) => {
    onChange(selected.filter((item) => item.id !== selectedItem.id && item));
  };

  return (
    <Container>
      <PickerGroup
        options={
          selectAll
            ? options.filter((item) => item.id !== 9999999999999 && item)
            : options
        } // Options to display in the dropdown
        selectedValues={
          selectAll
            ? selected.filter((item) => item.id !== 9999999999999 && item)
            : selected
        } // Preselected value to persist in dropdown
        onSelect={handleSelect} // Function will trigger on select event
        onRemove={handleRemove} // Function will trigger on remove event
        displayValue="name" // Property name to display in the dropdown options
        placeholder={placeholderDependency}
        style={{
          multiselectContainer: {
            'min-height': '45px',
            'alig-items': 'center',
            'padding-top': '3px',
          },
          chips: { background: '#1700FE' },
          option: {
            // To change css for dropdown options
            color: 'black',
          },
          searchBox: {
            'font-size': '20px',
            border: 'none',
            'border-bottom': '0px',
            'border-radius': '0px',
          },
        }}
      />
    </Container>
  );
};
