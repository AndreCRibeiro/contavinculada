/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, color, layout, variant } from 'styled-system';

const Box = styled.div`
  ${layout}
  ${space}
  ${color}
  ${variant({
    variants: {
      main: {
        width: '100%',
        height: '100%',
        justifyContent: 'space-around',
        overflow: 'auto',
      },
      menu: {
        height: '100%',
        bg: 'TRANSPARENT',
        mr: 'XSMALL',
        justifyContent: 'space-around',
      },
      login: {
        width: 351,
        justifyContent: 'center',
      },
      signup: {
        width: 351,
        height: 205,
        justifyContent: 'center',
      },
      forgot: {
        width: 351,
        height: 120,
        justifyContent: 'center',
      },
      modal: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 313,
      },
      modalAlert: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 313,
      },
      modalSuccess: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 305,
        padding: 20,
        right: 20,
      },
      modalText: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 1000,
        padding: 20,
      },
    },
  })}
  display: flex;
  align-items: center;
  flex-direction: column;
  border-radius: 15px;
`;

Box.defaultProps = {
  bg: 'WHITE',
  p: 'BIG',
};

/**
 * @param {string} variant - main, menu
 */

export default ({ children, ...props }) => {
  return <Box {...props}>{children}</Box>;
};
