/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, layout, variant } from 'styled-system';
import { Icons } from '../../../assets';

const Icon = styled.img`
  ${layout}
  ${space}
  ${variant({
  variants: {
    logout: {
      width: 30,
      height: 30,
    },
  },
})}
  transform:${({ invert }) => invert && 'rotate(180deg)'} ;
`;

Icon.defaultProps = {
  width: 35,
  height: 35,
};

/**
 * @param {boolean} invert - iverte o icone
 */
export default ({ children, icon, ...props }) => {
  return (
    <Icon src={Icons[icon]} {...props}>
      {children}
    </Icon>
  );
};
