/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, color, border, layout, typography } from 'styled-system';

const Input = styled.input`
  ${space}
  ${color}
  ${border}
  ${layout}
  ${typography}
  align-items: center;
  justify-content: center;
  font-family: 'Montserrat-Regular';
  ::-webkit-input-placeholder{
    color: ${({ theme }) => theme.colors.PLACEHOLDER}
  }
`;

const InputMultiLine = styled.textarea`
  ${space}
  ${color}
  ${border}
  ${layout}
  ${typography}
  align-items: center;
  justify-content: center;
  font-family: 'Montserrat-Regular';
  ::-webkit-input-placeholder{
    color: ${({ theme }) => theme.colors.PLACEHOLDER}
  }
`;

Input.defaultProps = {
  width: 300,
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  bg: 'WHITE_ICE',
};

InputMultiLine.defaultProps = {
  width: 300,
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  bg: 'WHITE_ICE',
  py: 'XXSMALL',
};

export default ({ register, value, multiLine, ...props }) => {
  return multiLine ? (
    <InputMultiLine ref={register} {...props} />
  ) : (
    <Input ref={register} value={value} {...props} />
  );
};
