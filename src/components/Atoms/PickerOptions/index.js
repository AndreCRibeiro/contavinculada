import React from 'react';
import styled from 'styled-components';
import { color, border, layout, space, typography } from 'styled-system';
import Text from '../Text';

const Container = styled.div`
  ${space}
  ${color}
  ${border}
  ${layout}
  align-items: center;
  justify-content: center;
`;

const PickerOptions = styled.select`
  ${typography}
  background: ${(props) => (props.teste ? 'transparent' : 'transparent')};
  border-color: ${(props) => (props.teste ? 'GREY_LIGHT' : 'transparent')};
  border-radius: 10px;
  border: 1px solid #BDBDBD;
  height: 35px;
  width: 100%;
  font-family: 'Montserrat-Regular';
  color: ${(props) => (props.teste ? '' : 'grey')};
`;

PickerOptions.defaultProps = {
  fontSize: 3,
};

Container.defaultProps = {
  height: 45,
  aligItems: 'center',
  pl: 'XXSMALL',
  pr: 'XSMALL',
};

export default ({
  name,
  showId,
  options,
  selected,
  placeholderDependency,
  teste,
  onChange,
}) => (
  <Container>
    {Array.isArray(options) ? (
      <PickerOptions name={name} teste={teste} onChange={onChange}>
        <option value="" disabled selected>
          {name}
        </option>
        {options?.map((item, index) => (
          <>
            <option
              defaultValue={index === 0}
              value={item.id}
              selected={selected}
            >
              {showId
                ? `${item.numero_contrato} - ${item.dadosEmpresa.name}`
                : item.name || item.numero_contrato}
            </option>
          </>
        ))}
      </PickerOptions>
    ) : (
      <Text variant="normal" pl="XXSMALL" mt="XSMALL">
        {placeholderDependency}
      </Text>
    )}
  </Container>
);
