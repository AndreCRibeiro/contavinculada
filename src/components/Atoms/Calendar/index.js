import React from 'react';
import styled from 'styled-components';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

const CustomCalendar = styled(Calendar)`
  position: absolute;
  margin: 70px 0 0 10px;
  z-index: 99;
`;

export default ({ onChange, viewYearCalendar, minDate, defaultValue }) => (
  <CustomCalendar
    value={defaultValue || new Date()}
    defaultView={viewYearCalendar ? 'decade' : 'year'}
    view={viewYearCalendar ? 'decade' : 'year'}
    locale="pt"
    onClickMonth={onChange}
    onClickYear={onChange}
    minDate={minDate && new Date(minDate)}
    keepFocus={false}
  />
);
