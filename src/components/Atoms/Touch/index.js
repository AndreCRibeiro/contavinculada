/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, color, layout, flexbox } from 'styled-system';

const Button = styled.button.attrs(({ noSubmit }) => ({
  type: noSubmit ? 'button' : 'submit',
}))`
  ${space}
  ${color}
  ${layout}
  ${flexbox}
  border: none;
  border-radius: 15px;
  display: flex;
  cursor: pointer;
`;

Button.defaultProps = {
  bg: 'TRANSPARENT',
};

export default ({ children, onClick, ...props }) => {
  return (
    <Button {...props} onClick={onClick}>
      {children}
    </Button>
  );
};
