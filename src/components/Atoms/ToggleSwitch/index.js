import React, { useState } from 'react';
import Switch from 'react-switch';

export default ({
  line,
  column,
  handleToggle,
  handleToggleAll,
  value,
  allSelected,
  ...props
}) => {
  const [sel, setSel] = useState(value);
  React.useEffect(() => {
    if (handleToggleAll && !allSelected) setSel(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [allSelected]);
  return (
    <Switch
      height={18}
      width={32}
      onChange={() => {
        if (handleToggleAll) handleToggleAll(!sel);
        if (handleToggle) handleToggle(line, column, !sel);
        setSel(!sel);
      }}
      checked={handleToggleAll ? sel : value}
      uncheckedIcon={false}
      checkedIcon={false}
      onColor="#1700FE"
      offColor="#aaa"
      handleDiameter={14}
    />
  );
};
