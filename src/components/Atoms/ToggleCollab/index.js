import React from 'react';
import Switch from 'react-switch';
import Text from '../Text';
import Container from '../Container';

export default ({
  name,
  register,
  label,
  percent,
  onChange,
  checked,
  ...props
}) => {
  return (
    <Container flexDirection="column" width={`${percent}%`}>
      <Container mb={15} alignItems="flex-end">
        <Text ml={2}>{label}</Text>
      </Container>
      <Switch
        name={name}
        register={register}
        height={23}
        width={68}
        onChange={onChange}
        checked={checked}
        uncheckedIcon={
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
              fontSize: 12,
              color: 'black',
              paddingRight: 18,
            }}
          >
            Inativo
          </div>
        }
        checkedIcon={
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: '100%',
              fontSize: 12,
              color: 'white',
              paddingLeft: 15,
            }}
          >
            Ativo
          </div>
        }
        onColor="#1700FE"
        offColor="#aaa"
        handleDiameter={14}
      />
    </Container>
  );
};
