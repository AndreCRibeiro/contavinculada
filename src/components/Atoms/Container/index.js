/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import {
  space,
  color,
  layout,
  variant,
  flexbox,
  border,
  position,
} from 'styled-system';

const Container = styled.div`
  ${space}
  ${color}
  ${layout}
  ${flexbox}
  ${border}
  ${position}
  ${variant({
  variants: {
    login: {
      width: '100vw',
      height: '100vh',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    main: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'space-between',
    },
    status: {
      height: '20px',
      borderRadius: 10,
      alignItems: 'center',
      justifyContent: 'center',
    },
    create: {
      width: '100%',
      height: '100%',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
    },
    lineCreate: {
      mb: 'SMALL',
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    filterError: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      marginTop: '100px',
    },
    columnCreate: {
      mb: 'SMALL',
      width: '100%',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    divider: {
      height: '1px',
      bg: 'PRIMARY',
      mb: 'SMALL',
      width: '100%',
    },
    dividerModal: {
      height: '1px',
      bg: 'GREY_LIGHT',
      mb: 'SMALL',
      width: '100%',
    },
    list: {
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      height: '100%',
    },
  },
})}
  display: flex;
`;

export default ({ children, ...props }) => {
  return <Container {...props}>{children}</Container>;
};
