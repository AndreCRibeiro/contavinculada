/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import InputMask from 'react-input-mask';
import styled from 'styled-components';
import { space, color, border, layout, typography } from 'styled-system';

const Input = styled(InputMask)`
  ${space}
  ${color}
  ${border}
  ${layout}
  ${typography}
  align-items: center;
  justify-content: center;
  font-family: 'Montserrat-Regular';
  ::-webkit-input-placeholder{
    color: ${({ theme }) => theme.colors.PLACEHOLDER}
  }
`;

Input.defaultProps = {
  width: 300,
  height: 45,
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 10,
  bg: 'WHITE_ICE',
};

export default ({ register, ...props }) => {
  return <Input ref={register} {...props} />;
};
