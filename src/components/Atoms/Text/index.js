/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import {
  typography,
  space,
  color,
  variant,
  layout,
  border,
} from 'styled-system';

const Text = styled.p`
  ${typography}
  ${space}
  ${color}
  ${layout}
  ${border}
  ${variant({
  variants: {
    title: {
      fontFamily: 'BOLD',
      fontSize: 4,
      color: 'BLACK',
    },
    labelTable: {
      fontFamily: 'SEMI_BOLD',
      fontSize: 1,
      color: 'BLACK',
    },
    normal: {
      fontFamily: 'REGULAR',
      fontSize: 2,
      color: 'BLACK',
    },
    menu: {
      fontFamily: 'REGULAR',
      fontSize: 3,
      color: 'WHITE',
    },
    menuSelected: {
      fontFamily: 'REGULAR',
      fontSize: 3,
      color: 'PRIMARY',
    },
    placeholder: {
      fontFamily: 'REGULAR',
      fontSize: 2,
      color: 'BLACK',
    },
    details: {
      fontFamily: 'REGULAR',
      fontSize: 0,
      color: 'BLACK',
    },
    button: {
      fontFamily: 'SEMI_BOLD',
      fontSize: 2,
      color: 'WHITE',
    },
    buttonOutline: {
      fontFamily: 'SEMI_BOLD',
      fontSize: 2,
      color: 'BLACK',
    },
    buttonForget: {
      fontFamily: 'SEMI_BOLD',
      fontSize: 1,
      color: 'WHITE',
    },
    buttonCancel: {
      fontFamily: 'SEMI_BOLD',
      fontSize: 2,
      color: 'WHITE',
    },
    page: {
      fontFamily: 'SEMI_BOLD',
      fontSize: 2,
      color: 'BLACK',
      ml: 'XSMALL',
    },
    currentPage: {
      fontFamily: 'SEMI_BOLD',
      fontSize: 3,
      ml: 'XSMALL',
      color: 'SECONDARY',
    },
    active: {
      fontFamily: 'REGULAR',
      fontSize: 1,
      color: 'GREEN',
    },
    noActive: {
      fontFamily: 'REGULAR',
      fontSize: 1,
      color: 'RED',
    },
    modal: {
      fontFamily: 'BOLD',
      fontSize: 2,
      color: 'BLACK',
      textAlign: 'center',
    },
    modalLabelText: {
      fontFamily: 'SEMI_BOLD',
      fontSize: 2,
      color: 'GREY',
    },
    modalText: {
      fontFamily: 'BOLD',
      fontSize: 2,
      color: 'BLACK',
      flex: 1,
    },
    error: {
      width: '90%',
      fontSize: 1,
      color: 'RED',
    },
    secondary: {
      fontFamily: 'REGULAR',
      fontSize: 1,
      color: 'WHITE',
    },
    resultTable: {
      fontFamily: 'REGULAR',
      fontSize: 2,
      borderColor: 'white',
      lineHeight: '2.5',
      textAlign: 'center',
      height: '34px',
      bg: 'ICE',
    },
    list: {
      fontFamily: 'REGULAR',
      fontSize: 3,
      color: 'BLACK',
    },
    calendarPicker: {
      fontFamily: 'REGULAR',
      fontSize: 3,
      color: 'BLACK',
    },
    filterError: {
      fontFamily: 'BOLD',
      fontSize: 2,
      color: 'BLACK',
    },
  },
})}
   overflow: hidden;
   text-overflow: ellipsis;
`;

Text.defaultProps = {
  variant: 'normal',
};

export default ({ children, ...props }) => {
  return <Text {...props}>{children}</Text>;
};
