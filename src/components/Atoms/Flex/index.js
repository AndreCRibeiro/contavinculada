/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, color, layout, flexbox, variant } from 'styled-system';

const Flex = styled.div`
  ${space}
  ${color}
  ${layout}
  ${flexbox}
  ${variant({
  variants: {
    drawer: {
      flexDirection: 'column',
    },
    modalConfirm: {
      flexDirection: 'column',
    },
    logout: {
      alignItems: 'center',
      justifyContent: 'flex-start',
      width: 210,
      padding: 0,
    },
  },
})}
  display: flex;
`;

Flex.defaultProps = {
  p: 'XSMALL',
  alignItems: 'center',
  flexDirection: 'row',
  justifyContent: 'center',
};

export default ({ children, ...props }) => {
  return <Flex {...props}>{children}</Flex>;
};
