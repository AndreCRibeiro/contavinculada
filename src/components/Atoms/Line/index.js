import React from 'react';
import styled from 'styled-components';
import { space, border, color, flexbox, variant, layout } from 'styled-system';

const Line = styled.div`
  ${space}
  ${color}
  ${border}
  ${flexbox}
  ${layout}
  ${variant({
  variants: {
    selected: {
      borderColor: 'BLUE_TRANSPARET',
      bg: 'SELECT_LINE',
    },
    result: {
      px: 'XXBIG',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
  },
})}
  min-height: 36px;
  display: flex;
  box-shadow: 0 ${({ selected }) => (selected ? 2 : 0)}px ${({ selected }) =>
    selected ? 5 : 0}px #ccc;
  margin-bottom: 5px;
`;

Line.defaultProps = {
  width: '100%',
  border: '1.5px solid',
  borderColor: 'ICE',
  borderRadius: 10,
  bg: 'ICE_LIGHT',
};

export default ({ children, selected, ...props }) => (
  <Line selected={selected} variant={selected && 'selected'} {...props}>
    {children}
  </Line>
);
