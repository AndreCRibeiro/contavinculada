/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, layout, variant, typography } from 'styled-system';

const Title = styled.h1`
  ${space}
  ${layout}
  ${typography}
  ${variant({
    variants: {
      login: {
        height: 95,
        width: 350,
        fontSize: 40,
        textAlign: 'center',
      },
      normal: {
        height: 90,
        width: 350,
        fontSize: 39,
        textAlign: 'center',
      },
    },
  })}
  color: ${({ theme }) => theme.colors.WHITE};
`;

Title.defaultProps = {
  variant: 'login',
};

export default ({ children, ...props }) => {
  return <Title {...props}>{children}</Title>;
};
