import React from 'react';
import styled from 'styled-components';
import { color, border, layout, variant } from 'styled-system';
import Icon from '../Icon';

const BoxSelect = styled.button.attrs({
  type: 'button',
})`
  ${color}
  ${border}
  ${layout}
  ${variant({
    variants: {
      selected: {
        borderColor: 'BLACK',
        bg: 'SECONDARY',
      },
    },
  })}
  width: 15px;
  height: 15px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

BoxSelect.defaultProps = {
  border: '1px solid',
  borderColor: 'GREY_LIGHT',
  borderRadius: 5,
  bg: 'WHITE',
};

export default ({ ...props }) => (
  <BoxSelect {...props}>
    <Icon icon="DONE_WHITE" height="70%" width="70%" />
  </BoxSelect>
);
