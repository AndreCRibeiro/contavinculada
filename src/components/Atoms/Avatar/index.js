/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import styled from 'styled-components';
import { space, layout, variant } from 'styled-system';
import { Imgs } from '../../../assets';

const Avatar = styled.img`
  ${layout}
  ${space}
  ${variant({
    variants: {
      drawer: {
        width: '80px',
        height: '80px',
        borderRadius: '15px',
      },
      retractedDrawer: {
        width: '60px',
        height: '60px',
        borderRadius: '30px',
      },
      create: {
        width: '140px',
        height: '140px',
        borderRadius: '15px',
      },
      createUser: {
        width: '220px',
        height: '220px',
        borderRadius: '15px',
      },
      avatar: {
        width: '30px',
        height: '30px',
        borderRadius: '15px',
      },
      avatarModal: {
        width: '120px',
        height: '120px',
        borderRadius: '15px',
      },
    },
  })}
  border: 1px solid #ccc;
`;

Avatar.defaultProps = {
  width: '30px',
  height: '30px',
};

export default ({ src, ...props }) => {
  return <Avatar src={src || Imgs.DEFAULT_PERFIL} {...props} />;
};
