/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback, useState, useEffect, useMemo } from 'react';
import immer from 'immer';
import { isString, each, findIndex } from 'lodash';
import { useWithdrawals } from '../global';
import { formated } from '../utils';

const tratamentoString = (str) => {
  const newStr = isString(str) ? str.replace('.', '').replace(',', '.') : str;

  return newStr;
};

export const useTable = (data, trigger, metadata) => {
  const { rubrica } = useWithdrawals();
  const [selectAll, setSelectAll] = useState(false);
  const [trigger2, setTrigger2] = useState(true);
  const [triggerOffset, setTriggerOffset] = useState(true);
  const [dataTable, setDataTable] = useState(data);
  const [backupData, setBackupData] = useState(data);
  const [offset, setOffset] = useState(1);

  useEffect(() => {
    const notSelectAll = dataTable.find((item) => !item.selected);
    const allSelected = dataTable.filter((item) => item.selected);
    if (notSelectAll || dataTable.length === 0) {
      setSelectAll(false);
    }
    if (allSelected.length === dataTable.length) {
      setSelectAll(true);
    }
  }, [dataTable]);

  useEffect(() => {
    // if (data.map((item) => item.data) === dataTable.map((item) => item.data))
    //   return false;
    if (data) {
      setDataTable(data);
      setBackupData(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data?.length, trigger]);

  const dataTableArray = useMemo(() => {
    dataTable.filter(
      (_, index) => index < offset * 10 && index >= (offset - 1) * 10
    );
    return dataTable.filter(
      (_, index) => index < offset * 10 && index >= (offset - 1) * 10
    );
  }, [dataTable, offset, trigger2]);

  const numberOfPage = useMemo(() => {
    const divider = dataTable.length / 10;
    const rest = dataTable.length % 10;
    return rest > 0 ? divider + 1 - rest / 10 : divider;
  }, [dataTable, offset]);

  const handleSelectItem = useCallback(
    (indexLine, enableOffset) => {
      if (enableOffset) {
        setDataTable((prevData) =>
          prevData.map((item, index) =>
            index === indexLine + (offset - 1) * 10
              ? { ...item, selected: !item.selected }
              : item
          )
        );
        return;
      }
      setDataTable((prevData) =>
        prevData.map((item, index) =>
          index === indexLine ? { ...item, selected: !item.selected } : item
        )
      );
    },
    [offset]
  );

  const handleSelectAll = useCallback(() => {
    setSelectAll(!selectAll);
    if (selectAll) {
      setDataTable((prevData) =>
        prevData.map((item) => ({ ...item, selected: false }))
      );
    } else {
      setDataTable((prevData) =>
        prevData.map((item) => ({ ...item, selected: true }))
      );
    }
  }, [selectAll]);

  const handleDelete = useCallback(
    () =>
      setDataTable((prevData) =>
        prevData.filter((item) => !item.selected && item)
      ),
    []
  );

  const handleChangeValue = async (
    lineIndex,
    columnIndex,
    value,
    callbackChangeValue
  ) => {
    setTrigger2(false);
    setDataTable(
      (prev) =>
        callbackChangeValue({
          offset,
          prev,
          dataTableArray,
          lineIndex,
          columnIndex,
          value,
          metadata,
        }).dataTable
    );
    setBackupData(
      (prev) =>
        callbackChangeValue({
          prev,
          lineIndex,
          columnIndex,
          dataTableArray,
          value,
          metadata,
        }).backupData
    );
    setTimeout(() => setTrigger2(true), 100);
  };

  const handleToggle = (lineIndex, columnIndex, value) => {
    setTrigger2(false);
    /*
      com o lineIndex eu tenho que buscar no array todo a posição do item e trocar ela
    */
    const idTable = dataTableArray[lineIndex].item.id;
    const index = findIndex(dataTable, (el) => el.item.id === idTable);
    setDataTable((prev) => {
      const nextState = immer(prev, (draftState) => {
        draftState[index].columns[rubrica === 'rescisao' ? 7 : 6].value = value
          ? 0
          : backupData[index].columns[rubrica === 'rescisao' ? 7 : 6].value;

        // subtotal - encargo
        draftState[index].columns[rubrica === 'rescisao' ? 8 : 7].value = value
          ? formated(
            parseFloat(
              tratamentoString(
                backupData[index].columns[rubrica === 'rescisao' ? 8 : 7]
                  .value
              )
            ) -
            parseFloat(
              tratamentoString(
                backupData[index].columns[rubrica === 'rescisao' ? 7 : 6]
                  .value
              )
            )
          )
          : backupData[index].columns[rubrica === 'rescisao' ? 8 : 7].value;
        // saldoEncargo + encargo - como zerou o encargo ele vai pro
        draftState[index].columns[rubrica === 'rescisao' ? 12 : 9].value = value
          ? formated(
            parseFloat(
              tratamentoString(
                backupData[index].columns[rubrica === 'rescisao' ? 12 : 9]
                  .value
              )
            ) +
            parseFloat(
              tratamentoString(
                backupData[index].columns[rubrica === 'rescisao' ? 7 : 6]
                  .value
              )
            )
          )
          : backupData[index].columns[rubrica === 'rescisao' ? 12 : 9].value;

        // saldoSubtotal + encargo
        draftState[index].columns[
          rubrica === 'rescisao' ? 13 : 10
        ].value = value
            ? formated(
              parseFloat(
                tratamentoString(
                  backupData[index].columns[rubrica === 'rescisao' ? 13 : 10]
                    .value
                )
              ) +
              parseFloat(
                tratamentoString(
                  backupData[index].columns[rubrica === 'rescisao' ? 7 : 6]
                    .value
                )
              )
            )
            : backupData[index].columns[rubrica === 'rescisao' ? 13 : 10].value;

        draftState[index].columns[rubrica === 'rescisao' ? 3 : 4].value = value;
      });

      return nextState;
    });
    setTimeout(() => setTrigger2(true), 100);
  };

  const handleToggleAll = (value) => {
    setTrigger2(false);
    setDataTable((prev) =>
      immer(prev, (draftState) => {
        each(draftState, (_, lineIndex) => handleToggle(lineIndex, '', value));
      })
    );
    setTimeout(() => setTrigger2(true), 100);
  };

  const nextPage = useCallback(() => {
    setTriggerOffset(false);
    setOffset(offset + 1);
    setTimeout(() => setTriggerOffset(true), 50);
  }, [offset]);

  const prevPage = useCallback(() => {
    if (offset > 0) {
      setTriggerOffset(false);
      setOffset(offset - 1);
      setTimeout(() => setTriggerOffset(true), 50);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [offset]);

  const selectPage = useCallback((page) => setOffset(page), [offset]);

  return {
    trigger2,
    triggerOffset,
    dataTableArray,
    dataTable,
    selectAll,
    offset,
    numberOfPage,
    prevPage,
    nextPage,
    selectPage,
    handleToggle,
    handleDelete,
    handleToggleAll,
    handleSelectAll,
    handleSelectItem,
    handleChangeValue,
  };
};
