/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect, useMemo } from 'react';

export const useAllData = (data, trigger) => {
  const [allDataTable, setAllDataTable] = useState(data);
  const offset = 1;

  useEffect(() => {
    if (data) {
      setAllDataTable(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data?.length, trigger]);

  const dataTableArray = useMemo(() => {
    allDataTable.filter(
      (_, index) => index < offset * 10 && index >= (offset - 1) * 10
    );
    return allDataTable.filter(
      (_, index) => index < offset * 10 && index >= (offset - 1) * 10
    );
  }, [allDataTable, offset]);

  return {
    dataTableArray,
    allDataTable,
    offset,
  };
};
