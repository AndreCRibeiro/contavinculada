import history from '../services/history';

export const useCommon = () => {
  return {
    history,
  };
};
