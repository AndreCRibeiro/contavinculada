/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useCallback } from 'react';
import { toast } from 'react-toastify';
import { useUser } from '../global';
import api from '../services/api';
import { useCommon } from './useCommon';

export const useAuth = (retrive) => {
  const { setUser, user } = useUser();
  const { history } = useCommon();

  const retriveUser = useCallback(async () => {
    const data = await localStorage.getItem('@contavinculada_user');
    if (data) {
      const { token } = JSON.parse(data);
      api.defaults.headers.Authorization = `Bearer ${token}`;
      setUser(JSON.parse(data));
      return;
    }
    api.defaults.headers.Authorization = '';
    history.push('/');
  }, []);

  const handleLogout = useCallback(() => {
    api.defaults.headers.Authorization = null;
    localStorage.clear();
    setUser(null);
    history.push('/');
  }, []);

  const handleFirstAccess = useCallback((response) => {
    api.defaults.headers.Authorization = `Bearer ${response.token}`;
    history.push('/signup');
    // window.location.reload();
  }, []);

  const handleSuccess = useCallback(async (response) => {
    if (!response?.token) {
      toast.warn('Recarregue a pagina e tente novamente', {
        position: toast.POSITION.TOP_CENTER,
      });
      return;
    }
    api.defaults.headers.Authorization = `Bearer ${response.token}`;
    await localStorage.setItem(
      '@contavinculada_user',
      JSON.stringify({
        token: response.token,
        ...response.user,
      })
    );
    setUser({
      token: response.token,
      ...response.user,
    });
  }, []);

  useEffect(() => {
    if (!user && retrive) retriveUser();
    // retriveUser();
    // handleLogout();
  }, []);

  return {
    user,
    handleLogout,
    handleSuccess,
    handleFirstAccess,
  };
};
