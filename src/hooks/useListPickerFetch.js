import { useMemo } from 'react';
import useSWR from 'swr';

export const useListPickerFetch = (url) => {
  const { data, error, revalidate } = useSWR(`${url}`);

  const loading = useMemo(() => !data && !error, [data, error]);

  return {
    data,
    error,
    loading,
    trigger: () => revalidate(),
  };
};
