import { useCallback, useState } from 'react';
import { toast } from 'react-toastify';

export const useUpdateFetch = (fetch, id) => {
  const [loading, setLoading] = useState(false);
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);

  const load = useCallback(
    async (params2 = {}) => {
      setLoading(true);
      setError(null);
      try {
        const { data } = await fetch({ id, ...params2 });
        setResponse(data);
        toast.success('Ok, tudo certo!', {
          position: toast.POSITION.TOP_CENTER,
        });
      } catch (err) {
        setError(err);
        toast.error(err?.response?.data?.message || 'Ops, algo deu errado', {
          position: toast.POSITION.TOP_CENTER,
        });
        setResponse(null);
      }
      setLoading(false);
    },
    [fetch, id]
  );

  return [load, { loading, response, error }];
};
