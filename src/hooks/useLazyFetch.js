import { useCallback, useState } from 'react';
import { toast } from 'react-toastify';

const ERRORS = {
  'Invalid Token': 'Codigo invalido',
  'Token not provided': 'Usuario nao logado',
};

export const useLazyFetch = (fetch, params = {}) => {
  const [loading, setLoading] = useState(false);
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);

  const load = useCallback(
    async (params2 = {}) => {
      setLoading(true);
      setError(null);
      try {
        const { data } = await fetch({ ...params, ...params2 });
        setResponse(data);
        toast.success('Ok, tudo certo!', {
          position: toast.POSITION.TOP_RIGHT,
        });
        setLoading(false);
      } catch (err) {
        if (err.response?.status >= 500) {
          toast.error(
            'Houve um erro na requisição. Tente novamente em instantes.',
            {
              position: toast.POSITION.TOP_RIGHT,
            }
          );
          setResponse(null);
          setLoading(false);
        } else {
          console.log({ err });
          toast.error(
            `${err.response?.data?.message || ERRORS[err.response?.data?.error]
            }`,
            {
              position: toast.POSITION.TOP_RIGHT,
            }
          );
          setResponse(null);
          setLoading(false);
        }
        setError(err);
      }
    },
    [fetch, params]
  );

  return [load, { loading, response, error }];
};
