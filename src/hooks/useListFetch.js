import { useEffect, useState, useCallback, useMemo } from 'react';
import { toast } from 'react-toastify';
import useSWR from 'swr';

export const useListFetch = (url) => {
  const [offset, setOffset] = useState(1);
  const [cachePages, setCachePages] = useState(0);

  const { data, error, mutate } = useSWR(`${url}?number=10&page=${offset}`);

  useEffect(() => {
    if (error) {
      toast.error('Houve um erro ao carregar ao lista', {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    if (data?.pages !== cachePages && data?.pages > 0) {
      setCachePages(data.pages);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error, data]);

  const nextPage = useCallback(() => setOffset(offset + 1), [offset]);

  const prevPage = useCallback(() => {
    if (offset > 0) {
      setOffset(offset - 1);
    }
    mutate(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [offset]);

  const selectPage = useCallback((page) => setOffset(page), []);

  const loading = useMemo(() => !data && !error, [data, error]);

  return {
    data,
    error,
    offset,
    loading,
    numberPages: cachePages,
    nextPage,
    prevPage,
    selectPage,
  };
};
