import React from 'react';
import { Router } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { ToastContainer } from 'react-toastify';
import { SWRConfig } from 'swr';
import GlobalStyle from './styles/global';
import Routes from './routes';
import theme from './styles/theme';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import { fetcher } from './config';
import history from './services/history';

const App = () => {
  return (
    <>
      <ThemeProvider theme={theme}>
        <SWRConfig
          value={{
            refreshInterval: 600000,
            fetcher,
          }}
        >
          <Router history={history}>
            <Routes />
            <ToastContainer autoClose={3000} />
          </Router>
        </SWRConfig>
      </ThemeProvider>
      <GlobalStyle />
    </>
  );
};

export default App;
