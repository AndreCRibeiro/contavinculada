import create from 'zustand';

const [useWithdrawals] = create((set) => ({
  rubrica: null,
  setRubrica: (rubrica) => set({ rubrica }),
}));

export { useWithdrawals };
