import create from 'zustand';

const [useStore] = create((set) => ({
  dataZustand: null,
  modalDelete: false,
  edit: false,
  pdf: false,
  setPdf: (data) => set(() => ({ dataZustand: data, pdf: true })),
  set: (data) => set(() => ({ dataZustand: data, modalDelete: true })),
  setEdit: (data) => set(() => ({ dataZustand: data, edit: true })),
  reset: () => set({ modalDelete: false, edit: false, pdf: false }),
}));

export default useStore;
