import create from 'zustand';

const [useUser] = create((set) => ({
  user: null,
  setUser: (user) => set({ user }),
  resetUser: () => set({ user: null }),
}));

export { useUser };
