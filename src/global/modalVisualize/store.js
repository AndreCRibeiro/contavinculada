import create from 'zustand';

const [useStoreVisualize] = create((set) => ({
  dataZustandVisualize: null,
  modalVisualize: false,
  setVisualize: (data) =>
    set(() => ({ dataZustandVisualize: data, modalVisualize: true })),
  resetVisualize: () => set({ modalVisualize: false }),
}));

export default useStoreVisualize;
