export const label = [
  {
    value: '',
    width: '5%',
  },
  {
    value: 'Colaborador',
    width: '23%',
  },
  {
    value: 'Posto',
    width: '10%',
  },
  {
    value: 'Ano',
    width: '6%',
  },
  {
    value: '13º',
    width: '10%',
    pl: '3.5%',
  },
  {
    value: 'Encargos',
    width: '10%',
    pl: '2%',
  },
  {
    value: 'Total',
    width: '10%',
    pl: '2.5%',
  },
  {
    value: '13º',
    width: '10%',
    pl: '3.5%',
  },
  {
    value: 'Encargos',
    width: '10%',
    pl: '2%',
  },
  {
    value: 'Total',
    width: '8%',
    pl: '2%',
  },
];

export const data = [
  {
    selected: false,
    columns: [
      {
        value: 0,
        width: '5%',
        type: 'text',
      },
      {
        value: 'Adriano Reis Ferreira',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Garçom',
        width: '10%',
        type: 'text',
      },
      {
        value: '2020',
        width: '6%',
        type: 'text',
      },
      {
        value: '2.189,00',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '761,77',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.950,77',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.200,00',
        width: '10%',
        border: true,
        type: 'text',
      },
      {
        value: '765,60 ',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.965,60',
        width: '8%',
        type: 'text',
        pl: '2%',
      },
    ],
  },
  {
    selected: false,
    columns: [
      {
        value: 0,
        width: '5%',
        type: 'text',
      },
      {
        value: 'Adriano Reis Ferreira',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Garçom',
        width: '10%',
        type: 'text',
      },
      {
        value: '2020',
        width: '6%',
        type: 'text',
      },
      {
        value: '2.189,00',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '761,77',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.950,77',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.200,00',
        width: '10%',
        border: true,
        type: 'text',
      },
      {
        value: '765,60 ',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.965,60',
        width: '8%',
        type: 'text',
        pl: '2%',
      },
    ],
  },
  {
    selected: false,
    columns: [
      {
        value: 0,
        width: '5%',
        type: 'text',
      },
      {
        value: 'Adriano Reis Ferreira',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Garçom',
        width: '10%',
        type: 'text',
      },
      {
        value: '2020',
        width: '6%',
        type: 'text',
      },
      {
        value: '2.189,00',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '761,77',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.950,77',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.200,00',
        width: '10%',
        border: true,
        type: 'text',
      },
      {
        value: '765,60 ',
        width: '10%',
        type: 'text',
        border: true,
      },
      {
        value: '2.965,60',
        width: '8%',
        type: 'text',
        pl: '2%',
      },
    ],
  },
];
