import * as yup from 'yup';

export const schema = yup.object().shape({
  name: yup.string().required('nome é obrigatorio'),
  email: yup.string().email('e-mail invalido').required('e-mail é obrigatorio'),
  cnpj: yup.string().required('cnpj é obrigatorio'),
  phone: yup.string().required('telefone da empresa é obrigatorio'),
  responsible_name: yup.string().required('nome do preposto é obrigatorio'),
  responsible_email: yup
    .string()
    .email('e-mail invalido')
    .required('e-mail do preposto é obrigatorio'),
  responsible_phone: yup
    .string()
    .required('telefone do prepostos é obrigatorio'),
});
