export const label = [
  {
    value: '',
    width: '3%',
  },
  {
    value: 'Colaborador',
    width: '15%',
  },
  {
    value: 'Posto',
    width: '9%',
  },
  //
  {
    value: '13º',
    width: '7.2%',
    pl: '3.5%',
  },
  {
    value: 'Férias',
    width: '7.2%',
    pl: '2.5%',
  },
  {
    value: 'Multa FGTS',
    width: '7%',
    pl: '1.7%',
  },
  {
    value: 'Encargos',
    width: '7.2%',
    pl: '2%',
  },
  {
    value: 'SubTotal',
    width: '9%',
    pl: '2.3%',
  },
  //
  {
    value: '13º',
    width: '7.2%',
    pl: '2.5%',
  },
  {
    value: 'Férias',
    width: '7.2%',
    pl: '1.5%',
  },
  {
    value: 'Multa FGTS',
    width: '6.5%',
    pl: '0.7%',
  },
  {
    value: 'Encargos',
    width: '7.2%',
    pl: '0.5%',
  },
  {
    value: 'SubTotal',
    width: '7.2%',
    pl: '1.5%',
  },
];

export const label2 = [
  {
    value: '',
    width: '3%',
  },
  {
    value: 'Colaborador',
    width: '15%',
  },
  {
    value: 'Posto',
    width: '10%',
  },
  {
    value: 'Zerar Encargos',
    width: '7%',
  },
  //
  {
    value: '13º',
    width: '8.5%',
    pl: '2%',
  },
  {
    value: 'Férias',
    width: '8.5%',
    pl: '1%',
  },
  {
    value: 'Multa FGTS',
    width: '8%',
    pl: '0.5%',
  },
  {
    value: 'Encargos',
    width: '8.5%',
    pl: '1%',
  },
  {
    value: 'SubTotal',
    width: '9%',
    pl: '1%',
  },
  //
  {
    value: '13º',
    width: '8.5%',
    pl: '2%',
  },
  {
    value: 'Férias',
    width: '8.5%',
    pl: '1.5%',
  },
  {
    value: 'Multa FGTS',
    width: '8.5%',
    pl: '1.2%',
  },
  {
    value: 'Encargos',
    width: '8.5%',
    // pl: '0.5%',
  },
  {
    value: 'SubTotal',
    width: '9%',
    pl: '1.3%',
  },
];
