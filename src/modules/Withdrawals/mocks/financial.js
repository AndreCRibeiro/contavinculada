export const label = (rubrica) => [
  {
    value: '',
    width: '4%',
  },
  {
    value: 'Colaborador',
    width: '23%',
  },
  {
    value: 'Posto',
    width: '15%',
  },
  {
    value: 'Ano',
    width: '6%',
  },
  {
    value: rubrica === 'ferias' ? 'Férias' : '13º',
    width: '8.5%',
    pl: rubrica === '13º' ? '3.5%' : '2.5%',
  },
  {
    value: 'Encargos',
    width: '8.5%',
    pl: '1.5%',
  },
  {
    value: 'Subtotal',
    width: '9%',
    pl: '2%',
  },
  {
    value: rubrica === 'ferias' ? 'Férias' : '13º',
    width: '8.5%',
    pl: rubrica === '13º' ? '3.5%' : '2.5%',
  },
  {
    value: 'Encargos',
    width: '8.5%',
    pl: '1.5%',
  },
  {
    value: 'Subtotal',
    width: '9%',
    pl: '2.5%',
  },
];

export const label2 = (rubrica) => [
  {
    value: '',
    width: '4%',
  },
  {
    value: 'Colaborador',
    width: '18%',
  },
  {
    value: 'Posto',
    width: '10%',
  },
  {
    value: 'Ano',
    width: '6%',
  },
  {
    value: 'Zerar Encargos',
    width: '10%',
    pl: '1%',
  },
  {
    value: rubrica === 'ferias' ? 'Férias' : '13º',
    width: '8.5%',
    pl: rubrica === '13º' ? '3.5%' : '2.5%',
  },
  {
    value: 'Encargos',
    width: '8.5%',
    pl: '1.5%',
  },
  {
    value: 'Subtotal',
    width: '9%',
    pl: '2%',
  },
  {
    value: rubrica === 'ferias' ? 'Férias' : '13º',
    width: '8.5%',
    pl: rubrica === '13º' ? '3.5%' : '2.5%',
  },
  {
    value: 'Encargos',
    width: '8.5%',
    pl: '1.5%',
  },
  {
    value: 'Subtotal',
    width: '9%',
    pl: '2.5%',
  },
];

export const data = [
  {
    selected: false,
    columns: [
      {
        value: 0,
        width: '4%',
        type: 'text',
      },
      {
        value: 'Adriano Reis Ferreira',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Garçom',
        width: '15%',
        type: 'text',
      },
      {
        value: '2020',
        width: '6%',
        type: 'text',
      },
      // retido
      {
        value: '1200,00',
        width: '8.5%',
        type: 'text',
        border: true,
      },
      {
        value: '761,77',
        width: '8.5%',
        type: 'text',
        border: true,
      },
      {
        value: '761,77',
        width: '9%',
        type: 'text',
        border: true,
      },
      // despendido
      {
        // value: '2.200,00',
        width: '8.5%',
        border: true,
        type: 'edit',
      },
      {
        // value: '765,60 ',
        width: '8.5%',
        type: 'text',
        border: true,
        dependent: 7,
      },
      {
        // value: '2.965,60',
        width: '9%',
        type: 'text',
        pl: '3%',
        dependent: 7,
      },
    ],
  },
];

export const data2 = [
  {
    selected: false,
    columns: [
      {
        value: 0,
        width: '4%',
        type: 'text',
      },
      {
        value: 'Adriano Reis Ferreira',
        width: '18%',
        type: 'text',
      },
      {
        value: 'Garçom',
        width: '10%',
        type: 'text',
      },
      {
        value: '2020',
        width: '6%',
        type: 'text',
      },
      {
        value: '761,77',
        width: '10%',
        type: 'toggle',
        border: true,
      },
      // retido
      {
        value: '203.189,00',
        width: '8.5%',
        type: 'text',
        border: true,
      },
      {
        value: '761,77',
        width: '8.5%',
        type: 'text',
        border: true,
      },
      {
        value: '761,77',
        width: '9%',
        type: 'text',
        border: true,
      },

      // despendido
      {
        value: '2.200,00',
        width: '8.5%',
        border: true,
        type: 'text',
      },
      {
        value: '765,60 ',
        width: '8.5%',
        type: 'text',
        border: true,
      },
      {
        value: '2.965,60',
        width: '9%',
        type: 'text',
        pl: '3%',
      },
    ],
  },
];
