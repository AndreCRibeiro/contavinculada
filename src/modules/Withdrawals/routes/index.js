import React from 'react';
import { Route } from 'react-router-dom';
import { WrapContainer } from '../../../components/Organisms';

import ListWithdrawals from '../pages/ListWithdrawals';
import CreateWithdrawl from '../pages/CreateWithdrawals';

const Routes = () => {
  return (
    <>
      <Route
        path="/withdrawl/list"
        exact
        component={() => (
          <WrapContainer>
            <ListWithdrawals />
          </WrapContainer>
        )}
      />
      <Route
        path="/withdrawl/create"
        exact
        component={() => (
          <WrapContainer>
            <CreateWithdrawl />
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
