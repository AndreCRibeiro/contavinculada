import { formatReal } from '../../../utils';

export default (data) => {
  return data?.map((item) => ({
    selected: false,
    columns: [
      {
        value: item.colaborador,
        width: '50%',
        type: 'text',
      },
      {
        value: `R$ ${formatReal(item.subtotal)}`,
        width: '50%',
        type: 'text',
      },
    ],
  }));
};
