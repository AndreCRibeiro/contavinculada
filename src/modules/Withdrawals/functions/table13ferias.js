import { each, isString } from 'lodash';
import immer from 'immer';
import { formated, formatedFloat } from '../../../utils';

const tratamentoString = (str) => {
  const newStr = isString(str) ? str.replace('.', '').replace(',', '.') : str;

  return newStr;
};

// 13 E FERIAS
export const total13Ferias = (array, rubrica) => {
  let totalCalc = 0.0;
  let totalEncargos = 0.0;
  const key =
    rubrica === 'ferias'
      ? 'total_ferias_retido_no_periodo'
      : 'total_13salario_retido_no_periodo';
  each(array, function (item) {
    totalCalc += item[key];
    totalEncargos += item.total_encargos_retido_no_periodo;
  });
  return {
    total: totalCalc > 0.0 ? totalCalc : 0,
    totalEncargos: totalEncargos > 0.0 ? totalEncargos : 0,
  };
};

export const totalDespendido13ferias = (columnEdit, array) => {
  let totalCalc = null;
  let totalEncargos = 0.0;
  each(array, function (item) {
    totalCalc += parseFloat(
      item.columns[columnEdit]?.value
        ? formatedFloat(item.columns[columnEdit]?.value)
        : 0.0
    );
    totalEncargos += parseFloat(
      item.columns[columnEdit + 1]?.value
        ? formatedFloat(item.columns[columnEdit + 1]?.value)
        : 0.0
    );
  });

  return {
    total: isNaN(totalCalc) ? 0.0 : totalCalc,
    totalEncargos: isNaN(totalEncargos) ? 0.0 : totalEncargos,
  };
};

export const total13FeriasALiberar = (
  array,
  rubrica,
  dataTableArray,
  dataTableArray2
) => {
  let totalCalc = 0.0;
  let totalEncargos = 0.0;

  /*
    somar todos 13 dos colaboradores mas se houver uma troca no array da tabela
    deve subtistuir o colaborador
  */

  each(dataTableArray2, (item) => {
    totalEncargos += parseFloat(formatedFloat(item?.columns[6].value));
    totalCalc += parseFloat(formatedFloat(item?.columns[5].value));
  });

  /* each(array, (item) => {
    const element = dataTableArray.find((el) => el.item.id === item.id);
    const elementTable2 = dataTableArray2.find((el) => el.item.id === item.id);
    if (elementTable2?.columns[6]?.value === 0) {
      totalEncargos += parseFloat(0.0);
      totalCalc += item[key];
      return;
    }
    totalCalc += parseFloat(
      formatedFloat(element?.columns[7].value || item[key])
    );

    totalEncargos += parseFloat(
      formatedFloat(
        element?.columns[8]?.value || item.total_encargos_retido_no_periodo
      )
    );
  }); */

  return {
    total: totalCalc > 0.0 ? totalCalc : 0.0,
    totalEncargos: totalEncargos > 0.0 ? totalEncargos : 0.0,
  };
};

export const total13FeriasSaldo = (array, rubrica) => {
  let totalCalc = 0.0;
  let totalEncargos = 0.0;
  // const key = rubrica === 'ferias' ? 'saldoFerias' : 'saldo13salario';
  each(array, function (item) {
    totalCalc += parseFloat(tratamentoString(item.columns[8].value));
    totalEncargos += parseFloat(tratamentoString(item.columns[9].value));
  });

  return {
    total: totalCalc > 0.0 ? totalCalc : 0.0,
    totalEncargos: totalEncargos > 0.0 ? totalEncargos : 0.0,
  };
};

// table
export const parseSecondTable13Ferias = (dataTableArray, rubrica) => {
  const keyRubrica =
    rubrica === 'ferias'
      ? 'total_ferias_retido_no_periodo'
      : 'total_13salario_retido_no_periodo';
  const keyEncargos = 'total_encargos_retido_no_periodo';
  /*
  console.log(`${keyRubrica} -> `, dataTableArray[0].item[keyRubrica]);
  console.log(`array -> `, dataTableArray); */
  /* const keySubtotal =
    rubrica === 'ferias' ? 'total_ferias_retido' : 'total_13salario_retido'; */
  return immer(dataTableArray, (draftState) => {
    each(dataTableArray, (item, index) => {
      draftState[index].columns = [
        {
          value: draftState[index].columns[0].value,
          width: '4%',
          type: 'text',
        },
        {
          value: draftState[index].columns[1].value,
          width: '18%',
          type: 'text',
        },
        {
          value: draftState[index].columns[2].value,
          width: '10%',
          type: 'text',
        },
        {
          value: draftState[index].columns[3].value,
          width: '6%',
          type: 'text',
        },
        {
          value: false,
          width: '10%',
          type: 'toggle',
          border: true,
        },
        // a Liberar
        // 13
        {
          value: formated(
            formatedFloat(
              tratamentoString(
                draftState[index].columns[7].value ||
                draftState[index].columns[4].value
              )
            )
          ),
          width: '8.5%',
          type: 'text',
          border: true,
        },
        // encargos 9 -7
        {
          value: formated(
            formatedFloat(
              draftState[index].columns[8].value ||
              draftState[index].columns[5].value
            )
          ),
          width: '8.5%',
          type: 'text',
          border: true,
        },
        // subtotal
        {
          value: draftState[index].columns[9].value
            ? formated(formatedFloat(draftState[index].columns[9].value))
            : draftState[index].columns[6].value,
          width: '9%',
          type: 'text',
          border: true,
        },

        // Saldo
        {
          value: formated(
            item.item[keyRubrica] -
            parseFloat(
              tratamentoString(
                draftState[index].columns[7].value ||
                draftState[index].columns[4].value
              )
            )
          ),
          width: '8.5%',
          border: true,
          type: 'text',
          noNegative: true,
        },
        {
          // 9 - 7
          value: formated(
            item.item[keyEncargos] -
            parseFloat(
              (draftState[index].columns[8].value &&
                formatedFloat(draftState[index].columns[8].value)) ||
              formatedFloat(draftState[index].columns[5].value)
            )
          ),
          width: '8.5%',
          type: 'text',
          border: true,
        },
        {
          value: formated(
            item.item[keyRubrica] -
            parseFloat(
              tratamentoString(
                draftState[index].columns[7].value ||
                draftState[index].columns[4].value
              )
            ) +
            (item.item[keyEncargos] -
              parseFloat(
                draftState[index].columns[8].value ||
                formatedFloat(draftState[index].columns[5].value)
              ))
          ),
          width: '9%',
          type: 'text',
          pl: '2%',
        },
      ];
    });
  });
};
