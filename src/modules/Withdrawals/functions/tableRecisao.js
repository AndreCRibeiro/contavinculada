import { each } from 'lodash';
import immer from 'immer';
import { formated, formatedFloat } from '../../../utils';
// RECISÃO
// TODO: so precisa chamar uma unica vez
export const totalRecisao = (array) => {
  let total13 = 0.0;
  let totalFerias = 0.0;
  let totalEncargos = 0.0;
  let totalFGTS = 0.0;
  each(array, function (item) {
    total13 += item.atual_13salario;
    totalFerias += item.atual_ferias;
    totalEncargos += item.atual_encargos;
    totalFGTS += item.atual_fgts;
  });

  return {
    total13: isNaN(total13) ? 0.0 : total13,
    totalFerias: isNaN(totalFerias) ? 0.0 : totalFerias,
    totalEncargos: isNaN(totalEncargos) ? 0.0 : totalEncargos,
    totalFGTS: isNaN(totalFGTS) ? 0.0 : totalFGTS,
  };
};

export const totalRecisaoDespendido = (array, dataTable) => {
  let total13 = 0.0;
  let totalFerias = 0.0;
  let totalEncargos = 0.0;
  let totalFGTS = 0.0;
  each(array, function (item, index) {
    total13 += parseFloat(
      dataTable[index]?.columns[8]?.value
        ? formatedFloat(dataTable[index]?.columns[8]?.value)
        : 0.0
    );
    totalFerias += parseFloat(
      dataTable[index]?.columns[9]?.value
        ? formatedFloat(dataTable[index]?.columns[9]?.value)
        : 0.0
    );
    totalFGTS += parseFloat(
      dataTable[index]?.columns[10]?.value
        ? formatedFloat(dataTable[index]?.columns[10]?.value)
        : 0.0
    );
    totalEncargos += parseFloat(
      dataTable[index]?.columns[11]?.value
        ? formatedFloat(dataTable[index]?.columns[11]?.value)
        : 0.0
    );
  });

  return {
    total13: isNaN(total13) ? 0.0 : total13,
    totalFerias: isNaN(totalFerias) ? 0.0 : totalFerias,
    totalEncargos: isNaN(totalEncargos) ? 0.0 : totalEncargos,
    totalFGTS: isNaN(totalFGTS) ? 0.0 : totalFGTS,
  };
};

export const totalRecisaoAliberar = (array, dataTable, dataTableArray2) => {
  let total13 = 0.0;
  let totalFerias = 0.0;
  let totalEncargos = 0.0;
  let totalFGTS = 0.0;

  each(dataTableArray2, ({ columns }) => {
    total13 += parseFloat(formatedFloat(columns[4].value));
    totalFerias += parseFloat(formatedFloat(columns[5].value));
    totalFGTS += parseFloat(formatedFloat(columns[6].value));
    totalEncargos += parseFloat(formatedFloat(columns[7].value));
  });

  return {
    total13: isNaN(total13) ? 0.0 : total13,
    totalFerias: isNaN(totalFerias) ? 0.0 : totalFerias,
    totalEncargos: isNaN(totalEncargos) ? 0.0 : totalEncargos,
    totalFGTS: isNaN(totalFGTS) ? 0.0 : totalFGTS,
  };
};

export const totalRecisaoSaldo = (array, dataTableArray) => {
  let total13 = 0.0;
  let totalFerias = 0.0;
  let totalEncargos = 0.0;
  let totalFGTS = 0.0;
  let totalSubtotal = 0.0;
  each(dataTableArray, ({ columns }) => {
    total13 += parseFloat(formatedFloat(columns[9].value));
    totalFerias += parseFloat(formatedFloat(columns[10].value));
    totalFGTS += parseFloat(formatedFloat(columns[11].value));
    totalEncargos += parseFloat(formatedFloat(columns[12].value));
    totalSubtotal += parseFloat(formatedFloat(columns[13].value));
  });

  return {
    total13,
    totalFerias,
    totalEncargos,
    totalFGTS,
    totalSubtotal,
  };
};

const encargosDespendidoRecisao = (
  despendido13,
  despendidoFerias,
  total13,
  totalFerias,
  totalEncargos
) => {
  const decimoTerceiro = parseFloat(formatedFloat(despendido13 || total13));
  const ferias = parseFloat(formatedFloat(despendidoFerias || totalFerias));
  const encargos = parseFloat(formatedFloat(totalEncargos));

  if (!(total13 + totalFerias)) return 0.0;
  const result =
    (decimoTerceiro + ferias) * (encargos / (total13 + totalFerias));

  return result;
};

export const parseSecondTableRecisao = (dataTableArray) => {
  return immer(dataTableArray, (draftState) => {
    each(dataTableArray, (_, index) => {
      const encargos = encargosDespendidoRecisao(
        draftState[index].columns[8].value || 0.0,
        draftState[index].columns[9].value || 0.0,
        dataTableArray[index].item.atual_13salario,
        dataTableArray[index].item.atual_ferias,
        dataTableArray[index].item.atual_encargos
      ).toFixed(2);

      draftState[index].columns = [
        {
          value: draftState[index].columns[0].value,
          width: '3%',
          type: 'text',
        },
        {
          value: draftState[index].columns[1].value,
          width: '14%',
          type: 'text',
        },
        {
          value: draftState[index].columns[2].value,
          width: '8%',
          type: 'text',
        },
        {
          value: false,
          width: '8%',
          type: 'toggle',
          border: true,
        },

        /*
        13 - 8
        ferias - 9
        fgts - 10
        encargos - 11
        subtotal - 12
      */
        // retido - REGRA DE NEGOCIO
        // 13
        {
          value: draftState[index].columns[8].value
            ? formated(formatedFloat(draftState[index].columns[8].value))
            : draftState[index].columns[3].value,
          width: '8.5%',
          type: 'text',
          border: true,
        },
        // ferias
        {
          value: draftState[index].columns[9].value
            ? formated(formatedFloat(draftState[index].columns[9].value))
            : draftState[index].columns[4].value,
          width: '8.5%',
          type: 'text',
          border: true,
        },
        // fgts
        {
          value: draftState[index].columns[10].value
            ? formated(formatedFloat(draftState[index].columns[10].value))
            : draftState[index].columns[5].value,
          width: '8.5%',
          type: 'text',
          border: true,
        },
        // encargos
        {
          value: draftState[index].columns[11].value
            ? formated(formatedFloat(encargos))
            : draftState[index].columns[6].value,
          width: '8.5%',
          type: 'text',
          border: true,
        },
        // subtotal
        {
          value: draftState[index].columns[12].value
            ? formated(formatedFloat(draftState[index].columns[12].value))
            : draftState[index].columns[7].value,
          width: '9%',
          type: 'text',
          pl: '1.5%',
          // border: true,
        },
        // ------ SALDO
        // Saldo 13
        {
          value: formated(
            formatedFloat(
              draftState[index].columns[8].value
                ? formatedFloat(draftState[index].columns[3].value) -
                formatedFloat(draftState[index].columns[8].value)
                : 0.0
            )
          ),
          width: '8.5%',
          border: true,
          type: 'text',
          noNegative: true,
        },
        // Saldo FERIAS
        {
          value: formated(
            formatedFloat(
              draftState[index].columns[9].value
                ? formatedFloat(draftState[index].columns[4].value) -
                formatedFloat(draftState[index].columns[9].value)
                : 0.0
            )
          ),
          width: '8.5%',
          border: true,
          type: 'text',
          noNegative: true,
        },
        // Saldo FGTS
        {
          value: formated(
            formatedFloat(
              draftState[index].columns[10].value
                ? formatedFloat(draftState[index].columns[5].value) -
                formatedFloat(draftState[index].columns[10].value)
                : 0.0
            )
          ),
          width: '8.5%',
          border: true,
          type: 'text',
          noNegative: true,
        },
        // Saldo Encargos
        {
          value: formated(
            formatedFloat(draftState[index].columns[6].value) -
            formatedFloat(encargos)
          ),
          width: '8.5%',
          type: 'text',
          border: true,
        },
        // Saldo total
        {
          value: formated(
            // 13 saldo
            parseFloat(
              formatedFloat(
                draftState[index].columns[8].value
                  ? formatedFloat(draftState[index].columns[3].value) -
                  formatedFloat(draftState[index].columns[8].value)
                  : 0.0
              )
            ) +
            // ferias
            parseFloat(
              formatedFloat(
                draftState[index].columns[9].value
                  ? formatedFloat(draftState[index].columns[4].value) -
                  formatedFloat(draftState[index].columns[9].value)
                  : 0.0
              )
            ) +
            // fgts
            parseFloat(
              formatedFloat(
                parseFloat(
                  draftState[index].columns[10].value
                    ? formatedFloat(draftState[index].columns[5].value) -
                    formatedFloat(draftState[index].columns[10].value)
                    : 0.0
                )
              )
            ) +
            // encargos
            parseFloat(
              formatedFloat(draftState[index].columns[6].value) -
              formatedFloat(encargos)
            )
          ),
          width: '9%',
          type: 'text',
          pl: '1.5%',
        },
      ];
    });
  });
};
