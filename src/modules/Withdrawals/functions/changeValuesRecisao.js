import immer from 'immer';
import { formatedFloat, formated } from '../../../utils';

const testNumber = (number) => {
  console.log({ number });
  return isNaN(number) ? 0.0 : number;
};

const changeValuesRecisao = ({
  prev,
  lineIndex,
  columnIndex,
  value: valueNotFormated,
  metadata,
}) => {
  const encargoDespendido = (
    valordespendido,
    encargos,
    salario13,
    ferias,
    log
  ) => {
    const result =
      (parseFloat(formatedFloat(valordespendido)) *
        parseFloat(formatedFloat(encargos))) /
      (parseFloat(formatedFloat(salario13)) +
        parseFloat(formatedFloat(ferias)));
    return result;
  };

  const value = testNumber(formatedFloat(valueNotFormated));

  /*
    vai ocorrre o valor despendido 2x -> 13salario, Ferias
    e o encargo e a soma dos dois
    13 -> 3 normal; coluna 8 (despendido)
    ferias -> 4 normal; coluna 9 (despendido)
    encargos -> coluna 11
  */
  const dataTable = immer(prev, (draftState) => {
    if (columnIndex === 8 || columnIndex === 9) {
      let encargos13 = 0.0;
      let encargosFerias = 0.0;
      const currentValue13 = draftState[lineIndex].columns[8].value;
      const currentValueFerias = draftState[lineIndex].columns[9].value;

      // atual encargos na tabela
      const currentEncagos13 = testNumber(
        encargoDespendido(
          currentValue13,
          draftState[lineIndex].item.atual_encargos,
          draftState[lineIndex].columns[3].value,
          draftState[lineIndex].columns[4].value,
          true
        )
      );
      const currentEncagosFerias = testNumber(
        encargoDespendido(
          currentValueFerias,
          draftState[lineIndex].item.atual_encargos,
          draftState[lineIndex].columns[3].value,
          draftState[lineIndex].columns[4].value,
          true
        )
      );
      const totalCurrentEncargos = currentEncagos13 + currentEncagosFerias;

      // lida com dados de 13 salario
      if (columnIndex === 8) {
        encargos13 = encargoDespendido(
          value,
          draftState[lineIndex].item.atual_encargos,
          draftState[lineIndex].columns[3].value,
          draftState[lineIndex].columns[4].value
        );
        draftState[lineIndex].columns[11].value =
          totalCurrentEncargos - currentEncagos13 + encargos13 || null;
        if (encargos13 === 0)
          draftState[lineIndex].columns[11].value = testNumber(
            currentEncagosFerias
          );
      }

      // lida com dados de Ferias
      if (columnIndex === 9) {
        encargosFerias = encargoDespendido(
          value,
          draftState[lineIndex].item.atual_encargos,
          draftState[lineIndex].columns[3].value,
          draftState[lineIndex].columns[4].value
        );
        draftState[lineIndex].columns[11].value =
          totalCurrentEncargos - currentEncagosFerias + encargosFerias || null;
        if (encargosFerias === 0)
          draftState[lineIndex].columns[11].value = currentEncagos13;
      }

      draftState[lineIndex].columns[11].value = formated(
        draftState[lineIndex].columns[11].value
      );

      /*   draftState[lineIndex].columns[11].value =
        encargos13 +
        encargosFerias +
        (draftState[lineIndex].columns[11].value || 0.0) || null; */
    }

    const subtotal = formated(
      // 13
      (parseFloat(
        formatedFloat(
          (columnIndex === 8
            ? value || 0.0
            : draftState[lineIndex].columns[8].value) || 0.0
        )
      ) || 0.0) + // ferias
      (parseFloat(
        formatedFloat(
          (columnIndex === 9
            ? value
            : draftState[lineIndex].columns[9].value) || 0.0
        )
      ) || 0.0) + // fgts
      (parseFloat(
        formatedFloat(
          (columnIndex === 10
            ? value || 0.0
            : draftState[lineIndex].columns[10].value) || 0.0
        )
      ) || 0.0) + // encargos
      parseFloat(
        formatedFloat(draftState[lineIndex].columns[11].value || 0.0)
      )
    );
    draftState[lineIndex].columns[12].value = subtotal;

    draftState[lineIndex].columns[columnIndex].value =
      value === '' ? null : valueNotFormated;
  });

  return {
    dataTable,
    backupData: dataTable,
  };
};

export default changeValuesRecisao;
