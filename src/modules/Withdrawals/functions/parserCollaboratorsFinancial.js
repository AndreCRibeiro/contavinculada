import { formated } from '../../../utils';

export default (data, rubrica) => {
  return data?.map((item, i) => ({
    selected: false,
    item,
    columns: [
      {
        value: i + 1,
        width: '4%',
        type: 'text',
      },
      {
        value: item.colaborador,
        width: '23%',
        type: 'text',
      },
      {
        value: item.posto,
        width: '15%',
        type: 'text',
      },
      {
        value:
          item?.ano_retirada ||
          `${item.inicio_per_aquisitivo}/${item.fim_per_aquisitivo}`,
        width: '6%',
        type: 'text',
      },
      // retido
      {
        value: formated(
          item.total_13salario_retido_no_periodo ||
            item.total_13salario_retido_no_periodo === 0
            ? item.total_13salario_retido_no_periodo
            : item.total_ferias_retido_no_periodo
        ),
        width: '8.5%',
        type: 'text',
        border: true,
      },
      {
        value: formated(item.total_encargos_retido_no_periodo),
        width: '8.5%',
        type: 'text',
        border: true,
      },
      {
        value: formated(
          item.total_13salario_retido_no_periodo +
            item.total_encargos_retido_no_periodo ===
            0
            ? item.total_13salario_retido_no_periodo +
            item.total_encargos_retido_no_periodo
            : item.subtotal_no_periodo
        ),
        width: '9%',
        type: 'text',
        border: true,
      },
      // despendido
      {
        // value: '2.200,00',
        width: '8.5%',
        border: true,
        type: 'edit',
        limit:
          item.total_13salario_retido_no_periodo ||
            item.total_13salario_retido_no_periodo === 0
            ? item.total_13salario_retido_no_periodo
            : item.total_ferias_retido_no_periodo,
      },
      {
        // value: '765,60 ',
        width: '8.5%',
        type: 'text',
        border: true,
        dependent: 7,
      },
      {
        // value: '2.965,60',
        width: '9%',
        type: 'text',
        pl: '2%',
        dependent: 7,
      },
    ],
  }));
};
