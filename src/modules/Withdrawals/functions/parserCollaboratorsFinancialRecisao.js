import { formated } from '../../../utils';

export default (data, contract) => {
  // const timeZone = 'America/Brasilia';
  return data?.map((item, i) => ({
    selected: false,
    item,
    columns: [
      {
        value: i + 1,
        width: '3%',
        type: 'text',
      },
      {
        value: item.colaborador,
        width: '15%',
        type: 'text',
      },
      {
        value: item.posto,
        width: '9%',
        type: 'text',
      },
      // retido
      {
        value: formated(item.atual_13salario),
        width: '7.2%',
        type: 'text',
        border: true,
      },
      {
        value: formated(item.atual_ferias),
        width: '7.2%',
        type: 'text',
        border: true,
      },
      {
        value: formated(item.atual_fgts), // fgts
        width: '7.2%',
        type: 'text',
        border: true,
      },
      {
        value: formated(item.atual_encargos), // encargos
        width: '7.2%',
        type: 'text',
        border: true,
      },
      {
        value: formated(item.total_geral), // subtotal
        width: '7.2%',
        type: 'text',
        border: true,
      },
      // despendido
      {
        width: '7.2%',
        type: 'edit',
        limit: item.atual_13salario,
      },
      {
        width: '7.2%',
        type: 'edit',
        limit: item.atual_ferias,
      },
      {
        width: '7.2%',
        border: true,
        type: 'edit',
        limit: item.atual_fgts,
      },
      {
        width: '7.2%',
        type: 'text',
        border: true,
        dependent: 7,
      },
      {
        // value: '2.965,60',
        width: '7.2%',
        type: 'text',
        pl: '1.5%',
        dependent: 7,
      },
    ],
  }));
};
