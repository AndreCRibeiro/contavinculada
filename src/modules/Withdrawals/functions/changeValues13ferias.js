import immer from 'immer';
import { formatedFloat, formated } from '../../../utils';

const changeValues13ferias = ({
  prev,
  lineIndex,
  columnIndex,
  dataTableArray,
  value: valueNotFormated,
  metadata,
}) => {
  const value = formatedFloat(valueNotFormated);
  const encargoDespendido = (valordespendido, encargos, valornormal) =>
    parseFloat(
      (parseFloat(valordespendido) * encargos) /
      parseFloat(valornormal.replace('.', '').replace(',', '.'))
    );

  // e o index do array inteiro e nao do array filtrado por 10
  const indexGeneralLine = prev.findIndex(
    (el) =>
      el.item.id_colaborador === dataTableArray[lineIndex].item.id_colaborador
  );

  const dataTable = immer(prev, (draftState) => {
    const encargos = encargoDespendido(
      value,
      prev[indexGeneralLine].item.total_encargos_retido_no_periodo,
      prev[indexGeneralLine].columns[4].value
    );
    console.log({
      encargos,
      valorDespendido: value,
      encargosqueveiodobanco:
        draftState[lineIndex].item.total_encargos_retido_no_periodo,
      valorRubrica: draftState[lineIndex].columns[4].value,
    });
    const subtotal = parseFloat(value) + encargos;
    // 13 ferias
    draftState[indexGeneralLine].columns[columnIndex].value =
      value === '' ? null : valueNotFormated;
    // encargos
    draftState[indexGeneralLine].columns[columnIndex + 1].value =
      encargos > 0 ? formated(encargos) : null;
    // subtotal
    draftState[indexGeneralLine].columns[columnIndex + 2].value =
      subtotal > 0 ? formated(subtotal) : null;
  });

  const backupData = immer(prev, (draftState) => {
    const encargos = parseFloat(value) * metadata.encargos;
    const subtotal = parseFloat(value) + encargos;
    draftState[indexGeneralLine].columns[columnIndex].value =
      value === '' ? null : parseFloat(value);
    draftState[indexGeneralLine].columns[columnIndex + 1].value =
      encargos > 0 ? encargos.toFixed(2) : null;
    draftState[indexGeneralLine].columns[columnIndex + 2].value =
      subtotal > 0 ? subtotal.toFixed(2) : null;
  });

  return {
    dataTable,
    backupData,
  };
};

export default changeValues13ferias;
