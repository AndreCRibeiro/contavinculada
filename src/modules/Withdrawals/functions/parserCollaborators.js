export default (data) => {
  // const timeZone = 'America/Brasilia';
  return data?.map((item, i) => ({
    selected: false,
    item,
    columns: [
      {
        value: i + 1,
        width: '5%',
        type: 'text',
      },
      {
        value: item.name,
        width: '50%',
        type: 'text',
      },
      {
        value: item.dadosCargo.nome_cargo,
        width: '50%',
        type: 'text',
      },
    ],
  }));
};
