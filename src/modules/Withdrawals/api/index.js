import api from '../../../services/api';

const WithdrawalsEndpoints = {
  postPreWithdrawals: (data) => api.post('/preliberacao', data),
  postWithdrawals: (data) => api.post('/liberacao', data),
  postCalculate: (data) => api.post('/calcliberacao', data),
  getList13: (data) =>
    api.get(`/liberacao/${data.idContrato}/${data.rubrica}?ano=${data.data}`),
  getListRecisao: (data) =>
    api.get(`/liberacao/${data.idContrato}/${data.rubrica}`),
  getListFerias: (data) =>
    api.get(
      `/liberacao/${data.idContrato}/${data.rubrica}?inicio=${data.data}&fim=${data.dataFinal}`
    ),
};

export default WithdrawalsEndpoints;
