import React, { useEffect } from 'react';

import { label } from '../mocks/collaborators';
import parser from '../functions/parserCollaborators';
import { useTable } from '../../../hooks';

import * as Atom from '../../../components/Atoms';
// import * as Molecules from '../../../components/Molecules';
import * as Organisms from '../../../components/Organisms';

export const SupervisorFlow = ({
  dataCollaborators,
  handleCollaboratorsSel,
}) => {
  const {
    offset,
    dataTable,
    selectAll,
    numberOfPage,
    dataTableArray,
    nextPage,
    prevPage,
    selectPage,
    // handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(dataCollaborators ? parser(dataCollaborators) : []);

  useEffect(() => {
    const selectedColaboratos = dataTable.filter((item) => item.selected);
    handleCollaboratorsSel(selectedColaboratos);
  }, [dataTable, selectAll, handleCollaboratorsSel]);

  return (
    !!dataCollaborators && (
      <>
        <Atom.Container variant="lineCreate">
          <Organisms.Tables
            labels={label}
            data={dataTableArray || []}
            selectAll={selectAll}
            noOptions
            onClickSelectAll={handleSelectAll}
            onClickSelectItem={handleSelectItem}
          />
        </Atom.Container>
        <Atom.Container variant="lineCreate">
          <Organisms.FooterList
            pages={numberOfPage}
            currentPage={offset}
            onNextPage={nextPage}
            onPrevPage={prevPage}
            onSelectPage={selectPage}
          />
        </Atom.Container>
      </>
    )
  );
};
