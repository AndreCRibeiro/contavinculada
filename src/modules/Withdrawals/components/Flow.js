/* eslint-disable eqeqeq */
import React from 'react';
import { SupervisorFlow } from './supervisor';
import { FinancialFlow } from './financial';
import { ManagerFlow } from './manager';

export default ({
  user,
  dataCollaborators,
  setColaboratosSel,
  dataContract,
  idContract,
  rubrica,
}) => {
  if (user?.type === 'fiscal') {
    return (
      <SupervisorFlow
        handleCollaboratorsSel={setColaboratosSel}
        dataCollaborators={dataCollaborators}
        rubrica={rubrica}
      />
    );
  }
  if (user?.type === 'financeiro' && dataCollaborators) {
    return (
      <FinancialFlow
        contract={
          dataContract.filter((contract) => contract.id == idContract)[0]
        }
        rubrica={rubrica}
        dataCollaborators={dataCollaborators}
      />
    );
  }
  if (user?.type === 'gestor' || user?.type === 'admin') {
    return <ManagerFlow dataCollaborators={dataCollaborators} />;
  }
  return null;
};
