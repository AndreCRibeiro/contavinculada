import React from 'react';

import { data, label } from '../mocks/financial2';
import { useTable } from '../../../hooks';

import * as Atom from '../../../components/Atoms';
import * as Organisms from '../../../components/Organisms';
import * as Molecules from '../../../components/Molecules';

export const ManagerFlow = ({ dataCollaborators }) => {
  const {
    offset,
    dataTable,
    selectAll,
    nextPage,
    prevPage,
    selectPage,
    handleSelectAll,
    handleSelectItem,
  } = useTable(data);

  return (
    data && (
      <>
        <Atom.Container variant="divider" />
        <Atom.Container variant="lineCreate">
          <Atom.Container width="45%" height="18px" />
          <Molecules.Button variant="delete" text="RECUSAR LIBERAÇÃO" />
        </Atom.Container>

        <Atom.Container variant="lineCreate" mt={25}>
          <Atom.Container width="45%" height="18px" />
          <Atom.Text backgroundColor="#7cc1708f" width="30%" textAlign="center">
            Valor Retido
          </Atom.Text>
          <Atom.Text backgroundColor="#d3e7318f" width="29%" textAlign="center">
            Valor a Liberar
          </Atom.Text>
        </Atom.Container>
        <Atom.Container variant="lineCreate">
          <Organisms.Tables
            mt={1}
            labels={label}
            data={dataTable || []}
            selectAll={selectAll}
            noOptions
            noSelect
            onClickSelectAll={handleSelectAll}
            onClickSelectItem={handleSelectItem}
          />
        </Atom.Container>
        <Atom.Container variant="lineCreate">
          <Atom.Text
            px="20px"
            width="45%"
            textAlign="right"
            variant="buttonOutline"
          >
            TOTAL GERAL
          </Atom.Text>
          <Atom.Text
            width="10%"
            textAlign="center"
            borderRight="1px solid"
            variant="resultTable"
            borderRadius={4}
          >
            13
          </Atom.Text>
          <Atom.Text
            width="10%"
            textAlign="center"
            borderRight="1px solid"
            variant="resultTable"
          >
            encargos
          </Atom.Text>
          <Atom.Text
            width="10%"
            textAlign="center"
            borderRight="1px solid"
            variant="resultTable"
          >
            total
          </Atom.Text>
          <Atom.Text
            width="10%"
            textAlign="center"
            borderRight="1px solid"
            variant="resultTable"
          >
            13
          </Atom.Text>
          <Atom.Text
            width="10%"
            textAlign="center"
            borderRight="1px solid"
            variant="resultTable"
          >
            encargos
          </Atom.Text>
          <Atom.Text
            width="9%"
            textAlign="center"
            borderRight="1px solid"
            variant="resultTable"
            borderRadius={4}
          >
            total
          </Atom.Text>
        </Atom.Container>
        <Atom.Container variant="lineCreate">
          <Atom.Container />
          <Organisms.FooterList
            pages={2}
            currentPage={offset}
            onNextPage={nextPage}
            onPrevPage={prevPage}
            onSelectPage={selectPage}
          />
          <Atom.Container />
        </Atom.Container>
      </>
    )
  );
};
