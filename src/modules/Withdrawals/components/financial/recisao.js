import React from 'react';
import * as Atom from '../../../../components/Atoms';
import {
  totalRecisao,
  totalRecisaoDespendido,
  totalRecisaoAliberar,
  totalRecisaoSaldo,
} from '../../functions/tableRecisao';
import { formated } from '../../../../utils';

export default ({
  dataCollaborators,
  dataTableArray,
  dataTableArray2,
  rubrica,
  table,
}) => {
  const total1 =
    table === 2
      ? totalRecisaoAliberar(dataCollaborators, dataTableArray, dataTableArray2)
      : totalRecisao(dataCollaborators);
  const total2 =
    table === 2
      ? totalRecisaoSaldo(dataCollaborators, dataTableArray2)
      : totalRecisaoDespendido(dataCollaborators, dataTableArray);
  return (
    rubrica === 'rescisao' && (
      <>
        <Atom.Text
          width="7.2%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          borderRadius={4}
          bg="#189273"
          fontWeight="bold"
        >
          {formated(total1.total13)}
        </Atom.Text>
        <Atom.Text
          width="7.2%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          borderRadius={4}
          fontWeight="bold"
        >
          {formated(total1.totalFerias)}
        </Atom.Text>
        <Atom.Text
          width="7.2%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          borderRadius={4}
          fontWeight="bold"
        >
          {formated(total1.totalFGTS)}
        </Atom.Text>
        <Atom.Text
          width="7.2%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          borderRadius={4}
          fontWeight="bold"
        >
          {formated(total1.totalEncargos)}
        </Atom.Text>
        <Atom.Text
          width="7.2%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          borderRadius={4}
          fontWeight="bold"
        >
          {formated(
            total1.total13 +
            total1.totalFerias +
            total1.totalEncargos +
            total1.totalFGTS
          )}
        </Atom.Text>
        {/* parte dois */}
        <Atom.Text
          width="7.1%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          fontWeight="bold"
        >
          {formated(total2.total13)}
        </Atom.Text>
        <Atom.Text
          width="7.1%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          fontWeight="bold"
        >
          {formated(total2.totalFerias)}
        </Atom.Text>
        <Atom.Text
          width="7.1%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          fontWeight="bold"
        >
          {formated(total2.totalFGTS)}
        </Atom.Text>
        <Atom.Text
          width="6.9%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          fontWeight="bold"
        >
          {formated(total2.totalEncargos)}
        </Atom.Text>
        <Atom.Text
          width="8.1%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          borderRadius={4}
          fontWeight="bold"
        >
          {formated(
            total2.total13 +
            total2.totalFerias +
            total2.totalEncargos +
            total2.totalFGTS
          )}
        </Atom.Text>
      </>
    )
  );
};
