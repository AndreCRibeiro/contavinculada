/* eslint-disable no-unused-vars */
import React from 'react';
import immer from 'immer';
// import currency from 'currency-formatter';
import useBus, { dispatch } from 'use-bus';
import { toast } from 'react-toastify';
import { data, data2, label, label2 } from '../../mocks/financial';
import {
  label as labelRecisao,
  label2 as labelRecisao2,
} from '../../mocks/recisaoFinancial';
import { useTable, useAuth, useLazyFetch } from '../../../../hooks';
import parser from '../../functions/parserCollaboratorsFinancial';
import parserRecisao from '../../functions/parserCollaboratorsFinancialRecisao';
import * as Atom from '../../../../components/Atoms';
import * as Organisms from '../../../../components/Organisms';
import WithdrawalsEndpoints from '../../api';
import { formatedFloat } from '../../../../utils';

import { parseSecondTable13Ferias } from '../../functions/table13ferias';

import changeValuesRecisao from '../../functions/changeValuesRecisao';
import changeValues13ferias from '../../functions/changeValues13ferias';

import { parseSecondTableRecisao } from '../../functions/tableRecisao';

import RecisaoTotal from './recisao';
import Ferias13Total from './13ferias';
/*
 TODO
 1. criar funções de caçculos repetidos
 2. separar recisao de 13 e ferias
*/

export const FinancialFlow = ({
  dataCollaborators,
  contract,
  rubrica = '13º',
}) => {
  const { user } = useAuth();
  const currency = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
  });
  const d1 = [];
  const d2 = [];

  const [fetch, status] = useLazyFetch(WithdrawalsEndpoints.postWithdrawals);

  React.useEffect(() => {
    if (status.response && !status.error) {
      dispatch('@reset_withdral');
    }
  }, [status]);

  for (let i = 0; i < 10; i++) {
    d1.push(data[0]);
    d2.push(data2[0]);
  }

  const array =
    rubrica === 'rescisao'
      ? parserRecisao(dataCollaborators, contract)
      : parser(dataCollaborators, rubrica);

  const {
    trigger2,
    triggerOffset,
    offset,
    selectAll,
    numberOfPage,
    dataTableArray,
    dataTable,
    prevPage,
    nextPage,
    selectPage,
    handleSelectAll,
    handleSelectItem,
    handleChangeValue,
  } = useTable(array || [], '', {
    encargos: contract?.encargos / 100,
  });

  const table2 = useTable(
    rubrica === 'rescisao'
      ? parseSecondTableRecisao(dataTable)
      : parseSecondTable13Ferias(dataTable, rubrica) || [],
    trigger2
  );

  useBus(
    '@handle_send_financial',
    () => {
      // id financeiro
      // id contrato
      // 7 e 8{
      let errorCondional = false;
      const nextState = table2.dataTableArray.map((item, index) => {
        if (rubrica === 'rescisao') {
          return {
            ...item.item,
            id_contrato: contract.id,
            id_financeiro: user.id,
            inicio_per_aquisitivo: null,
            fim_per_aquisitivo: null,
            ano_retirada: null,
            decimo_terceiro: item.item.atual_13salario,
            ferias: item.item.atual_ferias,
            multa_fgts: item.item.atual_fgts,
            encargos: item.item.atual_encargos,
            subtotal: item.item.total_geral,
          };
        }

        const negative = /([-])/g;
        if (
          negative.test(item.columns[8].value) ||
          negative.test(item.columns[9].value) ||
          negative.test(item.columns[10].value) < 0
        ) {
          errorCondional = true;
        }

        return {
          ...item.item,
          inicio_per_aquisitivo: item.item.inicio_per_aquisitivo || null,
          fim_per_aquisitivo: item.item.fim_per_aquisitivo || null,
          ano_retirada: item.item.ano_retirada || null,
          id_contrato: contract.id,
          id_financeiro: user.id,
          decimo_terceiro:
            rubrica === '13salario'
              ? parseFloat(formatedFloat(item.columns[5].value))
              : null, // item.item.total_13salario_retido_no_periodo,
          total_13salario_retido: item.item.total_13salario_retido,
          ferias:
            rubrica === 'ferias'
              ? parseFloat(formatedFloat(item.columns[5].value))
              : null, // || item.item?.total_ferias_retido_no_periodo,
          encargos: parseFloat(formatedFloat(item.columns[6].value)),
          multa_fgts: item.item.fgts,
          subtotal: parseFloat(formatedFloat(item.columns[7].value)), // item.item.subtotal_no_periodo,
        };
      });

      const body = {
        id_contrato: contract.id,
        id_financeiro: user.id,
        rubrica,
        data: nextState,
      };

      if (errorCondional) {
        toast.warn('Não é possivel fazer liberações com saldo negativo!', {
          autoClose: 5000,
        });
      }

      fetch(body);
    },
    [contract, user, dataTableArray.length, array]
  );

  return (
    data && (
      <>
        <Atom.Container variant="divider" />

        <Atom.Container variant="lineCreate">
          <Atom.Container
            width={rubrica === 'rescisao' ? '28%' : '48%'}
            height="18px"
          />
          <Atom.Text
            backgroundColor="#0c80e98f"
            width={rubrica === 'rescisao' ? '36%' : '26%'}
            textAlign="center"
          >
            Saldo Atual
          </Atom.Text>
          <Atom.Text
            backgroundColor="#d3e7318f"
            width={rubrica === 'rescisao' ? '37%' : '26%'}
            textAlign="center"
          >
            Valor a Liberar(despendido)
          </Atom.Text>
        </Atom.Container>
        <Atom.Container variant="lineCreate" mb="1px">
          <Organisms.Tables
            mt={1}
            offset={offset}
            labels={rubrica === 'rescisao' ? labelRecisao : label(rubrica)}
            data={dataTableArray || []}
            dataTable={dataTable}
            selectAll={selectAll}
            noOptions
            noSelect
            onClickSelectAll={handleSelectAll}
            onClickSelectItem={handleSelectItem}
            onChangeValue={(line, column, value) =>
              handleChangeValue(
                line,
                column,
                value,
                rubrica === 'rescisao'
                  ? changeValuesRecisao
                  : changeValues13ferias
              )
            }
          />
        </Atom.Container>

        <Atom.Container variant="lineCreate">
          <Atom.Text
            px="2%"
            width={rubrica === 'rescisao' ? '27.5%' : '48%'}
            textAlign="right"
            variant="buttonOutline"
          >
            TOTAL GERAL
          </Atom.Text>
          {rubrica === 'rescisao' ? (
            <RecisaoTotal
              rubrica={rubrica}
              dataCollaborators={dataCollaborators}
              dataTableArray={dataTableArray}
            />
          ) : (
            <Ferias13Total
              rubrica={rubrica}
              dataCollaborators={dataCollaborators}
              dataTableArray={dataTableArray}
            />
          )}
        </Atom.Container>

        {/* SECOND TABLE */}
        <Atom.Container variant="lineCreate" mt="MEDIUM">
          <Atom.Container
            width={rubrica === 'rescisao' ? '13%' : '19%'}
            height="18px"
          />
          <Atom.Container height="18px">
            <Atom.ToggleSwitch
              allSelected={
                !table2.dataTable.find(
                  (el) =>
                    el.columns[rubrica === 'rescisao' ? 3 : 4].value === false
                )
              }
              handleToggleAll={table2.handleToggleAll}
            />
            <Atom.Text ml="SMALL" textAlign="center">
              Zerar encargos de todos os colaboradores
            </Atom.Text>
          </Atom.Container>

          <Atom.Container
            width={rubrica === 'rescisao' ? '40%' : '10%'}
            height="18px"
          />
        </Atom.Container>
        <Atom.Container variant="lineCreate">
          <Atom.Container
            width={rubrica === 'rescisao' ? '28%' : '48%'}
            height="18px"
          />
          <Atom.Text
            backgroundColor="#7cc1708f"
            width={rubrica === 'rescisao' ? '36%' : '26%'}
            textAlign="center"
          >
            Valor a Liberar
          </Atom.Text>
          <Atom.Text
            backgroundColor="#ffc0028f"
            width={rubrica === 'rescisao' ? '37%' : '26%'}
            textAlign="center"
          >
            Saldo
          </Atom.Text>
        </Atom.Container>
        <Atom.Container variant="lineCreate">
          <Organisms.Tables
            mt={1}
            labels={rubrica === 'rescisao' ? labelRecisao2 : label2(rubrica)}
            data={table2.dataTableArray || []}
            selectAll={table2.selectAll}
            noOptions
            noSelect
            onToggle={table2.handleToggle}
            onClickSelectAll={table2.handleSelectAll}
            onClickSelectItem={table2.handleSelectItem}
          />
        </Atom.Container>

        <Atom.Container variant="lineCreate">
          <Atom.Text
            px="2%"
            width={rubrica === 'rescisao' ? '28%' : '48.5%'}
            textAlign="right"
            variant="buttonOutline"
          >
            TOTAL GERAL
          </Atom.Text>
          {rubrica === 'rescisao' ? (
            <RecisaoTotal
              rubrica={rubrica}
              dataCollaborators={dataCollaborators}
              dataTableArray={dataTableArray}
              dataTableArray2={table2.dataTable}
              table={2}
            />
          ) : (
            <Ferias13Total
              rubrica={rubrica}
              dataCollaborators={dataCollaborators}
              dataTableArray={dataTableArray}
              dataTable={dataTable}
              dataTableArray2={table2.dataTable}
              table={2}
            />
          )}
        </Atom.Container>

        <Atom.Container variant="lineCreate">
          <Atom.Container />
          <Organisms.FooterList
            pages={numberOfPage}
            currentPage={offset}
            numberOfPage={numberOfPage}
            onNextPage={() => {
              nextPage();
              table2.nextPage();
            }}
            onPrevPage={() => {
              prevPage();
              table2.prevPage();
            }}
            onSelectPage={(newOffset) => {
              selectPage(newOffset);
              table2.selectPage(newOffset);
            }}
          />
          <Atom.Container />
        </Atom.Container>
      </>
    )
  );
};
