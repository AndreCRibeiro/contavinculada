import React from 'react';
import * as Atom from '../../../../components/Atoms';
import { formated } from '../../../../utils';
import {
  total13Ferias,
  total13FeriasALiberar,
  totalDespendido13ferias,
  total13FeriasSaldo,
} from '../../functions/table13ferias';

export default ({
  dataCollaborators,
  dataTableArray,
  dataTableArray2,
  dataTable,
  rubrica,
  table,
}) => {
  const total =
    table === 2
      ? total13FeriasALiberar(
        dataCollaborators,
        rubrica,
        dataTableArray,
        dataTableArray2
      )
      : total13Ferias(dataCollaborators, rubrica);
  const total2 =
    table === 2
      ? total13FeriasSaldo(dataTableArray2, rubrica)
      : totalDespendido13ferias(7, dataTableArray);

  return (
    rubrica !== 'rescisao' && (
      <>
        <Atom.Text
          width="8.5%"
          textAlign="center"
          borderRight="1px solid"
          fontWeight="bold"
          variant="resultTable"
          borderRadius={4}
        >
          {formated(total?.total)}
        </Atom.Text>
        <Atom.Text
          width="8.5%"
          textAlign="center"
          borderRight="1px solid"
          fontWeight="bold"
          variant="resultTable"
        >
          {formated(total?.totalEncargos)}
        </Atom.Text>
        <Atom.Text
          width="9%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          fontWeight="bold"
        >
          {formated(total?.total + total?.totalEncargos)}
        </Atom.Text>
        <Atom.Text
          width="8.5%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          fontWeight="bold"
        >
          {formated(total2?.total)}
        </Atom.Text>
        <Atom.Text
          width="8.5%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          fontWeight="bold"
        >
          {formated(total2?.totalEncargos)}
        </Atom.Text>
        <Atom.Text
          width="10%"
          textAlign="center"
          borderRight="1px solid"
          variant="resultTable"
          borderRadius={4}
          fontWeight="bold"
        >
          {formated(total2?.total + total2?.totalEncargos)}
        </Atom.Text>
      </>
    )
  );
};
