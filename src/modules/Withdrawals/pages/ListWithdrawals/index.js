/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable no-unused-vars */
import React, { useCallback, useState, useEffect, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import { format, addYears } from 'date-fns';
import { useTable, useListPickerFetch, useLazyFetch } from '../../../../hooks';
import WithdrawalsEndpoints from '../../api';
import { label } from '../../mocks';
import api from '../../../../services/api';

// logic
import parser from '../../functions/parser';

// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const ListWithdrawals = () => {
  const history = useHistory();

  const [fetchList13, listRend13] = useLazyFetch(
    WithdrawalsEndpoints.getList13
  );
  const [fetchListFerias, listRendFerias] = useLazyFetch(
    WithdrawalsEndpoints.getListFerias
  );
  const [fetchListRecisao, listRendRecisao] = useLazyFetch(
    WithdrawalsEndpoints.getListRecisao
  );

  const {
    dataTable,
    selectAll,
    handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(
    parser(listRend13?.response?.liberacoes) ||
    parser(listRendFerias?.response?.liberacoes) ||
    parser(listRendRecisao?.response?.liberacoes) ||
    []
  );

  const [showFilters, setShowFilters] = useState(true);
  const [showButton, setShowButton] = useState(false);
  const [contractOptions, setContractOptions] = useState();
  const [params, setParams] = useState({
    idContrato: '',
    rubrica: '',
    data: '',
    dataFinal: '',
  });

  const [secondYearView, setSecondYearView] = useState();
  const [secondYear, setSecondYear] = useState();
  const { data: dataContracts } = useListPickerFetch(`/listContratos`);

  useEffect(() => {
    if (dataContracts) {
      setContractOptions(dataContracts);
    }
  }, [dataContracts]);

  const handleAdd = useCallback(() => history.push('/withdrawl/create'), [
    history,
  ]);

  const changeParams = (value, key) => {
    const data = value.target.value;
    if (key === 'rubrica') {
      setParams((prevState) => ({ ...prevState, rubrica: data }));
    }
    if (key === 'id') {
      setParams((prevState) => ({ ...prevState, idContrato: data }));
    }
  };

  useEffect(() => {
    const { idContrato, rubrica } = params;
    if (idContrato && rubrica) {
      setShowButton(true);
    }
  }, [params]);

  const handleClick = () => {
    console.log({ params });
    switch (params.rubrica) {
      case 'rescisao':
        fetchListRecisao(params);
        break;
      case '13salario':
        fetchList13(params);
        break;
      case 'ferias':
        fetchListFerias(params);
        break;
      default:
        break;
    }
    setShowFilters(false);
  };

  const handleDownload = (item) => {
    const { data, dataFinal, idContrato } = params;

    if (item === 'rescisao') {
      const link = document.createElement('a');
      link.href = `${api.baseURL}/docrescisao/${idContrato}/${data}`;
      const response = {
        file: `${api.defaults.baseURL}/docrescisao/${idContrato}/${data}`,
      };
      window.open(response.file);
    }
    if (item === '13salario') {
      const link = document.createElement('a');
      link.href = `${api.baseURL}/doc13salario/${idContrato}/${format(
        new Date(data),
        'yyyy'
      )}`;
      const response = {
        file: `${api.defaults.baseURL}/doc13salario/${idContrato}/${format(
          new Date(data),
          'yyyy'
        )}`,
      };
      window.open(response.file);
    }

    if (item === 'ferias') {
      const link = document.createElement('a');
      link.href = `${api.baseURL}/docferias/${idContrato}/${format(
        new Date(data),
        'yyyy'
      )}/${format(new Date(dataFinal), 'yyyy')}`;
      const response = {
        file: `${api.defaults.baseURL}/docferias/${idContrato}/${format(
          new Date(data),
          'yyyy'
        )}/${format(new Date(dataFinal), 'yyyy')}`,
      };
      window.open(response.file);
    }
  };

  const handleDownloadDoc = () => {
    const { rubrica } = params;
    switch (rubrica) {
      case 'rescisao':
        handleDownload(rubrica);
        break;
      case '13salario':
        handleDownload(rubrica);
        break;
      case 'ferias':
        handleDownload(rubrica);
        break;
      default:
        break;
    }
  };

  const handleModal = () => {
    setShowFilters(true);
    setShowButton(false);
    setParams({
      idContrato: null,
      rubrica: null,
      data: '',
      dataFinal: '',
    });
  };

  const finishDate = useMemo(() => {
    const { data } = params;
    const parse = new Date(data);
    const yearPlusOne = new Date(addYears(parse, 1));
    setSecondYear(false);
    return yearPlusOne;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [secondYear]);

  useEffect(() => {
    if (params.data) {
      setSecondYearView(true);
      setSecondYear(true);
    }
  }, [params.data]);

  return (
    <Atom.Container variant="main">
      <Organisms.HeaderOptions
        title="Liberação"
        searchLabel={showFilters ? false : 'Buscar retirada...'}
        onClickAdd={handleAdd}
        download={handleDownloadDoc}
        showButton={!showFilters}
        openModal={handleModal}
        infos={
          listRend13?.response?.liberacoes ||
          listRendFerias?.response?.liberacoes ||
          listRendRecisao?.response?.liberacoes
        }
      />
      {showFilters ? (
        <Atom.Container variant="list">
          <Atom.Text variant="list" mb="XBIG">
            Escolha as seguintes opções para filtrar a listagem:
          </Atom.Text>
          <Molecules.PickerList
            name="ID Contrato - Empresa"
            label="ID"
            options={contractOptions}
            placeholderDependency="Carregando..."
            onChange={(e) => changeParams(e, 'id')}
            list
            showId
            teste
          />
          <Molecules.PickerList
            name="Rubrica"
            label="Rubrica"
            options={[
              { id: 'rescisao', name: 'Rescisão' },
              { id: '13salario', name: '13º Salário' },
              { id: 'ferias', name: 'Férias + 1/3' },
            ]}
            placeholderDependency="Carregando..."
            onChange={(e) => changeParams(e, 'rubrica')}
            list
            teste
          />
          {params.rubrica && params.rubrica === '13salario' && (
            <Molecules.CalenderPicker
              viewYearCalendar
              percent={25}
              onChange={(e) => {
                setParams((prevState) => ({
                  ...prevState,
                  data: format(e, "yyyy-MM-dd'T'HH:mm:ss"),
                }));
              }}
              label="Ano"
            />
          )}
          {params.rubrica &&
            params.rubrica !== 'rescisao' &&
            params.rubrica !== '13salario' && (
              <Molecules.CalenderPicker
                viewYearCalendar
                percent={25}
                onChange={(e) => {
                  setParams((prevState) => ({
                    ...prevState,
                    data: format(e, "yyyy-MM-dd'T'HH:mm:ss"),
                  }));
                }}
                label="Ano início"
              />
            )}
          {params.rubrica === 'ferias' && secondYearView && (
            <Molecules.CalenderPicker
              viewYearCalendar
              percent={25}
              defaultValue={finishDate || new Date()}
              onChange={(e) => {
                setParams((prevState) => ({
                  ...prevState,
                  dataFinal: format(e, "yyyy-MM-dd'T'HH:mm:ss"),
                }));
              }}
              label="Ano fim"
              spaced
              minDate={params.data}
            />
          )}

          {showButton && (
            <Molecules.Button
              onClick={handleClick}
              text="Filtrar"
              mt="MEDIUM"
            />
          )}
        </Atom.Container>
      ) : (
        <Organisms.Tables
          labels={label}
          data={dataTable}
          selectAll={selectAll}
          onClickSelectAll={handleSelectAll}
          onClickSelectItem={handleSelectItem}
          noSelect
          noOptions
        />
      )}

      <Organisms.FooterList pages={[1, 1, 1, 1, 1]} currentPage={1} />
    </Atom.Container>
  );
};

export default ListWithdrawals;
