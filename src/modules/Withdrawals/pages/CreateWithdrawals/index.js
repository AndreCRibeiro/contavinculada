/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useMemo, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import useBus, { dispatch } from 'use-bus';
// import { yupResolver } from '@hookform/resolvers';
import { format, addYears } from 'date-fns';
import { useListPickerFetch, useLazyFetch, useAuth } from '../../../../hooks';
import { useWithdrawals } from '../../../../global';
import WithdrawalsEndpoints from '../../api';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

import { Flow } from '../../components';

const situation = [
  {
    id: 'rescisao',
    name: 'Rescisão - Multa do FGTS',
  },
  {
    id: '13salario',
    name: '13º Salario',
  },
  {
    id: 'ferias',
    name: 'Ferias + 1/3',
  },
];

const labelRubrica = {
  rescisao: 'rescisao',
  '13salario': '13salario',
  ferias: 'ferias',
};

const CreateWithdrawl = () => {
  const { setRubrica } = useWithdrawals();
  const { user } = useAuth();
  const history = useHistory();
  // const [errorSelect, setErroSelect] = useState(false);
  const [collaboratosSel, setColaboratosSel] = useState(null);

  const {
    // reset,
    watch,
    errors,
    control,
    register,
    getValues,
    handleSubmit,
  } = useForm({
    // resolver: yupResolver(schema),
  });

  const { data: dataContract, trigger } = useListPickerFetch('/listContratos');
  const { data: dataCollaborators } = useListPickerFetch(
    `/colaboradores?contrato=${watch('id_contrato')}`
  );

  const [fetchPreWithdrawals] = useLazyFetch(
    WithdrawalsEndpoints.postPreWithdrawals
  );

  const [fetchCalculate, statusCalc] = useLazyFetch(
    WithdrawalsEndpoints.postCalculate
  );

  useBus(
    '@reset_withdral',
    () => {
      trigger();
    },
    [dataContract, dataCollaborators]
  );

  const handlePreWithdrawals = (form) => {
    let body = {
      id_contrato: form.id_contrato,
      id_fiscal: user.id,
      rubrica: form.situacao,
      colaboradores: collaboratosSel.map((collab) => ({
        id_colaborador: collab.item.id_colaborador,
        nome_colaborador: collab.item.name,
        remuneracao_colaborador: collab.item.dadosCargo.remuneracao,
      })),
      ano_retirada: null,
      inicio_per_aquisitivo: null,
      fim_per_aquisitivo: null,
    };
    if (form.situacao === '13salario') {
      body = {
        ...body,
        ano_retirada: format(form.ano, 'yyyy'),
        inicio_per_aquisitivo: null,
        fim_per_aquisitivo: null,
      };
    }
    if (form.situacao === 'ferias') {
      body = {
        ...body,
        inicio_per_aquisitivo: format(form.inicio, 'yyyy'),
        fim_per_aquisitivo: format(form.fim, 'yyyy'),
        ano_retirada: null,
      };
    }

    fetchPreWithdrawals(body);
  };

  const handleCalculate = useCallback(
    (form) => {
      const value = getValues();

      let body = {
        rubrica: value.situacao,
        id_contrato: parseInt(value.id_contrato, 10),
        inicio_per_aquisitivo: null,
        fim_per_aquisitivo: null,
        ano_retirada: null,
      };

      if (value.situacao === '13salario') {
        body = {
          ...body,
          ano_retirada: format(value.ano || new Date(), 'yyyy'),
        };
      }

      if (value.situacao === 'ferias') {
        body = {
          ...body,
          inicio_per_aquisitivo: format(value.inicio || new Date(), 'yyyy'),
          fim_per_aquisitivo: format(value.fim || new Date(), 'yyyy'),
        };
      }

      fetchCalculate(body);
    },
    [errors, fetchCalculate, getValues]
  );
  /* const handleWithdrawals = useCallback(() => { }, [
    errors,
    fetchCalculate,
    getValues,
  ]); */

  const handleApprove = useCallback(() => { }, [
    errors,
    fetchCalculate,
    getValues,
  ]);

  const onSubmit = (form) => {
    if (user?.type === 'fiscal') handlePreWithdrawals(form);

    if (user?.type === 'financeiro') dispatch('@handle_send_financial'); // handleWithdrawals(form);

    if (user?.type === 'gestor' || user?.type === 'admin') handleApprove(form);
  };

  const handleCancel = useCallback(() => history.push('/withdrawl/list'), [
    history,
  ]);

  // show
  const showCalendar = useMemo(() => {
    if (watch('situacao') === '13salario') return 31.66;
    if (watch('situacao') === 'ferias') return 23;
    return false;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [watch('situacao')]);

  const finishDate = useMemo(
    () => watch('inicio') && new Date(addYears(watch('inicio'), 1)),
    [watch('inicio')]
  );

  const rubrica = useMemo(() => {
    setRubrica(getValues().situacao);
    return getValues().situacao;
  }, [statusCalc.response]);

  return (
    <Atom.Container variant="main">
      <Atom.Form onSubmit={handleSubmit(onSubmit)}>
        <Organisms.HeaderCreate
          title="Liberação"
          searchLabel="Buscar empresas..."
          onClickCancel={handleCancel}
        />
        <Atom.Container
          variant="create"
          justifyContent="flex-start"
          mt="MEDIUM"
        >
          <Atom.Container variant="lineCreate">
            <Molecules.Picker
              name="id_contrato"
              register={register}
              percent={showCalendar || 48.5}
              label="Contrato"
              options={dataContract}
              placeholderDependency="Carregando . . . "
            />
            <Molecules.Picker
              name="situacao"
              register={register}
              percent={showCalendar || 48.5}
              label="Rubrica"
              options={situation}
            />
            {showCalendar && (
              <Controller
                control={control}
                name={showCalendar === 23 ? 'inicio' : 'ano'}
                render={(props) => (
                  <Molecules.CalendarForm
                    viewYearCalendar
                    percent={showCalendar}
                    label={
                      showCalendar === 23
                        ? 'Início do período aquisitivo'
                        : 'Ano'
                    }
                    {...props}
                  />
                )}
              />
            )}
            {showCalendar === 23 && (
              <Controller
                control={control}
                name="fim"
                render={(props) => (
                  <Molecules.CalendarForm
                    // dt.setFullYear(dt.getFullYear() + n)
                    defaultValue={finishDate}
                    viewYearCalendar
                    percent={showCalendar}
                    label="Fim do período aquisitivo"
                    minDate={watch('inicio')}
                    {...props}
                  />
                )}
              />
            )}
          </Atom.Container>
          {user?.type === 'financeiro' && (
            <Atom.Container variant="lineCreate" mt="SMALL">
              <Molecules.Button
                text="CALCULAR"
                width="30%"
                disableSubmit
                onClick={handleCalculate}
              />
              <Atom.Container />
            </Atom.Container>
          )}
          {statusCalc.loading && <Atom.Loader />}
          {!statusCalc.loading && statusCalc.response?.length > 0 && (
            <Flow
              user={user}
              rubrica={labelRubrica[rubrica]}
              dataCollaborators={statusCalc?.response?.map((item, id) => ({
                ...item,
                id,
              }))}
              setColaboratosSel={setColaboratosSel}
              dataContract={dataContract}
              idContract={watch('id_contrato')}
            />
          )}
          {!statusCalc.loading && statusCalc.response?.length === 0 && (
            <Atom.Text fontSize="32px" mt="25px" ml="15px">
              Não há nenhum colaborador para esse cálculo...
            </Atom.Text>
          )}
          {user?.type === 'fiscal' && (
            <Flow
              user={user}
              rubrica={labelRubrica[rubrica]}
              dataCollaborators={dataCollaborators?.colaboradores.map(
                (item, id) => ({
                  ...item,
                  id,
                  id_colaborador: item.id,
                })
              )}
              setColaboratosSel={setColaboratosSel}
              dataContract={dataContract}
              idContract={watch('id_contrato')}
            />
          )}
        </Atom.Container>
      </Atom.Form>
    </Atom.Container>
  );
};

export default CreateWithdrawl;
