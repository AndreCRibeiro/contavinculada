import api from '../../../services/api';

const DepositsEndpoints = {
  postDeposits: (data) => api.post('/retencao', data),
  postPreDeposit: (data) => api.post('/preretencao', data),
  getCalculate: ({ data }) => {
    return api.post(`/calcretencao`, data);
  },
};

export default DepositsEndpoints;
