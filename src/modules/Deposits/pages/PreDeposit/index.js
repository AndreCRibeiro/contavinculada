/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { format, startOfDay, subHours, endOfMonth } from 'date-fns';
import { toast } from 'react-toastify';
import {
  useListPickerFetch,
  useLazyFetch,
  useAuth,
  useTable,
} from '../../../../hooks';
import DepositsEndpoints from '../../api';
import { formatReal } from '../../../../utils';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

import { label } from '../../mocks/collaborators';

import parser from '../../functions/parserCollaborators';

const PreDeposit = () => {
  const { user } = useAuth();
  const history = useHistory();

  const { control, reset, watch, register, handleSubmit } = useForm();

  const [fetchSave, { response: responseSave }] = useLazyFetch(
    DepositsEndpoints.postPreDeposit
  );
  const { data: dataContract } = useListPickerFetch(
    `/listContratos?competencia=${format(
      watch('month') || new Date(),
      'yyyy-M'
    )}`
  );

  const { data: dataCollaborators } = useListPickerFetch(
    `/colaboradores?contrato=${watch('contrato')}&competencia=${format(
      watch('month') || new Date(),
      'yyyy-M'
    )}&situacao=ativo`
  );

  const {
    offset,
    dataTable,
    selectAll,
    numberOfPage,
    dataTableArray,
    prevPage,
    nextPage,
    selectPage,
    handleSelectAll,
    handleSelectItem,
  } = useTable(
    dataCollaborators ? parser(dataCollaborators.colaboradores) : []
  );

  const onSubmit = (form) => {
    const testeData = subHours(startOfDay(endOfMonth(form.month)), 3);

    const selectedColaboratos = dataTable
      .filter((item) => item.selected)
      .map((el) => el.id);

    const dataPre = {
      id_contrato: parseInt(form.contrato, 10),
      id_fiscal: user.id,
      id_fiscal_substituto: null,
      competencia: testeData,
      colaboradores: selectedColaboratos.length
        ? dataCollaborators.colaboradores
          .filter((colab) => selectedColaboratos.includes(colab.id))
          .map((colab) => ({
            id: colab.id,
            name: colab.name,
            remuneracao: colab.dadosCargo.remuneracao,
            // data_disponibilizacao: colab.data_disponibilizacao,
          }))
        : [],
    };

    if (!dataPre || !dataPre.colaboradores.length) {
      toast.warning('Selecione ao menos um colaborador', {
        position: toast.POSITION.TOP_RIGHT,
      });
    }

    fetchSave(dataPre);
  };

  const handleCancel = useCallback(() => history.push('/deposit/list'), [
    history,
  ]);

  const handleTotal = () => {
    let remuneracaoTotal = 0;
    dataCollaborators.colaboradores.map((collaborator) => {
      remuneracaoTotal += collaborator.dadosCargo.remuneracao;
    });
    return formatReal(remuneracaoTotal.toFixed(2));
  };

  const editedAliquots = (id, type) => {
    if (!dataContract) return null;
    const data = dataContract.find((item) => item.id === parseInt(id, 10));
    if (!id) {
      return '0,00';
    }
    if (type === 'conta_vinculada' || type === 'descricao_contrato') {
      return `${data[type]}`.replace('.', ',');
    }
    if (type) {
      return `${data[type]}`.replace('.', ',');
    }
    const total = data
      ? data.decimo_terceiro +
      data.encargos +
      data.ferias_adicionais +
      data.recisao_fgts
      : 0;
    return `${total.toFixed(2)}%`.replace('.', ',');
  };

  useEffect(() => {
    if (responseSave) {
      reset({}, { errors: false });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [responseSave]);

  return (
    <Atom.Container variant="main">
      <Atom.Form onSubmit={handleSubmit(onSubmit)}>
        <Organisms.HeaderCreate
          title="Retenção"
          searchLabel="Buscar empresas..."
          onClickCancel={handleCancel}
        // onClickAdd={handleSave}
        />
        <Atom.Container variant="lineCreate">
          <Controller
            control={control}
            name="month"
            render={(props) => (
              <Molecules.CalendarForm
                percent={48.75}
                label="Competência"
                {...props}
              />
            )}
          />
          <Molecules.Picker
            name="contrato"
            register={register}
            percent={48.75}
            label="Contrato"
            options={
              dataContract && [
                { numero_contrato: 'Selecione um contrato', id: null },
                ...dataContract,
              ]
            }
            placeholderDependency={!dataContract && 'Carregando . . .'}
          />
        </Atom.Container>
        {!!watch('contrato') && watch('contrato') !== 'Selecione um contrato' && (
          <>
            <Atom.Container variant="divider" />
            <Atom.Text mb="SMALL">Dados do Contrato</Atom.Text>
            <Atom.Container variant="lineCreate">
              <Molecules.TextForm
                percent={49}
                label="Conta Vinculada"
                infoText={editedAliquots(watch('contrato'), 'conta_vinculada')}
              />
              <Molecules.TextForm
                percent={49}
                label="Objeto"
                infoText={editedAliquots(
                  watch('contrato'),
                  'descricao_contrato'
                )}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.TextForm
                percent={18}
                label="13º Salario(%)"
                infoText={editedAliquots(watch('contrato'), 'decimo_terceiro')}
              />
              <Molecules.TextForm
                percent={18}
                label="Férias + 1/3(%)"
                infoText={editedAliquots(
                  watch('contrato'),
                  'ferias_adicionais'
                )}
              />
              <Molecules.TextForm
                percent={18}
                label="Multa do FTGS (%)"
                infoText={editedAliquots(watch('contrato'), 'recisao_fgts')}
              />
              <Molecules.TextForm
                percent={18}
                label="Encargos (%)"
                infoText={editedAliquots(watch('contrato'), 'encargos')}
              />
              <Molecules.TextForm
                percent={18}
                label="Total"
                infoText={editedAliquots(watch('contrato'))}
              />
            </Atom.Container>
            <Atom.Container variant="divider" />
          </>
        )}
        {!!dataCollaborators && (
          <>
            <Atom.Container variant="lineCreate">
              <Organisms.Tables
                labels={label}
                data={dataTableArray || []}
                selectAll={selectAll}
                noOptions
                onClickSelectAll={handleSelectAll}
                onClickSelectItem={(i) => handleSelectItem(i, true)}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Atom.Text
                px="20px"
                width="70%"
                textAlign="right"
                variant="buttonOutline"
              >
                TOTAL GERAL
              </Atom.Text>
              <Atom.Text
                width="30%"
                textAlign="center"
                borderRight="1px solid"
                variant="resultTable"
                borderRadius={4}
              >
                {`R$  ${handleTotal()}`}
              </Atom.Text>
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Atom.Container />
              <Organisms.FooterList
                pages={numberOfPage}
                currentPage={offset}
                onNextPage={nextPage}
                onPrevPage={prevPage}
                onSelectPage={selectPage}
              />
              <Atom.Container />
            </Atom.Container>
          </>
        )}
      </Atom.Form>
    </Atom.Container>
  );
};

export default PreDeposit;
