/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useMemo, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { toast } from 'react-toastify';
import { format, startOfDay, subHours, endOfMonth } from 'date-fns';
import { useListPickerFetch, useLazyFetch, useTable } from '../../../../hooks';
import DepositsEndpoints from '../../api';
import api from '../../../../services/api';
import { formatReal } from '../../../../utils';
// logic
import parser from '../../functions/parserResult';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

import { label } from '../../mocks/teste';

const CreateDeposit = () => {
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [contractSelected, setContract] = useState(null);
  const [constextSuccess, setContextSuccess] = useState(null);
  const [
    fetchSave,
    { response: responseSave, loading: loadingSave },
  ] = useLazyFetch(DepositsEndpoints.postDeposits);
  const [
    fetchCalculate,
    { response: responseCalculate, loading: loadingCalculate },
  ] = useLazyFetch(DepositsEndpoints.getCalculate);

  const {
    offset,
    trigger2,
    dataTable,
    numberOfPage,
    dataTableArray,
    prevPage,
    nextPage,
    selectPage,
    handleChangeValue,
  } = useTable(loadingCalculate ? [] : parser(responseCalculate || []));

  const {
    watch,
    errors,
    control,
    reset,
    register,
    getValues,
    handleSubmit,
  } = useForm();

  const { data: dataContract, loadingContract } = useListPickerFetch(
    `/listpreretencao?competencia=${watch('month')}`
  );

  const contracts = useMemo(
    () =>
      dataContract?.contratos?.map((item) => ({
        ...item,
        numero_contrato: item.numero_contrato,
        id: item.id_contrato,
      })),
    [dataContract]
  );

  const onSubmit = () => {
    const form = getValues();
    const testeData = subHours(startOfDay(endOfMonth(form.month)), 3);
    let total = 0.0;
    // eslint-disable-next-line array-callback-return
    dataTable.map((line) => {
      total = parseFloat(line.columns[8].value) + total;
    });

    // eslint-disable-next-line eqeqeq
    const constract = contracts?.find((el) => el.id == form.contrato);
    if (!responseCalculate) {
      toast.warning('Faça o calculo de um contrato', {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    if (
      responseCalculate &&
      // responseCalculate.length &&
      !loadingSave &&
      !loadingContract
    ) {
      setContextSuccess({
        idContrato: constract.id_contrato,
        competencia: testeData,
      });
      const data = responseCalculate.filter(
        (colaborator) =>
          colaborator.decimo13 || colaborator.ferias || colaborator.fgts
      );
      fetchSave({
        id_contrato: parseInt(form.contrato, 10),
        competencia: testeData,
        data,
      });
    } else {
      console.log('entrei no erro');
    }
  };

  const handleCalculate = useCallback(() => {
    const contractValidation = watch('contrato');
    if (contractValidation === 'Selecione um contrato') {
      toast.warn('Selecione um contrato para calcular', {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      const form = getValues();
      const testeData = subHours(startOfDay(endOfMonth(form.month)), 3);
      const contract = dataContract.contratos.find(
        (item) => `${item.id_contrato}` === `${form.contrato}`
      );

      if (Object.keys(errors).length) {
        return;
      }

      fetchCalculate({
        id: parseInt(form.contrato, 10),
        data: {
          competencia: testeData,
          id_contrato: parseInt(form.contrato, 10),
        },
      });
      setContract(contract);
    }
  }, [errors, fetchCalculate, getValues]);

  const handleCancel = useCallback(() => history.push('/deposit/list'), [
    history,
  ]);

  const handleDowloadDoc = () => {
    const { idContrato, competencia } = constextSuccess;
    const link = document.createElement('a');
    link.href = `${api.baseURL}/docretencao/${format(
      competencia,
      'yyyy-M'
    )}/${idContrato}`;
    // 1
    const response = {
      file: `${api.defaults.baseURL}/docretencao/${format(
        competencia,
        'yyyy-M'
      )}/${idContrato}`,
    };
    window.open(response.file);
  };

  const handleTotal = (keyObject) => {
    let total = 0;
    if (responseCalculate.message) {
      return formatReal(total.toFixed(2));
    }
    // eslint-disable-next-line array-callback-return
    responseCalculate.map((collaborator) => {
      if (collaborator[keyObject] === 'N/A') {
        total = 'N/A';
      } else {
        total += collaborator[keyObject];
      }
    });
    return total === 'N/A' ? 'N/A' : formatReal(total?.toFixed(2));
  };

  useEffect(() => {
    if (responseSave) {
      setModal(true);
      reset({}, { errors: false });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [responseSave]);

  const enableCalculate =
    watch('contrato') !== 'Selecione um contrato' &&
    `${contractSelected?.id_contrato}` === watch('contrato') &&
    !loadingCalculate &&
    responseCalculate;

  return (
    <>
      <Molecules.Modal
        show={modal}
        img="SUCCESS"
        confirm
        blackButton
        variant="modalSuccess"
        title="Retenção realizada com sucesso"
        textBlueButton="BAIXAR RELATÓRIO DA RETENÇÃO"
        textBlackButton="VOLTAR PARA RETENÇÃO"
        textBack="FECHAR"
        handleCloseModal={() => setModal(false)}
        handleNavigate={handleDowloadDoc}
        handleNavigateToList={() => setModal(false)}
      />
      <Atom.Container variant="main">
        <Atom.Form onSubmit={handleSubmit(onSubmit)}>
          <Organisms.HeaderCreate
            title="Retenção"
            onClickCancel={handleCancel}
          // onClickAdd={}
          />
          <Atom.Container variant="lineCreate">
            <Controller
              control={control}
              name="month"
              render={(props) => (
                <Molecules.CalendarForm
                  percent={48.75}
                  label="Competência"
                  {...props}
                />
              )}
            />
            <Molecules.Picker
              name="contrato"
              register={register}
              percent={48.75}
              label="Contrato"
              options={
                dataContract?.contratos && [
                  { numero_contrato: 'Selecione um contrato', id: null },
                  ...dataContract.contratos,
                ]
              }
              placeholderDependency={!dataContract && 'Carregando . . .'}
            />
          </Atom.Container>

          <Atom.Container variant="lineCreate" mt="SMALL">
            <Molecules.Button
              text="CALCULAR"
              width="30%"
              disableSubmit
              onClick={handleCalculate}
            />
            <Atom.Container />
          </Atom.Container>
          {loadingCalculate && (
            <Atom.Container
              mt="15%"
              justifyContent="center"
              alignItems="center"
            >
              <Atom.Loader />
            </Atom.Container>
          )}
          {enableCalculate && trigger2 && (
            <>
              <Atom.Container variant="divider" />
              <Organisms.Tables
                trigger={trigger2}
                noSelect
                labels={label}
                data={dataTableArray}
                selectAll={false}
                onChangeValue={handleChangeValue}
                noOptions
              />
            </>
          )}
          {enableCalculate && (
            <>
              <Atom.Container variant="lineCreate">
                <Atom.Text
                  px="20px"
                  width="37%"
                  textAlign="right"
                  variant="buttonOutline"
                >
                  TOTAL GERAL
                </Atom.Text>
                <Atom.Text
                  width="12%"
                  textAlign="center"
                  borderRight="1px solid"
                  variant="resultTable"
                  borderRadius={4}
                >
                  {`R$  ${handleTotal('remuneracao')}`}
                </Atom.Text>
                <Atom.Text
                  width="10%"
                  textAlign="center"
                  borderRight="1px solid"
                  variant="resultTable"
                >
                  {`R$  ${handleTotal('decimo13')}`}
                </Atom.Text>
                <Atom.Text
                  width="10%"
                  textAlign="center"
                  borderRight="1px solid"
                  variant="resultTable"
                >
                  {`R$  ${handleTotal('ferias')}`}
                </Atom.Text>
                <Atom.Text
                  width="10%"
                  textAlign="center"
                  borderRight="1px solid"
                  variant="resultTable"
                >
                  {`R$  ${handleTotal('encargosSociais')}`}
                </Atom.Text>
                <Atom.Text
                  width="10%"
                  textAlign="center"
                  borderRight="1px solid"
                  variant="resultTable"
                >
                  {`R$  ${handleTotal('fgts')}`}
                </Atom.Text>
                <Atom.Text
                  width="13%"
                  textAlign="center"
                  borderRight="1px solid"
                  variant="resultTable"
                  borderRadius={4}
                >
                  {`R$  ${handleTotal('total')}`}
                </Atom.Text>
              </Atom.Container>
              <Atom.Container variant="lineCreate">
                <Atom.Container />
                <Organisms.FooterList
                  pages={numberOfPage}
                  currentPage={offset}
                  onNextPage={nextPage}
                  onPrevPage={prevPage}
                  onSelectPage={selectPage}
                />
                <Atom.Container />
              </Atom.Container>
            </>
          )}
        </Atom.Form>
      </Atom.Container>
    </>
  );
};

export default CreateDeposit;
