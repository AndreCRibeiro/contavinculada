/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useTable, useListFetch, useAllData } from '../../../../hooks';
import api from '../../../../services/api';
import { label } from '../../mocks';
import useStore from '../../../../global/modalDelete/store';
import useStoreVisualize from '../../../../global/modalVisualize/store';
import { formatReal } from '../../../../utils';

// logic
import parser from '../../functions/parser';

// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const ListDeposits = () => {
  const [inputText, setInputText] = useState('');
  const [inputText2, setInputText2] = useState('');
  const [filteredList, setFilteredList] = useState([]);
  const [dateRegex, setDateRegex] = useState('');
  const history = useHistory();
  const {
    data,
    error,
    offset,
    loading,
    numberPages,
    nextPage,
    prevPage,
    selectPage,
  } = useListFetch('/retencao');
  const {
    dataTable,
    selectAll,
    handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(parser(data?.data) || [], offset);

  const { allDataTable } = useAllData(parser(data?.dataAll) || [], offset);

  const { dataZustand, reset, pdf } = useStore();
  const {
    dataZustandVisualize,
    modalVisualize,
    resetVisualize,
  } = useStoreVisualize();

  const handleAdd = useCallback(() => history.push('/deposit/create'), [
    history,
  ]);

  const handleDownloadDoc = (item) => {
    const teste = item.competencia.split('/');
    switch (teste[0]) {
      case 'janeiro':
        setDateRegex(`${teste[1]}-1`);
        break;
      case 'fevereiro':
        setDateRegex(`${teste[1]}-2`);
        break;
      case 'março':
        setDateRegex(`${teste[1]}-3`);
        break;
      case 'abril':
        setDateRegex(`${teste[1]}-4`);
        break;
      case 'maio':
        setDateRegex(`${teste[1]}-5`);
        break;
      case 'junho':
        setDateRegex(`${teste[1]}-6`);
        break;
      case 'julho':
        setDateRegex(`${teste[1]}-7`);
        break;
      case 'agosto':
        setDateRegex(`${teste[1]}-8`);
        break;
      case 'setembro':
        setDateRegex(`${teste[1]}-9`);
        break;
      case 'outubro':
        setDateRegex(`${teste[1]}-10`);
        break;
      case 'novembro':
        setDateRegex(`${teste[1]}-11`);
        break;
      default:
        setDateRegex(`${teste[1]}-12`);
        break;
    }
  };

  useEffect(() => {
    if (dateRegex !== '') {
      const link = document.createElement('a');
      link.href = `${api.baseURL}/docretencao/${dateRegex}/${dataZustand.id_contrato}`;
      // 1
      const response = {
        file: `${api.defaults.baseURL}/docretencao/${dateRegex}/${dataZustand.id_contrato}`,
      };
      window.open(response.file);
      reset();
    }
  }, [dateRegex]);

  console.log({ dataTable, allDataTable });

  useEffect(() => {
    if (inputText !== '' && inputText2 === '') {
      const teste = allDataTable.filter((input) => {
        if (input.data.contrato.includes(inputText)) {
          return true;
        }
        return false;
      });
      setFilteredList(teste);
    }
    if (inputText2 !== '' && inputText === '') {
      const teste = allDataTable.filter((input) => {
        if (input.data.competencia.includes(inputText2)) {
          return true;
        }
        return false;
      });
      setFilteredList(teste);
    }
    if (inputText !== '' && inputText2 !== '') {
      const teste = allDataTable.filter((input) => {
        if (input.data.contrato.includes(inputText)) {
          return true;
        }
        return false;
      });
      const teste2 = teste.filter((input) => {
        if (input.data.competencia.includes(inputText2)) {
          return true;
        }
        return false;
      });
      setFilteredList(teste2);
    } else if (inputText === '' && inputText2 === '') {
      setFilteredList([]);
    }
  }, [inputText, inputText2]);

  return (
    <>
      {pdf && (
        <Molecules.Modal
          show={pdf}
          confirm
          variant="modalAlert"
          title={`Deseja fazer o download do arquivo PDF da retenção ${dataZustand.id} do contrato ${dataZustand.contrato}?`}
          textBlueButton="BAIXAR"
          textBack="CANCELAR"
          handleNavigate={() => handleDownloadDoc(dataZustand)}
          handleNavigateToList={() => reset()}
        />
      )}
      {modalVisualize && (
        <Molecules.Modal
          show={modalVisualize}
          positionModal
          variant="modalText"
          textTitle1="Contrato"
          text1={dataZustandVisualize.contrato}
          textTitle2="Competência"
          text2={dataZustandVisualize.competencia}
          textTitle3="Total Retido"
          text3={`R$ ${formatReal(dataZustandVisualize.totalRetido)}`}
          handleCloseModal={() => resetVisualize()}
        />
      )}
      <Atom.Container variant="main">
        <Organisms.HeaderList
          title="Retenção"
          searchLabel="Digite o nº do contrato..."
          searchLabel2="Buscar competência..."
          onClickAdd={handleAdd}
          onClickExclude={handleDelete}
          onInputChange={(text) => setInputText(text)}
          deposit
          onInputChange2={(text) => setInputText2(text)}
        />
        {loading ? (
          <Atom.Loader size={35} width="100%" />
        ) : inputText !== '' && filteredList.length === 0 ? (
          <Atom.Container variant="filterError">
            <Atom.Text variant="filterError">
              Nenhuma retenção encontrada 1
            </Atom.Text>
          </Atom.Container>
        ) : inputText2 !== '' && filteredList.length === 0 ? (
          <Atom.Container variant="filterError">
            <Atom.Text variant="filterError">
              Nenhuma retenção encontrada 2
            </Atom.Text>
          </Atom.Container>
        ) : (
          <Organisms.Error error={error}>
            <Organisms.Tables
              labels={label}
              data={filteredList.length !== 0 ? filteredList : dataTable}
              selectAll={selectAll}
              onClickSelectAll={handleSelectAll}
              onClickSelectItem={handleSelectItem}
              noSelect
              deposit
            />
          </Organisms.Error>
        )}
        <Organisms.FooterList
          pages={numberPages}
          currentPage={offset}
          onSelectPage={selectPage}
          onNextPage={nextPage}
          onPrevPage={prevPage}
        />
      </Atom.Container>
    </>
  );
};

export default ListDeposits;
