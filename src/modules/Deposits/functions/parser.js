import { formatReal } from '../../../utils';

export default (data) => {
  return data?.map((item) => ({
    selected: false,
    data: item,
    columns: [
      {
        value: item.competencia,
        width: '30%',
        type: 'text',
      },
      {
        value: item.contrato,
        width: '30%',
        type: 'text',
      },
      {
        value: `R$ ${formatReal(item.totalRetido)}`,
        width: '30%',
        type: 'text',
      },
    ],
  }));
};
