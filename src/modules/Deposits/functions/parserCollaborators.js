import { formatReal } from '../../../utils';

export default (data) => {
  // const timeZone = 'America/Brasilia';
  return data?.map((item, i) => ({
    selected: false,
    id: item.id,
    columns: [
      {
        value: i + 1,
        width: '5%',
        type: 'text',
      },
      {
        value: item.name,
        width: '50%',
        type: 'text',
      },
      {
        value: item.dadosCargo.nome_cargo,
        width: '25%',
        type: 'text',
      },
      {
        value: formatReal(item.dadosCargo.remuneracao),
        width: '20%',
        type: 'text',
        pl: '1.75%',
      },
    ],
  }));
};
