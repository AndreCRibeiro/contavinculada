import { formatReal } from '../../../utils';

export default (data) => {
  // const timeZone = 'America/Brasilia';
  return data.length
    ? data?.map((item, i) => ({
        selected: false,
        columns: [
          {
            type: 'text',
            width: '3%',
            value: i + 1,
          },
          {
            type: 'text',
            width: '23%',
            value: item.colaborador,
          },
          {
            type: 'text',
            width: '10%',
            value: item.posto,
          },
          {
            type: 'text',
            width: '12%',
            value: formatReal(item.remuneracao),
            border: true,
          },
          {
            type: 'text',
            width: '10%',
            value: formatReal(item.decimo13),
            border: true,
          },
          {
            type: 'text',
            width: '10%',
            value: formatReal(item.ferias),
            border: true,
          },
          {
            type: 'text',
            width: '10%',
            value: formatReal(item.encargosSociais),
            border: true,
          },
          {
            type: 'text',
            width: '10%',
            value: formatReal(item.fgts),
            border: true,
          },
          {
            type: 'text',
            width: '12%',
            value: formatReal(item.total),
            pl: '3%',
          },
        ],
      }))
    : [];
};
