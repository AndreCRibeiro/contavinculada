import * as yup from 'yup';

export const schema = yup.object().shape({
  colaboradores: yup.array().required('Selecione ao menos um colaborador'),
});
