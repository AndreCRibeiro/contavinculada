export const label = [
  {
    value: '',
    width: '3%',
  },
  {
    value: 'Colaborador',
    width: '23%',
  },
  {
    value: 'Posto',
    width: '10%',
  },
  {
    value: 'Remuneração',
    width: '12%',
  },
  {
    value: '13º salário',
    width: '10%',
  },
  {
    value: 'Férias +1/3',
    width: '10%',
  },
  {
    value: 'Encargos Sociais',
    width: '10%',
  },
  {
    value: 'Multa FGTS',
    width: '10%',
  },
  {
    value: 'SubTotal',
    width: '12%',
  },
];

export const data = [
  {
    id_financeiro: 1,
    competencia: '2020-08-14T18:37:01.152Z',
    colaborador: 'Paulo Tapa ',
    posto: 'copeiro',
    remuneracao: 1200.5,
    decimo13: 144.06,
    ferias: 192.08,
    encargosSociais: 168.07,
    fgts: 60.03,
    total: 564.24,
  },
  {
    id_financeiro: 1,
    competencia: '2020-08-14T18:37:01.152Z',
    colaborador: 'Ayrton',
    posto: 'copeiro',
    remuneracao: 1200.5,
    decimo13: 144.06,
    ferias: 192.08,
    encargosSociais: 168.07,
    fgts: 60.03,
    total: 564.24,
  },
  {
    id_financeiro: 1,
    competencia: '2020-08-14T18:37:01.152Z',
    colaborador: 'Andre',
    posto: 'copeiro',
    remuneracao: 1200.5,
    decimo13: 144.06,
    ferias: 192.08,
    encargosSociais: 168.07,
    fgts: 60.03,
    total: 564.24,
  },
];
