export const label = [
  {
    value: '',
    width: '5%',
  },
  {
    value: 'Colaborador',
    width: '50%',
  },
  {
    value: 'Posto',
    width: '25%',
  },
  {
    value: 'Remuneração',
    width: '20%',
  },
];

export const data = [
  {
    selected: false,
    columns: [
      {
        value: 0,
        width: '5%',
        type: 'text',
      },
      {
        value: 'Ayrton Lacerda',
        width: '55%',
        type: 'text',
      },
      {
        value: 'Garçom',
        width: '40%',
        type: 'text',
      },
    ],
  },
];
