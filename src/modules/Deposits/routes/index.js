import React from 'react';
import { Route } from 'react-router-dom';
import { WrapContainer } from '../../../components/Organisms';
import { useUser } from '../../../global';
import ListDeposits from '../pages/ListDeposits';
import CreateDeposit from '../pages/CreateDeposit';
import PreDeposit from '../pages/PreDeposit';

const Routes = () => {
  const { user } = useUser();
  return (
    <>
      <Route
        path="/deposit/list"
        exact
        component={() => (
          <WrapContainer>
            <ListDeposits />
          </WrapContainer>
        )}
      />
      <Route
        path="/deposit/create"
        exact
        component={() => (
          <WrapContainer>
            {user?.type === 'fiscal' ? <PreDeposit /> : <CreateDeposit />}
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
