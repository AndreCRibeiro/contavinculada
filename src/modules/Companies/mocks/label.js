export const label = [
  {
    value: 'Empresa',
    width: '40%',
  },
  {
    value: 'CNPJ',
    width: '25%',
  },
  {
    value: 'Telefone',
    width: '25%',
  },
];
