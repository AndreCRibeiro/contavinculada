/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import { schema } from '../../mocks';
// logic
import CompaniesEndpoints from '../../api';
import { useLazyFetch } from '../../../../hooks';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const CreateCompanies = () => {
  const history = useHistory();

  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [update, setUpdate] = useState(false);

  const [fetch, { response }] = useLazyFetch(CompaniesEndpoints.postCompanies);
  const [updateReq, { response: updateResponse }] = useLazyFetch(
    CompaniesEndpoints.updateCompanie
  );

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: !modal && yupResolver(schema),
    defaultValues: {
      name: history.location?.state ? history.location?.state.data.name : null,
      cnpj: history.location?.state ? history.location?.state.data.cnpj : null,
      phone: history.location?.state
        ? history.location?.state.data.phone
        : null,
      email: history.location?.state
        ? history.location?.state.data.email
        : null,
      responsible_name: history.location?.state
        ? history.location?.state.data.responsible_name
        : null,
      responsible_phone: history.location?.state
        ? history.location?.state.data.responsible_phone
        : null,
      responsible_email: history.location?.state
        ? history.location?.state.data.responsible_email
        : null,
      description: history.location?.state
        ? history.location?.state.data.description
        : null,
    },
  });

  const onSubmit = (form) => {
    fetch(form);
  };

  const onUpdateSubmit = (form, { id = history.location.state.data.id }) => {
    updateReq({ ...form, id });
  };

  const handleCancel = useCallback(() => history.push('/companie/list'), [
    history,
  ]);

  useEffect(() => {
    if (history.location?.state) {
      setUpdate(true);
    }
  }, []);

  useEffect(() => {
    if (response) {
      setModal(true);
      reset(
        {},
        {
          errors: false,
        }
      );
    }
    if (updateResponse) {
      setModalUpdate(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response, updateResponse]);

  return (
    <>
      <Atom.Container variant="main">
        <Atom.Form
          onSubmit={
            update ? handleSubmit(onUpdateSubmit) : handleSubmit(onSubmit)
          }
        >
          <Organisms.HeaderCreate
            title={update ? 'Atualizar Empresa' : 'Empresa'}
            searchLabel="Buscar empresas..."
            onClickCancel={handleCancel}
          />
          <Atom.Container
            variant="create"
            justifyContent="flex-start"
            mt="MEDIUM"
          >
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                name="name"
                register={register}
                error={errors?.name?.message}
                percent={70}
                label="Nome"
                placeholder="Nome da empresa"
              />
              <Molecules.InputForm
                name="cnpj"
                register={register}
                error={errors?.cnpj?.message}
                withMask
                mask="99.999.999/9999-99"
                percent={27.5}
                label="CNPJ"
                placeholder="xx.xxx.xxx/xxxx-xx"
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                name="phone"
                register={register}
                error={errors?.phone?.message}
                withMask
                mask="(99) 99999-9999"
                percent={27.5}
                label="Telefone"
                placeholder="(61)99999-9999"
                obsLabel="(da empresa)"
              />
              <Molecules.InputForm
                name="email"
                register={register}
                error={errors?.email?.message}
                percent={70}
                label="E-mail"
                placeholder="Email da empresa - exemplo@email.com"
              />
            </Atom.Container>
            <Atom.Container variant="divider" />
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                name="responsible_name"
                register={register}
                error={errors?.responsible_name?.message}
                percent={49}
                label="Preposto"
                placeholder="Responsável pela empresa"
              />
              <Molecules.InputForm
                name="responsible_phone"
                register={register}
                error={errors?.responsible_phone?.message}
                withMask
                mask="(99) 99999-9999"
                percent={48.5}
                label="Telefone"
                placeholder="(61)99999-9999"
                obsLabel="(do preposto)"
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                name="responsible_email"
                register={register}
                error={errors?.responsible_email?.message}
                percent={49}
                label="E-mail"
                placeholder="Email do responsável - exemplo@email.com"
              />
            </Atom.Container>
            <Atom.Container variant="divider" />
            <Molecules.InputForm
              name="description"
              register={register}
              percent={100}
              label="Outras Informações"
              placeholder="Informações adicionais para a empresa"
              multiLine
            />
          </Atom.Container>
        </Atom.Form>
      </Atom.Container>
      <Molecules.Modal
        show={modalUpdate}
        img="SUCCESS"
        confirm
        variant="modalSuccess"
        title="Empresa alterada com sucesso"
        textBlueButton="VOLTAR PARA EMPRESAS"
        textBack="ALTERAR NOVAMENTE"
        handleNavigate={() => history.push('/companie/list')}
        handleNavigateToList={() => setModalUpdate(false)}
      />
      <Molecules.Modal
        show={modal}
        img="SUCCESS"
        confirm
        blackButton
        variant="modalSuccess"
        title="Empresa adicionada com sucesso"
        textBlueButton="ADICIONAR CONTRATO"
        textBlackButton="ADICIONAR NOVA EMPRESA"
        textBack="VOLTAR PARA EMPRESAS"
        handleCloseModal={() => setModalUpdate(false)}
        handleNavigate={() => history.push('/contract/create')}
        handleNavigateToList={() => history.push('/companie/list')}
      />
    </>
  );
};

export default CreateCompanies;
