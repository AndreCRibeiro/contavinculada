/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import useStore from '../../../../global/modalDelete/store';
import useStoreVisualize from '../../../../global/modalVisualize/store';
import { useTable, useListFetch, useAllData } from '../../../../hooks';
import api from '../../../../services/api';
import { label } from '../../mocks';
// logic
import parser from '../../functions/parser';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const ListCompanies = () => {
  const [inputText, setInputText] = useState('');
  const [filteredList, setFilteredList] = useState([]);
  const history = useHistory();
  const {
    data,
    error,
    offset,
    loading,
    numberPages,
    nextPage,
    prevPage,
    selectPage,
  } = useListFetch('/empresas');

  const {
    selectAll,
    handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(parser(data?.data) || [], offset);

  const { allDataTable } = useAllData(parser(data?.all) || [], offset);

  const { dataZustand, modalDelete, reset, edit } = useStore();
  const {
    dataZustandVisualize,
    modalVisualize,
    resetVisualize,
  } = useStoreVisualize();

  const handleAdd = useCallback(() => history.push('/companie/create'), [
    history,
  ]);

  const handleEdit = (dataEdit) => {
    history.push('/companie/create', { data: dataEdit });
  };

  useEffect(() => {
    if (edit) {
      handleEdit(dataZustand);
      reset();
    }
  }, [edit, dataZustand]);

  const handleDeleteItem = async (item) => {
    const response = await api.delete(`/empresas/${item.id}`);
    reset();
    if (response.status === 200) {
      toast.success('Empresa deletada com sucesso', {
        position: toast.POSITION.TOP_CENTER,
      });
    } else {
      toast.error('Erro ao deletar empresa', {
        position: toast.POSITION.TOP_CENTER,
      });
    }
  };

  useEffect(() => {
    if (inputText !== '') {
      const teste = allDataTable.filter((input) => {
        if (input.data.name.toLowerCase().includes(inputText.toLowerCase())) {
          return true;
        }
        return false;
      });
      setFilteredList(teste);
    } else {
      setFilteredList([]);
    }
  }, [inputText]);

  return (
    <>
      {modalDelete && (
        <Molecules.Modal
          show={modalDelete}
          img="ALERT"
          alert
          variant="modalAlert"
          title={`Deseja realmente apagar ${dataZustand.name}`}
          handleCloseModal={() => reset()}
          handleDelete={() => handleDeleteItem(dataZustand)}
        />
      )}
      {modalVisualize && (
        <Molecules.Modal
          show={modalVisualize}
          threeLinesModal
          variant="modalText"
          textTitle1="Nome"
          text1={dataZustandVisualize.name}
          textTitle2="CNPJ"
          text2={dataZustandVisualize.cnpj}
          textTitle3="E-mail"
          text3={dataZustandVisualize.email}
          textTitle4="Telefone"
          text4={dataZustandVisualize.phone}
          textTitle5="Descrição"
          text5={dataZustandVisualize.description}
          textTitle6="Nome do Responsável"
          text6={dataZustandVisualize.responsible_name}
          textTitle7="Telefone do Responsável"
          text7={dataZustandVisualize.responsible_phone}
          textTitle8="Email do Responsável"
          text8={dataZustandVisualize.responsible_email}
          textTitle10="Outras Informações"
          text10={dataZustandVisualize.other || 'Nada declarado'}
          handleCloseModal={() => resetVisualize()}
        />
      )}
      <Atom.Container variant="main">
        <Organisms.HeaderList
          title="Empresas"
          searchLabel="Buscar empresa..."
          onClickAdd={handleAdd}
          onClickExclude={handleDelete}
          onInputChange={(text) => setInputText(text)}
        />
        {loading ? (
          <Atom.Loader size={35} width="100%" />
        ) : inputText !== '' && filteredList.length === 0 ? (
          <Atom.Container variant="filterError">
            <Atom.Text variant="filterError">
              Nenhuma empresa encontrado
            </Atom.Text>
          </Atom.Container>
        ) : (
          <Organisms.Error error={error}>
            <Organisms.Tables
              labels={label}
              data={
                filteredList.length !== 0 ? filteredList : parser(data?.data)
              }
              selectAll={selectAll}
              onClickSelectAll={handleSelectAll}
              onClickSelectItem={handleSelectItem}
              noSelect
            />
          </Organisms.Error>
        )}
        <Organisms.FooterList
          pages={numberPages}
          currentPage={offset}
          onNextPage={nextPage}
          onPrevPage={prevPage}
          onSelectPage={selectPage}
        />
      </Atom.Container>
    </>
  );
};

export default ListCompanies;
