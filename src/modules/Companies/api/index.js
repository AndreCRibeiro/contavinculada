import api from '../../../services/api';

const CompaniesEndpoints = {
  postCompanies: (data) => api.post('/empresas', data),
  updateCompanie: (data) => api.put(`/empresas/${data.id}`, data),
  getCompanies: () => api.get('/empresas'),
};

export default CompaniesEndpoints;
