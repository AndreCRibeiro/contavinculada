import React from 'react';
import { WrapContainer } from '../../../components/Organisms';
import Route from '../../../routes/Route';

import ListCompanies from '../pages/ListCompanies';
import CreateCompanies from '../pages/CreateCompanies';

const Routes = () => {
  return (
    <>
      <Route
        isPrivate
        path="/companie/list"
        exact
        component={() => (
          <WrapContainer>
            <ListCompanies />
          </WrapContainer>
        )}
      />
      <Route
        isPrivate
        path="/companie/create"
        exact
        component={() => (
          <WrapContainer>
            <CreateCompanies />
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
