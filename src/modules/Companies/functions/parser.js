export default (data) => {
  return data?.map((item) => ({
    selected: false,
    data: { ...item, name: item.name },
    columns: [
      {
        value: item?.name,
        width: '40%',
        type: 'text',
      },
      {
        value: item?.cnpj,
        width: '25%',
        type: 'text',
      },
      {
        value: item?.phone,
        width: '25%',
        type: 'text',
      },
    ],
  }));
};
