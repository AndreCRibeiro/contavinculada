import api from '../../../services/api';

const CollaboratorsEndpoints = {
  postCollaborator: (data) => api.post('/colaboradores', data),
  updateCollaborator: (data) => api.put(`/colaboradores/${data.id}`, data),
};

export default CollaboratorsEndpoints;
