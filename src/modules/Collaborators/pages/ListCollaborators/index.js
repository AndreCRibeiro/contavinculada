/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { format } from 'date-fns';
import { toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import useStore from '../../../../global/modalDelete/store';
import useStoreVisualize from '../../../../global/modalVisualize/store';
import { useTable, useListFetch, useAllData } from '../../../../hooks';
import api from '../../../../services/api';
import { label } from '../../mocks';
// logic
import parser from '../../functions/parser';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const ListCollaborators = () => {
  const [inputText, setInputText] = useState('');
  const [filteredList, setFilteredList] = useState([]);
  const history = useHistory();

  const {
    data,
    error,
    offset,
    loading,
    numberPages,
    nextPage,
    prevPage,
    selectPage,
  } = useListFetch('/colaboradores');

  const {
    selectAll,
    handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(parser(data?.data) || [], offset);

  const { allDataTable } = useAllData(parser(data?.all) || [], offset);

  const { dataZustand, modalDelete, reset, edit } = useStore();
  const {
    dataZustandVisualize,
    modalVisualize,
    resetVisualize,
  } = useStoreVisualize();

  const handleAdd = useCallback(() => history.push('/collaborator/create'), [
    history,
  ]);

  const handleEdit = (dataEdit) => {
    const bankID = dataEdit.banco?.split(' ');
    history.push('/collaborator/create', {
      data: dataEdit,
      bankID: bankID && bankID[0],
    });
  };

  useEffect(() => {
    if (edit) {
      handleEdit(dataZustand);
      reset();
    }
  }, [edit, dataZustand]);

  const handleDeleteItem = async (item) => {
    const response = await api.delete(`/colaboradores/${item.id}`);
    reset();
    if (response.status === 200) {
      toast.success('Colaborador(a) deletado(a) com sucesso', {
        position: toast.POSITION.TOP_CENTER,
      });
    } else {
      toast.error('Erro ao deletar colaboradpr(a)', {
        position: toast.POSITION.TOP_CENTER,
      });
    }
  };

  useEffect(() => {
    if (inputText !== '') {
      const teste = allDataTable.filter((input) => {
        if (input.data.name.toLowerCase().includes(inputText.toLowerCase())) {
          return true;
        }
        return false;
      });
      setFilteredList(teste);
    } else {
      setFilteredList([]);
    }
  }, [inputText]);

  return (
    <>
      {modalDelete && (
        <Molecules.Modal
          show={modalDelete}
          img="ALERT"
          alert
          variant="modalAlert"
          title={`Deseja realmente apagar ${dataZustand.name}`}
          handleCloseModal={() => reset()}
          handleDelete={() => handleDeleteItem(dataZustand)}
        />
      )}
      {modalVisualize && (
        <Molecules.Modal
          show={modalVisualize}
          collaboratorsModal
          variant="modalText"
          textTitle1="Nome"
          text1={dataZustandVisualize.name}
          textTitle2="Cargo"
          text2={dataZustandVisualize.dadosCargo.nome_cargo}
          textTitle3="Data de Nascimento"
          text3={
            dataZustandVisualize.data_nascimento === null
              ? '-'
              : format(
                new Date(dataZustandVisualize.data_nascimento),
                'dd/MM/yyyy'
              )
          }
          textTitle4="Sexo"
          text4={dataZustandVisualize.sexo || '-'}
          textTitle5="Estado civil"
          text5={dataZustandVisualize.estado_civil || '-'}
          textTitle6="RG"
          text6={dataZustandVisualize.rg || '-'}
          textTitle7="CPF"
          text7={dataZustandVisualize.cpf || '-'}
          textTitle8="Data de Admissão"
          text8={format(
            new Date(dataZustandVisualize.data_admissao),
            'dd/MM/yyyy'
          )}
          textTitle9="Data de Disponibilização"
          text9={format(
            new Date(dataZustandVisualize.data_disponibilizacao),
            'dd/MM/yyyy'
          )}
          textTitle10="Data de Desligamento"
          text10={
            dataZustandVisualize.data_desligamento === null
              ? '-'
              : format(
                new Date(dataZustandVisualize.data_desligamento),
                'dd/MM/yyyy'
              )
          }
          textTitle11="Estado"
          text11={dataZustandVisualize.estado || '-'}
          textTitle12="Cidade"
          text12={dataZustandVisualize.cidade || '-'}
          textTitle13="CEP"
          text13={dataZustandVisualize.cep || '-'}
          textTitle14="Rua"
          text14={dataZustandVisualize.rua || '-'}
          textTitle15="Bairro"
          text15={dataZustandVisualize.bairro || '-'}
          textTitle16="Número"
          text16={dataZustandVisualize.numero || '-'}
          textTitle17="Banco"
          text17={
            dataZustandVisualize.banco !== 'null'
              ? dataZustandVisualize.banco
              : '-'
          }
          textTitle18="Agência"
          text18={dataZustandVisualize.agencia || '-'}
          textTitle19="Conta"
          text19={dataZustandVisualize.conta || '-'}
          textTitle20="Situação"
          text20={dataZustandVisualize.situacao || '-'}
          textTitle21="Motivo"
          text21={
            dataZustandVisualize.motivo === 'null'
              ? '-'
              : dataZustandVisualize.motivo || '-'
          }
          textTitle22="Telefone"
          text22={dataZustandVisualize.telefone || '-'}
          handleCloseModal={() => resetVisualize()}
        />
      )}
      <Atom.Container variant="main">
        <Organisms.HeaderList
          title="Colaboradores"
          searchLabel="Buscar colaborador..."
          onClickAdd={handleAdd}
          onClickExclude={handleDelete}
          onInputChange={(text) => setInputText(text)}
        />
        {loading ? (
          <Atom.Loader size={35} width="100%" />
        ) : inputText !== '' && filteredList.length === 0 ? (
          <Atom.Container variant="filterError">
            <Atom.Text variant="filterError">
              Nenhum colaborador encontrado
            </Atom.Text>
          </Atom.Container>
        ) : (
          <Organisms.Error error={error}>
            <Organisms.Tables
              labels={label}
              data={
                filteredList.length !== 0 ? filteredList : parser(data?.data)
              }
              selectAll={selectAll}
              onClickSelectAll={handleSelectAll}
              onClickSelectItem={handleSelectItem}
              noSelect
            />
          </Organisms.Error>
        )}
        <Organisms.FooterList
          pages={numberPages}
          currentPage={offset}
          onNextPage={nextPage}
          onPrevPage={prevPage}
          onSelectPage={selectPage}
        />
      </Atom.Container>
    </>
  );
};

export default ListCollaborators;
