/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable camelcase */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import { format } from 'date-fns';
import { useListPickerFetch, useLazyFetch } from '../../../../hooks';
import { genre, civilState, banks, schema } from '../../mocks';
import CollaboratorsEndpoints from '../../api';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const CreateCollaborators = () => {
  const history = useHistory();
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [update, setUpdate] = useState(false);
  const [toggle, setToggle] = useState(false);
  const [selected, setSelected] = useState('');

  const [fetch, { response }] = useLazyFetch(
    CollaboratorsEndpoints.postCollaborator
  );
  const [updateReq, { response: updateResponse }] = useLazyFetch(
    CollaboratorsEndpoints.updateCollaborator
  );

  const { register, handleSubmit, errors, reset, watch } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      name: history.location?.state ? history.location?.state.data.name : null,
      empresa: history.location?.state
        ? history.location?.state.data.empresa
        : null,
      contrato: history.location?.state
        ? history.location?.state.data.contrato
        : null,
      cargo: history.location?.state
        ? history.location?.state.data.cargo
        : null,
      sexo: history.location?.state ? history.location?.state.data.sexo : null,
      rg: history.location?.state ? history.location?.state.data.rg : null,
      cpf: history.location?.state ? history.location?.state.data.cpf : null,
      data_nascimento: history.location?.state
        ? history.location?.state.data?.data_nascimento !== null
          ? format(
            new Date(history.location?.state.data.data_nascimento),
            'dd/MM/yyyy'
          )
          : null
        : null,
      telefone: history.location?.state
        ? history.location?.state.data.telefone
        : null,
      estado_civil: history.location?.state
        ? history.location?.state.data.estado_civil
        : null,
      data_admissao: history.location?.state
        ? format(
          new Date(history.location?.state.data.data_admissao),
          'dd/MM/yyyy'
        )
        : null,
      data_disponibilizacao: history.location?.state
        ? format(
          new Date(history.location?.state.data.data_disponibilizacao),
          'dd/MM/yyyy'
        )
        : null,
      estado: history.location?.state
        ? history.location?.state.data.estado
        : null,
      cidade: history.location?.state
        ? history.location?.state.data.cidade
        : null,
      numero: history.location?.state
        ? history.location?.state.data.numero
        : null,
      rua: history.location?.state ? history.location?.state.data.rua : null,
      bairro: history.location?.state
        ? history.location?.state.data.bairro
        : null,
      cep: history.location?.state ? history.location?.state.data.cep : null,
      banco: history.location?.state ? history.location?.state.bankID : null,
      agencia: history.location?.state
        ? history.location?.state.data.agencia
        : null,
      conta: history.location?.state
        ? history.location?.state.data.conta
        : null,
    },
  });

  const { data: companiesData } = useListPickerFetch('/listEmpresas');

  const { data: contractsData } = useListPickerFetch(
    `/contratos?empresa=${watch('empresa')}`
  );
  const { data: positionData } = useListPickerFetch(
    `/cargos?contrato=${watch('contrato')}`
  );

  const onSubmit = (form) => {
    const slectedBack = banks.find((item) => item.id === form.banco);
    const editedBank = `${form.banco ? `${slectedBack?.id} - ${slectedBack?.name}` : null
      }`;
    const { data_nascimento, data_admissao, data_disponibilizacao } = form;
    const dataNascimento = data_nascimento.split('/');
    const dataAdmissao = data_admissao.split('/');
    const dataDisponibilizacao = data_disponibilizacao.split('/');

    fetch({
      ...form,
      empresa: parseInt(form.empresa, 0),
      contrato: parseInt(form.contrato, 0),
      cargo: parseInt(form.cargo, 0),
      data_nascimento: dataNascimento
        ? new Date(dataNascimento[2], dataNascimento[1] - 1, dataNascimento[0])
        : null,
      data_admissao: new Date(
        dataAdmissao[2],
        dataAdmissao[1] - 1,
        dataAdmissao[0]
      ),
      data_disponibilizacao: new Date(
        dataDisponibilizacao[2],
        dataDisponibilizacao[1] - 1,
        dataDisponibilizacao[0]
      ),
      banco: editedBank === 'null' ? null : editedBank,
      motivo: null,
      agencia: form.agencia || null,
      bairro: form.bairro || null,
      cep: form.cep || null,
      cidade: form.cidade || null,
      conta: form.conta || null,
      estado: form.estado || null,
      estado_civil: form.estado_civil || null,
      numero: form.numero || null,
      rg: form.rg || null,
      rua: form.rua || null,
      sexo: form.sexo || null,
      telefone: form.telefone || null,
    });
  };

  const onUpdateSubmit = (form, { id = history.location.state.data.id }) => {
    const slectedBack = banks?.find((item) => item.id === form.banco);
    const editedBank = `${form.banco ? `${slectedBack?.id} - ${slectedBack?.name}` : null
      }`;
    const {
      data_nascimento,
      data_admissao,
      data_disponibilizacao,
      data_desligamento,
    } = form;
    const dataNascimento = data_nascimento.split('/');
    const dataAdmissao = data_admissao.split('/');
    const dataDisponibilizacao = data_disponibilizacao.split('/');
    const dataDesligamento = data_desligamento?.split('/');
    updateReq({
      ...form,
      empresa: parseInt(form.empresa, 0),
      contrato: parseInt(form.contrato, 0),
      cargo: parseInt(form.cargo, 0),
      data_nascimento: dataNascimento
        ? new Date(dataNascimento[2], dataNascimento[1] - 1, dataNascimento[0])
        : null,
      data_admissao: new Date(
        dataAdmissao[2],
        dataAdmissao[1] - 1,
        dataAdmissao[0]
      ),
      data_disponibilizacao: new Date(
        dataDisponibilizacao[2],
        dataDisponibilizacao[1] - 1,
        dataDisponibilizacao[0]
      ),
      data_desligamento: dataDesligamento
        ? new Date(
          dataDesligamento[2],
          dataDesligamento[1] - 1,
          dataDesligamento[0]
        )
        : null,
      banco: editedBank === 'null' ? null : editedBank,
      situacao: toggle ? 'ativo' : 'inativo',
      id,
      agencia: form.agencia || null,
      bairro: form.bairro || null,
      cep: form.cep || null,
      cidade: form.cidade || null,
      conta: form.conta || null,
      estado: form.estado || null,
      estado_civil: form.estado_civil || null,
      numero: form.numero || null,
      rg: form.rg || null,
      rua: form.rua || null,
      sexo: form.sexo || null,
      telefone: form.telefone || null,
    });
  };

  useEffect(() => {
    if (history.location?.state) {
      setUpdate(true);
    }
  }, []);

  useEffect(() => {
    if (history.location?.state?.data?.situacao === 'ativo') {
      setToggle(true);
    }
  }, [update]);

  useEffect(() => {
    if (response) {
      setModal(true);
      reset(
        {},
        {
          errors: false,
        }
      );
    }
    if (updateResponse) {
      setModalUpdate(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response, updateResponse]);

  const handleCancel = useCallback(() => history.push('/collaborator/list'), [
    history,
  ]);

  const handleToggle = () => {
    setToggle(!toggle);
  };

  useEffect(() => {
    const dataNascimento = watch('data_nascimento').split('/');
    console.log(dataNascimento);
  }, [watch('data_nascimento')]);

  return (
    <>
      <Atom.Container variant="main">
        <Atom.Form
          onSubmit={
            update ? handleSubmit(onUpdateSubmit) : handleSubmit(onSubmit)
          }
        >
          <Organisms.HeaderCreate
            title={update ? 'Atualizar Colaborador' : 'Colaboradores'}
            searchLabel="Buscar empresas..."
            onClickCancel={handleCancel}
          />

          <Atom.Container
            variant="create"
            justifyContent="flex-start"
            mt="MEDIUM"
          >
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                name="name"
                register={register}
                error={errors?.name?.message}
                percent={60}
                label="Nome"
                placeholder="Nome do Colaborador"
              />
              <Molecules.Picker
                name="empresa"
                register={register}
                percent={37.5}
                label="Empresa"
                options={companiesData}
                placeholderDependency="Carregando..."
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.Picker
                name="contrato"
                register={register}
                error={errors?.contrato?.message}
                percent={30}
                label="Contrato"
                options={contractsData?.contratos}
                placeholderDependency={
                  companiesData
                    ? 'Selecione uma empresa'
                    : 'Selecione uma empresa'
                }
              />
              <Molecules.Picker
                name="cargo"
                register={register}
                error={
                  errors?.cargo?.message ||
                  (positionData?.length === 0 &&
                    'Não ha cargos para esse contrato')
                }
                percent={35}
                label="Cargo"
                options={positionData}
                placeholderDependency={
                  contractsData
                    ? 'Selecione um contrato'
                    : 'Selecione um contrato'
                }
              />
              <Molecules.Picker
                name="sexo"
                register={register}
                error={errors?.sexo?.message}
                percent={30}
                label="Sexo"
                options={genre}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Atom.Container variant="create" mr="BIG">
                <Atom.Container variant="lineCreate">
                  <Molecules.InputForm
                    name="rg"
                    register={register}
                    error={errors?.rg?.message}
                    label="RG"
                    percent={22.5}
                    placeholder="000000000-00"
                  />
                  <Molecules.InputForm
                    name="cpf"
                    register={register}
                    error={errors?.cpf?.message}
                    withMask
                    mask="999.999.999-99"
                    label="CPF"
                    percent={22.5}
                    placeholder="XXX.XXX.XXX-XX"
                  />
                  <Molecules.InputForm
                    name="data_nascimento"
                    register={register}
                    error={errors?.data_nascimento?.message}
                    withMask
                    mask="99/99/9999"
                    percent={22.5}
                    label="Data Nascimento"
                    placeholder="XX/XX/XXXX"
                  />
                  <Molecules.InputForm
                    name="telefone"
                    register={register}
                    error={errors?.telefone?.message}
                    withMask
                    mask="(99) 99999-9999"
                    label="Telefone"
                    percent={22.5}
                    placeholder="(XX) XXXXX-XXXX"
                  />
                </Atom.Container>
                <Atom.Container variant="lineCreate">
                  <Molecules.Picker
                    name="estado_civil"
                    register={register}
                    error={errors?.estado_civil?.message}
                    percent={40}
                    label="Estado Civil"
                    options={civilState}
                  />
                  <Molecules.InputForm
                    name="data_admissao"
                    register={register}
                    error={errors?.data_admissao?.message}
                    withMask
                    mask="99/99/9999"
                    percent={27.5}
                    label="Data Admissão"
                    placeholder="XX/XX/XXXX"
                  />
                  <Molecules.InputForm
                    name="data_disponibilizacao"
                    register={register}
                    error={errors?.data_disponibilizacao?.message}
                    withMask
                    mask="99/99/9999"
                    percent={27.5}
                    label="Data Disponibilização"
                    placeholder="XX/XX/XXXX"
                  />
                </Atom.Container>
                <Atom.Container variant="divider" />
                <Atom.Container variant="lineCreate">
                  <Molecules.InputForm
                    name="estado"
                    register={register}
                    error={errors?.estado?.message}
                    percent={35}
                    label="Estado"
                    placeholder="Estado"
                  />
                  <Molecules.InputForm
                    name="cidade"
                    register={register}
                    error={errors?.cidade?.message}
                    percent={35}
                    label="Cidade"
                    placeholder="Cidade"
                  />
                  <Molecules.InputForm
                    name="numero"
                    register={register}
                    error={errors?.numero?.message}
                    percent={25}
                    label="Número"
                    placeholder="Número"
                  />
                </Atom.Container>
              </Atom.Container>
              <Atom.Avatar variant="create" />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                name="rua"
                register={register}
                error={errors?.rua?.message}
                percent={40}
                label="Rua"
                placeholder="Rua"
              />
              <Molecules.InputForm
                name="bairro"
                register={register}
                error={errors?.bairro?.message}
                percent={35}
                label="Bairro"
                placeholder="Bairro"
              />
              <Molecules.InputForm
                name="cep"
                register={register}
                error={errors?.cep?.message}
                mask="99999-999"
                withMask
                percent={20}
                label="Cep"
                placeholder="XXXXX-XXX"
              />
            </Atom.Container>
            <Atom.Container variant="divider" />
            <Atom.Container variant="lineCreate">
              <Molecules.Picker
                name="banco"
                register={register}
                error={errors?.banco?.message}
                percent={50}
                showId
                label="Banco"
                options={banks}
              />
              <Molecules.InputForm
                name="agencia"
                register={register}
                error={errors?.agencia?.message}
                percent={20}
                label="Agência"
                placeholder="Agência"
              />
              <Molecules.InputForm
                name="conta"
                register={register}
                error={errors?.conta?.message}
                percent={22.5}
                label="Conta"
                placeholder="Conta"
              />
            </Atom.Container>
            {update && (
              <>
                <Atom.Container variant="divider" />
                <Atom.Container variant="lineCreate">
                  <Atom.ToggleCollab
                    name="situacao"
                    register={register}
                    label="Status"
                    percent={10}
                    onChange={handleToggle}
                    checked={toggle}
                  />
                  {!toggle && (
                    <>
                      <Atom.RadioCollab
                        name="radio"
                        register={register}
                        label="Motivo do desligamento"
                        percent={20}
                        onChange={(e) => setSelected(e.target.value)}
                      />
                      <Molecules.InputForm
                        name="data_desligamento"
                        register={register}
                        error={errors?.data_desligamento?.message}
                        withMask
                        mask="99/99/9999"
                        percent={60}
                        label="Data Desligamento"
                        placeholder="XX/XX/XXXX"
                      />
                    </>
                  )}
                </Atom.Container>
                {selected === 'outros' && (
                  <Atom.Container variant="lineCreate">
                    <Molecules.InputForm
                      name="motivo"
                      register={register}
                      error={errors?.conta?.message}
                      percent={100}
                      label="Motivo"
                      placeholder="Motivo"
                      multiLine
                    />
                  </Atom.Container>
                )}
              </>
            )}
          </Atom.Container>
        </Atom.Form>
      </Atom.Container>
      <Molecules.Modal
        show={modal}
        img="SUCCESS"
        confirm
        variant="modalSuccess"
        title="Colaborador adicionado com sucesso"
        textBlueButton="ADICIONAR NOVO COLABORADOR"
        textBack="VOLTAR PARA COLABORADORES"
        handleCloseModal={() => setModal(false)}
        handleNavigate={() => setModal(false)}
        handleNavigateToList={handleCancel}
      />
      <Molecules.Modal
        show={modalUpdate}
        img="SUCCESS"
        confirm
        variant="modalSuccess"
        title="Colaborador alterada com sucesso"
        textBlueButton="VOLTAR PARA COLABORADORES"
        textBack="ALTERAR NOVAMENTE"
        handleNavigate={handleCancel}
        handleNavigateToList={() => setModalUpdate(false)}
      />
    </>
  );
};

export default CreateCollaborators;
