export const label = [
  {
    value: 'Nome',
    width: '20%',
  },
  {
    value: 'Contrato',
    width: '8%',
  },
  {
    value: 'Cargo',
    width: '18%',
  },
  {
    value: 'CPF',
    width: '12%',
  },
  {
    value: 'Status',
    width: '12%',
  },
  {
    value: 'Data Admissão',
    width: '10%',
  },
  {
    value: 'Data Disponib.',
    width: '10%',
  },
];
