const genre = [
  {
    id: 'Masculino',
    name: 'Masculino',
  },
  { id: 'Feminino', name: 'Feminino' },
  { id: 'Outros', name: 'Outros' },
];

const civilState = [
  {
    id: 'Casado',
    name: 'Casado',
  },
  {
    id: 'Solteiro',
    name: 'Solteiro',
  },
  {
    id: 'Divorciado',
    name: 'Divorciado',
  },
  {
    id: 'Viuvo',
    name: 'Viuvo',
  },
];

export { genre, civilState };
export * from './banks';
export * from './validation';
export * from './label';
