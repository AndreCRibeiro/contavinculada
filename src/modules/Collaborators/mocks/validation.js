import * as yup from 'yup';

export const schema = yup.object().shape({
  name: yup.string().required('Nome é obrigatorio'),
  data_nascimento: yup.string(),
  rg: yup.string(),
  cpf: yup.string().required('Cpf é obrigatorio'),
  telefone: yup.string(),
  estado: yup.string(),
  cidade: yup.string(),
  numero: yup.string(),
  rua: yup.string(),
  bairro: yup.string(),
  cep: yup.string(),
  agencia: yup.string(),
  conta: yup.string(),
  data_admissao: yup.string().required('Data de admissão é obrigatorio'),
  data_disponibilizacao: yup
    .string()
    .required('Data de disponibilização é obrigatorio'),
  data_desligamento: yup.string(),
  motivo: yup.string(),
  empresa: yup.string().required('Seleção de empresa é obrigatório'),
  contrato: yup.string().required('Seleção de contrato é obrigatório'),
  cargo: yup.string().required('Seleção do cargo é obrigatório'),
});
