import { format } from 'date-fns';

export default (data) => {
  return data?.map((item) => ({
    selected: false,
    data: { ...item, name: item.name },
    columns: [
      {
        value: item.name,
        width: '20%',
        type: 'text',
      },
      {
        value: item.dadosContrato?.numero_contrato,
        width: '8%',
        type: 'text',
      },
      {
        value: item.dadosCargo?.nome_cargo,
        width: '18%',
        type: 'text',
      },
      {
        value: item.cpf,
        width: '12%',
        type: 'text',
      },
      {
        value: item.situacao !== 'inativo',
        width: '12%',
        type: 'status',
      },
      {
        value: format(new Date(item.data_admissao), 'dd/MM/yyyy'),
        width: '10%',
        type: 'text',
      },
      {
        value: format(new Date(item.data_disponibilizacao), 'dd/MM/yyyy'),
        width: '10%',
        type: 'text',
      },
    ],
  }));
};
