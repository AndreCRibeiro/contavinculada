import React from 'react';
import { Route } from 'react-router-dom';
import { WrapContainer } from '../../../components/Organisms';

import ListCollaborators from '../pages/ListCollaborators';
import CreateCollaborators from '../pages/CreateCollaboratos';

const Routes = () => {
  return (
    <>
      <Route
        path="/collaborator/list"
        exact
        component={() => (
          <WrapContainer>
            <ListCollaborators />
          </WrapContainer>
        )}
      />
      <Route
        path="/collaborator/create"
        exact
        component={() => (
          <WrapContainer>
            <CreateCollaborators />
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
