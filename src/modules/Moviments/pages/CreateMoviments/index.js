import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import * as Atom from '../../../../components/Atoms';
// import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const CreateMoviment = () => {
  const history = useHistory();

  const handleCancel = useCallback(() => history.push('/moviment/list'), [
    history,
  ]);

  return (
    <Atom.Container variant="main">
      <Organisms.HeaderCreate
        title="Movimentações"
        searchLabel="Buscar empresas..."
        onClickCancel={handleCancel}
      />
      <Atom.Container
        variant="create"
        justifyContent="flex-start"
        mt="MEDIUM"
      />
    </Atom.Container>
  );
};

export default CreateMoviment;
