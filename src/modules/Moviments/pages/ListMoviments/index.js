import React, { useState, useMemo, useEffect } from 'react';
import { format, addYears } from 'date-fns';
import { useListPickerFetch } from '../../../../hooks';
import api from '../../../../services/api';

// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const ListMoviments = () => {
  const { data: dataContracts } = useListPickerFetch(`/listContratos`);

  // States
  const [showButton, setShowButton] = useState(false);
  const [contractOptions, setContractOptions] = useState();
  const [params, setParams] = useState({
    idContrato: '',
    rubrica: '',
    data: '',
    dataFinal: '',
  });
  const [secondYearView, setSecondYearView] = useState();
  const [secondYear, setSecondYear] = useState();

  // Memo
  const finishDate = useMemo(() => {
    const { data } = params;
    const parse = new Date(data);
    const yearPlusOne = new Date(addYears(parse, 1));
    setSecondYear(false);
    return yearPlusOne;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [secondYear]);

  // Functions
  const changeParams = (value, key) => {
    const data = value.target.value;
    if (key === 'rubrica') {
      setParams((prevState) => ({ ...prevState, rubrica: data }));
    }
    if (key === 'id') {
      setParams((prevState) => ({ ...prevState, idContrato: data }));
    }
  };

  useEffect(() => {
    if (dataContracts) {
      setContractOptions(dataContracts);
    }
  }, [dataContracts]);

  useEffect(() => {
    const { idContrato, rubrica } = params;
    if (idContrato && rubrica) {
      setShowButton(true);
    }
  }, [params]);

  useEffect(() => {
    if (params.data) {
      setSecondYearView(true);
      setSecondYear(true);
    }
  }, [params.data]);

  const handleDownload = (item) => {
    const { data, dataFinal, idContrato } = params;

    if (item === 'rescisao') {
      const link = document.createElement('a');
      link.href = `${api.baseURL}/relatoriorescisao/${idContrato}`;
      const response = {
        file: `${api.defaults.baseURL}/relatoriorescisao/${idContrato}`,
      };
      window.open(response.file);
    }
    if (item === '13salario') {
      const link = document.createElement('a');
      link.href = `${api.baseURL}/relatorio13salario/${idContrato}/${format(
        new Date(data),
        'yyyy'
      )}`;
      const response = {
        file: `${api.defaults.baseURL
          }/relatorio13salario/${idContrato}/${format(new Date(data), 'yyyy')}`,
      };
      window.open(response.file);
    }

    if (item === 'ferias') {
      const link = document.createElement('a');
      link.href = `${api.baseURL}/relatorioferias/${idContrato}/${format(
        new Date(data),
        'yyyy'
      )}/${format(new Date(dataFinal), 'yyyy')}`;
      const response = {
        file: `${api.defaults.baseURL}/relatorioferias/${idContrato}/${format(
          new Date(data),
          'yyyy'
        )}/${format(new Date(dataFinal), 'yyyy')}`,
      };
      window.open(response.file);
    }
  };

  const handleDownloadDoc = () => {
    const { rubrica } = params;
    switch (rubrica) {
      case 'rescisao':
        handleDownload(rubrica);
        break;
      case '13salario':
        handleDownload(rubrica);
        break;
      case 'ferias':
        handleDownload(rubrica);
        break;
      default:
        break;
    }
  };

  return (
    <Atom.Container variant="main">
      <Organisms.HeaderOptions
        title="Relatórios"
        searchLabel={false}
        onClickAdd={false}
        download={[]}
        showButton={false}
        openModal={false}
        infos={[]}
      />
      <Atom.Container variant="list">
        <Atom.Text variant="list" mb="XBIG">
          Escolha as seguintes opções para filtrar a listagem:
        </Atom.Text>
        <Molecules.PickerList
          name="ID Contrato - Empresa"
          label="ID"
          options={contractOptions}
          placeholderDependency="Carregando..."
          onChange={(e) => changeParams(e, 'id')}
          list
          showId
          teste
        />
        <Molecules.PickerList
          name="Rubrica"
          label="Rubrica"
          options={[
            { id: 'rescisao', name: 'Rescisão' },
            { id: '13salario', name: '13º Salário' },
            { id: 'ferias', name: 'Férias + 13' },
          ]}
          placeholderDependency="Carregando..."
          onChange={(e) => changeParams(e, 'rubrica')}
          list
          teste
        />
        {params.rubrica && params.rubrica === '13salario' && (
          <Molecules.CalenderPicker
            viewYearCalendar
            percent={25}
            onChange={(e) => {
              setParams((prevState) => ({
                ...prevState,
                data: format(e, "yyyy-MM-dd'T'HH:mm:ss"),
              }));
            }}
            label="Ano"
          />
        )}
        {params.rubrica &&
          params.rubrica !== 'rescisao' &&
          params.rubrica !== '13salario' && (
            <Molecules.CalenderPicker
              viewYearCalendar
              percent={25}
              onChange={(e) => {
                setParams((prevState) => ({
                  ...prevState,
                  data: format(e, "yyyy-MM-dd'T'HH:mm:ss"),
                }));
              }}
              label="Ano início"
            />
          )}
        {params.rubrica === 'ferias' && secondYearView && (
          <Molecules.CalenderPicker
            viewYearCalendar
            percent={25}
            defaultValue={finishDate || new Date()}
            onChange={(e) => {
              setParams((prevState) => ({
                ...prevState,
                dataFinal: format(e, "yyyy-MM-dd'T'HH:mm:ss"),
              }));
            }}
            label="Ano fim"
            spaced
            minDate={params.data}
          />
        )}

        {showButton && (
          <Molecules.Button
            onClick={handleDownloadDoc}
            text="Baixar"
            mt="MEDIUM"
          />
        )}
      </Atom.Container>
      <Organisms.FooterList pages={[1, 1, 1, 1, 1]} currentPage={1} />
    </Atom.Container>
  );
};

export default ListMoviments;
