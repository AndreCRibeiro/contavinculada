import React from 'react';
import { Route } from 'react-router-dom';
import { WrapContainer } from '../../../components/Organisms';

import ListMoviments from '../pages/ListMoviments';
import CreateMoviment from '../pages/CreateMoviments';

const Routes = () => {
  return (
    <>
      <Route
        path="/reports/list"
        exact
        component={() => (
          <WrapContainer>
            <ListMoviments />
          </WrapContainer>
        )}
      />
      <Route
        path="/reports/create"
        exact
        component={() => (
          <WrapContainer>
            <CreateMoviment />
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
