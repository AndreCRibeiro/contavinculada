import { format } from 'date-fns';

export default (data) => {
  return data?.map((item) => ({
    selected: false,
    data: { ...item, name: item.numero_contrato },
    columns: [
      {
        value: item?.numero_contrato,
        width: '7%',
        type: 'text',
      },
      {
        value: item?.dadosEmpresa.name,
        width: '20%',
        type: 'text',
      },
      {
        value: item?.descricao_contrato,
        width: '47%',
        type: 'text',
      },
      {
        value: format(new Date(item?.data_inicio), 'dd/MM/yyyy'),
        width: '8%',
        type: 'text',
      },
      {
        value: format(new Date(item?.data_fim), 'dd/MM/yyyy'),
        width: '8%',
        type: 'text',
      },
    ],
  }));
};
