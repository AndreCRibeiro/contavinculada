import React from 'react';
import { WrapContainer } from '../../../components/Organisms';
import Route from '../../../routes/Route';

import ListContracts from '../pages/ListContracts';
import CreateContract from '../pages/CreateContracts';

const Routes = () => {
  return (
    <>
      <Route
        isPrivate
        path="/contract/list"
        exact
        component={() => (
          <WrapContainer>
            <ListContracts />
          </WrapContainer>
        )}
      />
      <Route
        isPrivate
        path="/contract/create"
        exact
        component={() => (
          <WrapContainer>
            <CreateContract />
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
