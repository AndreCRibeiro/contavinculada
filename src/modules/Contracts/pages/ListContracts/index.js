/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { format } from 'date-fns';
import useStore from '../../../../global/modalDelete/store';
import useStoreVisualize from '../../../../global/modalVisualize/store';
import { useTable, useListFetch, useAllData } from '../../../../hooks';
import api from '../../../../services/api';
import { label } from '../../mocks';

// logic
import parser from '../../functions/parser';

// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const ListContracts = () => {
  const [inputText, setInputText] = useState('');
  const [filteredList, setFilteredList] = useState([]);
  const history = useHistory();

  const {
    data,
    error,
    offset,
    loading,
    numberPages,
    nextPage,
    prevPage,
    selectPage,
  } = useListFetch('/contratos');

  const {
    selectAll,
    handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(parser(data?.data) || [], offset);

  const { allDataTable } = useAllData(parser(data?.all) || [], offset);

  const { dataZustand, modalDelete, reset, edit } = useStore();
  const {
    dataZustandVisualize,
    modalVisualize,
    resetVisualize,
  } = useStoreVisualize();

  const handleAdd = useCallback(() => history.push('/contract/create'), [
    history,
  ]);

  const handleEdit = (dataEdit) => {
    history.push('/contract/create', { data: dataEdit });
  };

  useEffect(() => {
    if (edit) {
      handleEdit(dataZustand);
      reset();
    }
  }, [edit, dataZustand]);

  const handleDeleteItem = async (item) => {
    const response = await api.delete(`/contratos/${item.id}`);
    reset();
    if (response.status === 200) {
      toast.success('Contrato deletado com sucesso', {
        position: toast.POSITION.TOP_CENTER,
      });
    } else {
      toast.error('Erro ao deletar contrato', {
        position: toast.POSITION.TOP_CENTER,
      });
    }
  };

  useEffect(() => {
    if (inputText !== '') {
      const teste = allDataTable.filter((input) => {
        if (input.data.name.includes(inputText)) {
          return true;
        }
        return false;
      });
      setFilteredList(teste);
    } else {
      setFilteredList([]);
    }
  }, [inputText]);

  return (
    <>
      {modalDelete && (
        <Molecules.Modal
          show={modalDelete}
          img="ALERT"
          alert
          variant="modalAlert"
          title={`Deseja realmente apagar ${dataZustand.name}`}
          handleCloseModal={() => reset()}
          handleDelete={() => handleDeleteItem(dataZustand)}
        />
      )}
      {modalVisualize && (
        <Molecules.Modal
          show={modalVisualize}
          contractModal
          variant="modalText"
          textTitle1="Empresa"
          text1={dataZustandVisualize.dadosEmpresa.name}
          textTitle2="Contrato"
          text2={dataZustandVisualize.name}
          textTitle3="Data inicial do contrato"
          text3={format(
            new Date(dataZustandVisualize.data_inicio),
            'dd/MM/yyyy'
          )}
          textTitle4="Data final do contrato"
          text4={format(new Date(dataZustandVisualize.data_fim), 'dd/MM/yyyy')}
          textTitle5="Descrição"
          text5={dataZustandVisualize.descricao_contrato}
          textTitle6="13º Salario"
          text6={`${dataZustandVisualize.decimo_terceiro}%`}
          textTitle7="Férias / Adicionais"
          text7={`${dataZustandVisualize.ferias_adicionais}%`}
          textTitle8="Multa FGTS"
          text8={`${dataZustandVisualize.recisao_fgts}%`}
          textTitle9="Encargos"
          text9={`${parseFloat(dataZustandVisualize.encargos, 1.0)}%`}
          textTitle10="Conta Vinculada"
          text10={dataZustandVisualize.conta_vinculada}
          textTitle11="Processo"
          text11={dataZustandVisualize.processo}
          textTitle12="Total"
          text12={`${(
            dataZustandVisualize.decimo_terceiro +
            dataZustandVisualize.ferias_adicionais +
            dataZustandVisualize.encargos +
            dataZustandVisualize.recisao_fgts
          ).toFixed(2)}%`}
          handleCloseModal={() => resetVisualize()}
        />
      )}
      <Atom.Container variant="main">
        <Organisms.HeaderList
          title="Contratos"
          searchLabel="Buscar contrato..."
          onClickAdd={handleAdd}
          onClickExclude={handleDelete}
          onInputChange={(text) => setInputText(text)}
        />
        {loading ? (
          <Atom.Loader size={35} width="100%" />
        ) : inputText !== '' && filteredList.length === 0 ? (
          <Atom.Container variant="filterError">
            <Atom.Text variant="filterError">
              Nenhuma contrato encontrado
            </Atom.Text>
          </Atom.Container>
        ) : (
          <Organisms.Error error={error}>
            <Organisms.Tables
              labels={label}
              data={
                filteredList.length !== 0 ? filteredList : parser(data?.data)
              }
              selectAll={selectAll}
              onClickSelectAll={handleSelectAll}
              onClickSelectItem={handleSelectItem}
              noSelect
            />
          </Organisms.Error>
        )}
        <Organisms.FooterList
          pages={numberPages}
          currentPage={offset}
          onNextPage={nextPage}
          onPrevPage={prevPage}
          onSelectPage={selectPage}
        />
      </Atom.Container>
    </>
  );
};

export default ListContracts;
