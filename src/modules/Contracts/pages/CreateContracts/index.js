/* eslint-disable no-nested-ternary */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import { format } from 'date-fns';
import { schema } from '../../mocks';
// logic
import ContractsEndpoints from '../../api';
import { useLazyFetch, useListPickerFetch } from '../../../../hooks';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const CreateContract = () => {
  const history = useHistory();

  const [companiesOptions, setcompaniesOptions] = useState();
  const [fiscalsOptions, setFiscalOptions] = useState();

  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [update, setUpdate] = useState(false);

  const [fetch, { response }] = useLazyFetch(ContractsEndpoints.postContracts);
  const [updateReq, { response: updateResponse }] = useLazyFetch(
    ContractsEndpoints.updateContract
  );

  const { data } = useListPickerFetch('/listEmpresas');
  const { data: dataFiscal } = useListPickerFetch('/users/fiscal');

  useEffect(() => {
    if (data) {
      setcompaniesOptions(data);
    }
    if (dataFiscal) {
      setFiscalOptions(dataFiscal);
    }
  }, [data, dataFiscal]);

  const { register, handleSubmit, errors, reset, watch } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      descricao_contrato: history.location?.state
        ? history.location?.state.data.descricao_contrato
        : null,
      data_inicio: history.location?.state
        ? format(
          new Date(history.location?.state.data.data_inicio),
          'dd/MM/yyyy'
        )
        : null,
      data_fim: history.location?.state
        ? format(new Date(history.location?.state.data.data_fim), 'dd/MM/yyyy')
        : null,
      numero_contrato: history.location?.state
        ? history.location?.state.data.numero_contrato
        : null,
      empresa: history.location?.state
        ? history.location?.state.data.empresa
        : null,
      decimo_terceiro: history.location?.state
        ? history.location?.state.data.decimo_terceiro
        : null,
      ferias_adicionais: history.location?.state
        ? history.location?.state.data.ferias_adicionais
        : null,
      recisao_fgts: history.location?.state
        ? history.location?.state.data.recisao_fgts
        : null,
      encargos: history.location?.state
        ? history.location?.state.data.encargos
        : null,
      conta_vinculada: history.location?.state
        ? history.location?.state.data.conta_vinculada
        : null,
      processo: history.location?.state
        ? history.location?.state.data.processo
        : null,
    },
  });

  const add = [
    watch('decimo_terceiro'),
    watch('ferias_adicionais'),
    watch('recisao_fgts'),
    watch('encargos'),
  ];

  const addResult = add.map((aliquot) =>
    Number(aliquot?.toString().replace(',', '.'))
  );

  // const addResult = add.map(Number);

  const total = addResult.reduce((a, b) => a + b, 0).toFixed(2);

  const onSubmit = (form) => {
    const { data_fim, data_inicio } = form;
    const dataFim = data_fim.split('/');
    const dataincio = data_inicio.split('/');
    fetch({
      ...form,
      decimo_terceiro: Number(form.decimo_terceiro?.replace(',', '.')),
      ferias_adicionais: Number(form.ferias_adicionais?.replace(',', '.')),
      recisao_fgts: Number(form.recisao_fgts?.replace(',', '.')),
      encargos: Number(form.encargos?.replace(',', '.')),
      data_fim: new Date(dataFim[2], dataFim[1] - 1, dataFim[0]),
      data_inicio: new Date(dataincio[2], dataincio[1] - 1, dataincio[0]),
    });
  };

  const onUpdateSubmit = (form, { id = history.location.state.data.id }) => {
    const { data_fim, data_inicio } = form;
    const dataFim = data_fim.split('/');
    const dataincio = data_inicio.split('/');
    updateReq({
      ...form,
      decimo_terceiro: Number(form.decimo_terceiro?.replace(',', '.')),
      ferias_adicionais: Number(form.ferias_adicionais?.replace(',', '.')),
      recisao_fgts: Number(form.recisao_fgts?.replace(',', '.')),
      encargos: Number(form.encargos?.replace(',', '.')),
      data_fim: new Date(dataFim[2], dataFim[1] - 1, dataFim[0]),
      data_inicio: new Date(dataincio[2], dataincio[1] - 1, dataincio[0]),
      id,
    });
  };

  useEffect(() => {
    if (history.location?.state) {
      setUpdate(true);
    }
  }, []);

  useEffect(() => {
    if (response) {
      setModal(true);
      reset(
        {},
        {
          errors: false,
        }
      );
    }
    if (updateResponse) {
      setModalUpdate(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response, updateResponse]);

  const handleCancel = useCallback(() => history.push('/contract/list'), [
    history,
  ]);

  return (
    <>
      <Molecules.Modal
        show={modal}
        img="SUCCESS"
        confirm
        blackButton
        variant="modalSuccess"
        title="Contrato adicionado com sucesso"
        textBlueButton="ADICIONAR NOVO CARGO"
        textBlackButton="ADICIONAR NOVO CONTRATO"
        textBack="VOLTAR PARA CONTRATOS"
        handleCloseModal={() => setModal(false)}
        handleNavigate={() => history.push('/position/create')}
        handleNavigateToList={() => history.push('/contract/list')}
      />
      <Molecules.Modal
        show={modalUpdate}
        img="SUCCESS"
        confirm
        variant="modalSuccess"
        title="Contrato alterada com sucesso"
        textBlueButton="VOLTAR PARA CONTRATOS"
        textBack="ALTERAR NOVAMENTE"
        handleNavigate={() => history.push('/contract/list')}
        handleNavigateToList={() => setModalUpdate(false)}
      />
      <Atom.Container variant="main">
        <Atom.Form
          onSubmit={
            update ? handleSubmit(onUpdateSubmit) : handleSubmit(onSubmit)
          }
        >
          <Organisms.HeaderCreate
            title={update ? 'Atualizar Contratos' : 'Contratos'}
            searchLabel="Buscar empresas..."
            onClickCancel={handleCancel}
          />
          <Atom.Container
            variant="create"
            justifyContent="flex-start"
            mt="MEDIUM"
          >
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                name="descricao_contrato"
                register={register}
                percent={100}
                label="Objeto"
                placeholder="Objeto do contato"
                error={errors?.descricao_contrato?.message}
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                withMask
                mask="99/99/9999"
                name="data_inicio"
                register={register}
                percent={15}
                label="Vigência De"
                placeholder="00/00/0000"
                error={errors?.data_inicio?.message}
              />
              <Molecules.InputForm
                withMask
                mask="99/99/9999"
                name="data_fim"
                register={register}
                percent={15}
                label="Vigência Até"
                placeholder="00/00/0000"
                error={errors?.data_fim?.message}
              />
              <Molecules.InputForm
                percent={15}
                label="Número"
                name="numero_contrato"
                register={register}
                placeholder="17/05"
                error={errors?.numero_contrato?.message}
              />
              <Molecules.Picker
                percent={47.5}
                label="Empresa"
                name="empresa"
                register={register}
                options={companiesOptions}
                placeholder="Selecione a empresa"
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.Picker
                percent={48}
                label="Fiscal Responsável"
                name="fiscal"
                register={register}
                options={fiscalsOptions?.data}
                placeholder="Selecione o fiscal responsável"
              />
              <Molecules.Picker
                percent={48}
                label="Fiscal Substituto"
                name="fiscal_substituto"
                register={register}
                options={fiscalsOptions?.data}
                placeholder="Selecione o fiscal substituto"
              />
            </Atom.Container>
            <Atom.Container variant="divider" />
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                percent={45}
                label="Conta Vinculada"
                placeholder="Número da conta vinculada"
                name="conta_vinculada"
                register={register}
              />
              <Molecules.InputForm
                percent={52}
                label="Processo"
                placeholder="Número do processo"
                name="processo"
                register={register}
              />
            </Atom.Container>
            <Atom.Container variant="divider" />
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                percent={18}
                maxLength={5}
                label="13º Salario(%)"
                placeholder="0.00%"
                name="decimo_terceiro"
                register={register}
              />
              <Molecules.InputForm
                percent={18}
                maxlength={5}
                label="Férias + 1/3(%)"
                placeholder="0.00%"
                name="ferias_adicionais"
                register={register}
              />
              <Molecules.InputForm
                percent={18}
                maxlength={5}
                label="Multa do FGTS (%)"
                placeholder="0.00%"
                name="recisao_fgts"
                register={register}
              />
              <Molecules.InputForm
                percent={18}
                maxlength={5}
                label="Encargos"
                placeholder="0.00%"
                name="encargos"
                register={register}
              />
              <Molecules.TextForm
                percent={18}
                label="Total"
                infoText={total === 0 ? '0.00%' : `${total}%`}
              />
            </Atom.Container>
          </Atom.Container>
        </Atom.Form>
      </Atom.Container>
    </>
  );
};

export default CreateContract;
