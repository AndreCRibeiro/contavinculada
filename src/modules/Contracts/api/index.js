import api from '../../../services/api';

const ContractsEndpoints = {
  postContracts: (data) => api.post('/contratos', data),
  updateContract: (data) => api.put(`/contratos/${data.id}`, data),
  getCompanies: () => api.get('/listEmpresas'),
};

export default ContractsEndpoints;
