export const label = [
  {
    value: 'Contrato',
    width: '7%',
  },
  {
    value: 'Empresa',
    width: '20%',
  },
  {
    value: 'Objeto',
    width: '47%',
  },
  {
    value: 'Início',
    width: '8%',
  },
  {
    value: 'Fim',
    width: '8%',
  },
];
