import * as yup from 'yup';

export const schema = yup.object().shape({
  descricao_contrato: yup
    .string()
    .required('Descrição do contrato é obrigatório'),
  data_inicio: yup.string().required('Data de início é obrigatório'),
  data_fim: yup.string().required('Data de encerramento é obrigatório'),
  numero_contrato: yup.string().required('Número do contrato é obrigatório'),
  decimo_terceiro: yup.string().required('13º Salário é obrigatório'),
  ferias_adicionais: yup.string().required('Férias + 1/3 é obrigatório'),
  recisao_fgts: yup.string().required('Multa do FGTS é obrigatório'),
  encargos: yup.string().required('Encargos é obrigatório'),
  processo: yup.string().required('Número do processo é obrigatório'),
  conta_vinculada: yup
    .string()
    .required('Número da conta vinculada é obrigatório'),
});
