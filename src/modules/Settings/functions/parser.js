import { format } from 'date-fns';

export default (data) => {
  return data?.map((item) => ({
    selected: false,
    columns: [
      {
        value: item.id,
        width: '4%',
        type: 'id',
      },
      {
        value: item.photo_url,
        width: '6%',
        type: 'image',
      },
      {
        value: item.name,
        width: '20%',
        type: 'text',
      },
      {
        value: item.type,
        width: '13%',
        type: 'text',
      },
      {
        value: item.email,
        width: '20%',
        type: 'text',
      },
      {
        value: item.matricula,
        width: '10%',
        type: 'text',
      },
      {
        value: format(new Date(item.createdAt), 'dd/MM/yyyy'),
        width: '15%',
        type: 'text',
      },
    ],
  }));
};
