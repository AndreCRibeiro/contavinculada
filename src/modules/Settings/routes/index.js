import React from 'react';
import { WrapContainer } from '../../../components/Organisms';
import Route from '../../../routes/Route';

import ListUsers from '../pages/ListUsers';
import CreateUsers from '../pages/CreateUsers';

const Routes = () => {
  return (
    <>
      <Route
        isPrivate
        path="/settings/list-users"
        exact
        component={() => (
          <WrapContainer>
            <ListUsers />
          </WrapContainer>
        )}
      />
      <Route
        isPrivate
        path="/settings/create-users"
        exact
        component={() => (
          <WrapContainer>
            <CreateUsers />
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
