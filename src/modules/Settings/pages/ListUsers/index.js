import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import parser from '../../functions/parser';
import { label } from '../../mocks';
import { useTable, useListFetch } from '../../../../hooks';
import * as Atom from '../../../../components/Atoms';
import * as Organisms from '../../../../components/Organisms';

const ListUsers = () => {
  const history = useHistory();
  const {
    data,
    error,
    offset,
    loading,
    numberPages,
    nextPage,
    prevPage,
  } = useListFetch('/users', 12);

  const {
    dataTable,
    selectAll,
    handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(parser(data?.data) || [], offset);

  const handleAdd = useCallback(() => history.push('/settings/create-users'), [
    history,
  ]);

  return (
    <Atom.Container variant="main">
      <Organisms.HeaderList
        title="Permissões"
        searchLabel="Buscar empresa..."
        onClickAdd={handleAdd}
        onClickExclude={handleDelete}
      />
      {loading ? (
        <Atom.Loader size={35} width="100%" />
      ) : (
        <Organisms.Error error={error}>
          <Organisms.Tables
            labels={label}
            data={dataTable}
            selectAll={selectAll}
            onClickSelectAll={handleSelectAll}
            onClickSelectItem={handleSelectItem}
          />
        </Organisms.Error>
      )}
      <Organisms.FooterList
        pages={numberPages}
        currentPage={offset}
        onNextPage={nextPage}
        onPrevPage={prevPage}
      />
    </Atom.Container>
  );
};

export default ListUsers;
