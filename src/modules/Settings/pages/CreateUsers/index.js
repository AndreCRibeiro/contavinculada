/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';
// logic
import Endpoints from '../../api';
import { useLazyFetch } from '../../../../hooks';
// ui
import permissions from '../../mocks/permission';
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const schema = yup.object().shape(
  {
    name: yup.string().required('nome é obrigatorio'),
    email: yup
      .string()
      .email('e-mail invalido')
      .required('e-mail é obrigatorio'),
    matricula: yup
      .string() /*
      .matches(/^_?([^_]+)_?$/, 'matricula tem 7 caracteres!')
      .min(6, 'matricula tem 7 caracteres') */
      .required(),
  },
  [['_']]
);

const CreateUsers = () => {
  const history = useHistory();
  const [fetch, { response }] = useLazyFetch(Endpoints.postCreateUser);
  const [show, setShow] = useState(false);

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (form) => fetch(form);

  const handleCancel = useCallback(() => history.push('/settings/list-users'), [
    history,
  ]);

  useEffect(() => {
    if (response) {
      setShow(true);
      reset();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response]);

  return (
    <>
      {show && (
        <Molecules.Modal
          show={show}
          handleCloseModal={() => setShow(false)}
          userModal
          textModal
          showProp
          img="SUCCESS"
          variant="modalText"
          title="Novo usuário cadastrado!"
          subTitle="Anote os dados!"
          textTitle1="Nome:  "
          text1={response.name}
          textTitle2="Email:  "
          text2={response.email}
          textTitle3="Matricula: "
          text3={response.matricula}
          textTitle4="Permissão: "
          text4={response.type}
          textTitle5="Senha: "
          text5={response.password}
        />
      )}
      <Atom.Container variant="main">
        <Atom.Form onSubmit={handleSubmit(onSubmit)}>
          <Organisms.HeaderCreate
            title="Permissões"
            onClickCancel={handleCancel}
          />
          <Atom.Container
            variant="create"
            justifyContent="flex-start"
            mt="MEDIUM"
          >
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                name="name"
                register={register}
                error={errors?.name?.message}
                percent={100}
                label="Nome"
                placeholder="Nome do usuario"
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Atom.Avatar variant="createUser" />
              <Atom.Container variant="create" ml="BIG">
                <Atom.Container variant="lineCreate">
                  <Molecules.InputForm
                    name="email"
                    register={register}
                    error={errors?.email?.message}
                    percent={100}
                    label="Email"
                    placeholder="email do usuario"
                  />
                </Atom.Container>
                <Atom.Container variant="lineCreate">
                  <Molecules.Picker
                    name="type"
                    register={register}
                    percent={100}
                    label="Nivel de Permissão"
                    options={permissions}
                  />
                </Atom.Container>
                <Atom.Container variant="lineCreate">
                  <Molecules.InputForm
                    name="matricula"
                    register={register}
                    error={errors?.matricula?.message}
                    typeMask="registration"
                    // withMask
                    // mask="9999/99"
                    percent={100}
                    label="Matricula"
                    placeholder="matricula do usuario"
                  />
                </Atom.Container>
              </Atom.Container>
            </Atom.Container>
          </Atom.Container>
        </Atom.Form>
      </Atom.Container>
    </>
  );
};

export default CreateUsers;
