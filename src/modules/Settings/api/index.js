import api from '../../../services/api';

const SettingsEndpoints = {
  postCreateUser: (data) => api.post('/users', data),
};

export default SettingsEndpoints;
