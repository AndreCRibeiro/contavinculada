import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';
// logic
import Endpoints from '../../api';
import { useLazyFetch, useAuth } from '../../../../hooks';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';

const schema = yup.object().shape({
  email: yup.string().email('e-mail invalido').required('e-mail é obrigatorio'),
  password: yup.string().min(6, 'minimo 6 caracteres').required(),
});

const Login = () => {
  const { handleSuccess, handleFirstAccess } = useAuth(true);
  const [fetch, { response, loading }] = useLazyFetch(Endpoints.postSession);
  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (form) => fetch(form);

  const handleResponseSuccess = async (res) => {
    if (res?.user?.new_user) {
      handleFirstAccess(res);
      return;
    }
    if (res) {
      await handleSuccess(res);
    }
  };

  useEffect(() => {
    if (response) handleResponseSuccess(response);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response]);

  return (
    <Atom.Container variant="login">
      <Atom.Title mb="BIG">Bem-vindo ao Conta Vinculada</Atom.Title>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Atom.Box variant="login" p={18}>
          {errors?.email && (
            <Atom.Text variant="error">{errors?.email.message}</Atom.Text>
          )}
          <Atom.Input
            name="email"
            register={register}
            type="email"
            mt="XSMALL"
            mb="XXSMALL"
            p={18}
            variant="login"
            placeholder="Email"
          />
          {errors?.password && (
            <Atom.Text variant="error">{errors?.password.message}</Atom.Text>
          )}
          <Atom.Input
            name="password"
            register={register}
            variant="login"
            type="password"
            mt="XSMALL"
            mb="XXSMALL"
            p={18}
            placeholder="Senha"
          />
        </Atom.Box>
        {loading ? (
          <Atom.Loader color size={35} mt="MEDIUM" width="100%" />
        ) : (
          <Molecules.Button variant="login" mt="MEDIUM" text="ENTRAR" />
        )}
      </form>
      <Link to="/signup">
        <Molecules.SecondaryButton normal mt="BIG" text="Primeiro acesso?" />
      </Link>
      <Molecules.Modal
        img="ALERT"
        text="Empresa adicionada com sucesso"
        variant="modalAlert"
        alert
        textBlueButton="ADICIONAR CONTATO"
        secondaryTextBlueButton="para essa empresa"
        blackButton
        textBlackButton="ADICIONAR NOVA EMPRESA"
        textBack="VOLTAR PARA EMPRESAS"
      />
    </Atom.Container>
  );
};

export default Login;
