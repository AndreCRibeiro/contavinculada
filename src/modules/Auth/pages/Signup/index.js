import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import * as yup from 'yup';
// logic
import Endpoints from '../../api';
import { useLazyFetch, useAuth } from '../../../../hooks';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';

const schema = yup.object().shape({
  email: yup.string().email('e-mail invalido').required('e-mail é obrigatorio'),
  matricula: yup.string().required('matricula é obrigatorio'),
  password: yup.string().min(6, 'minimo 6 caracteres').required(),
  new_password: yup.string().min(6, 'minimo 6 caracteres').required(),
});

const Signup = () => {
  const { handleLogout } = useAuth();
  const [errorConfirm, setErroConfirm] = useState(false);
  const [fetch, { response, loading }] = useLazyFetch(
    Endpoints.putRecoveryPass
  );

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (form) => {
    if (form.new_password !== form.confirm_password) {
      setErroConfirm(true);
      return;
    }
    fetch(form);
  };

  useEffect(() => {
    if (response) handleLogout();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response]);

  return (
    <Atom.Container variant="login">
      <Atom.Title variant="normal" mb="BIG">
        Cadastre-se no Conta Vinculada
      </Atom.Title>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Atom.Box p={18}>
          {errors?.email && (
            <Atom.Text variant="error">{errors?.email?.message}</Atom.Text>
          )}
          <Atom.Input
            name="email"
            register={register}
            type="email"
            variant="login"
            mt="XSMALL"
            mb="XXSMALL"
            p={18}
            placeholder="e-mail"
          />
          {errors?.email && (
            <Atom.Text variant="error">{errors?.matricula?.message}</Atom.Text>
          )}
          <Atom.Input
            name="matricula"
            register={register}
            variant="login"
            mt="XSMALL"
            mb="XXSMALL"
            p={18}
            placeholder="matricula"
          />
          {errors?.email && (
            <Atom.Text variant="error">{errors?.password?.message}</Atom.Text>
          )}
          <Atom.Input
            name="password"
            register={register}
            variant="login"
            mt="XSMALL"
            mb="XXSMALL"
            p={18}
            placeholder="senha atual"
            type="password"
          />
          {errors?.email && (
            <Atom.Text variant="error">
              {errors?.new_password?.message}
            </Atom.Text>
          )}
          <Atom.Input
            name="new_password"
            register={register}
            type="password"
            variant="login"
            mt="XSMALL"
            mb="XXSMALL"
            p={18}
            placeholder="nova senha"
          />
          {errorConfirm && (
            <Atom.Text variant="error" mt="XXSMALL">
              as senhas não são iguais
            </Atom.Text>
          )}
          <Atom.Input
            name="confirm_password"
            register={register}
            type="password"
            variant="login"
            mt="XSMALL"
            mb="XXSMALL"
            p={18}
            placeholder="repetir nova senha"
          />
        </Atom.Box>
        {loading ? (
          <Atom.Loader color size={35} mt="MEDIUM" width="100%" />
        ) : (
          <Molecules.Button variant="login" mt="BIG" text="CADASTRAR" />
        )}
      </form>

      <Link to="/">
        <Molecules.SecondaryButton normal mt="MEDIUM" text="CANCELAR" />
      </Link>
    </Atom.Container>
  );
};

export default Signup;
