import React from 'react';
import { Link } from 'react-router-dom';

import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';

const ForgetPassword = () => {
  return (
    <Atom.Container variant="login">
      <Atom.Title mb="BIG">Esqueceu sua senha?</Atom.Title>
      <Atom.Box variant="forgot" p={18}>
        <Atom.Input
          variant="login"
          mt="XSMALL"
          mb="XXSMALL"
          p={18}
          placeholder="Email"
        />
      </Atom.Box>
      <Molecules.Button variant="login" mt="BIG" text="ENVIAR EMAIL" />
      <Link to="/">
        <Molecules.SecondaryButton normal mt="MEDIUM" text="CANCELAR" />
      </Link>
    </Atom.Container>
  );
};

export default ForgetPassword;
