import React from 'react';
// import { Route } from 'react-router-dom';
import Login from '../pages/Login';
import Signup from '../pages/Signup';
import ForgetPassword from '../pages/ForgetPassword';
import Route from '../../../routes/Route';

const Routes = () => {
  return (
    <>
      <Route path="/" exact component={() => <Login />} />
      <Route path="/signup" exact component={() => <Signup />} />
      <Route path="/forgot" component={() => <ForgetPassword />} />
    </>
  );
};

export default Routes;
