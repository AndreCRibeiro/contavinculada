import api from '../../../services/api';

const AuthEndpoints = {
  postSession: (data) => api.post('/sessions', data),
  putRecoveryPass: (data) => api.put('/recoverypass', data),
};

export default AuthEndpoints;
