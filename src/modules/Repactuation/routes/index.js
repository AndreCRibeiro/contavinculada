import React from 'react';
import { Route } from 'react-router-dom';
import { WrapContainer } from '../../../components/Organisms';

import ListRepactuation from '../pages/ListRepactuations';
import CreateRepactuation from '../pages/CreateRepactuation';

const Routes = () => {
  return (
    <>
      <Route
        path="/repactuation/list"
        exact
        component={() => (
          <WrapContainer>
            <ListRepactuation />
          </WrapContainer>
        )}
      />
      <Route
        path="/repactuation/create"
        exact
        component={() => (
          <WrapContainer>
            <CreateRepactuation />
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
