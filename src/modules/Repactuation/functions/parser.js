export default (data) => {
  return data?.map((item) => ({
    selected: false,
    columns: [
      {
        value: item.id,
        width: '5%',
        type: 'id',
      },
      {
        value: item.nome_empresa,
        width: '23%',
        type: 'text',
      },
      {
        value: item.descricao,
        width: '22%',
        type: 'text',
      },
      {
        value: item.remuneracao,
        width: '20%',
        type: 'text',
      },
      {
        value: item.remuneracao,
        width: '20%',
        type: 'text',
      },
    ],
  }));
};
