export const label = [
  {
    value: 'ID',
    width: '5%',
  },
  {
    value: 'Contrato',
    width: '23%',
  },
  {
    value: 'Mês',
    width: '22%',
  },
  {
    value: 'Proporcionalidade',
    width: '20%',
  },
  {
    value: 'Valor',
    width: '20%',
  },
];
