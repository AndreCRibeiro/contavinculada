import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const contracts = [
  {
    id: 0,
    name: '1/2020 - Copagem',
  },
  {
    id: 0,
    name: '2/2020 - Gaçom',
  },
  {
    id: 0,
    name: '3/2020 - Manobrista',
  },
];

const cargo = [
  {
    id: 0,
    name: 'Garçom',
  },
  {
    id: 0,
    name: 'Secretaria',
  },
  {
    id: 0,
    name: 'Copeira',
  },
];

const Colaborators = [
  { name: 'Srigar', id: 1 },
  { name: 'Sam1', id: 2 },
  { name: 'Srigar2', id: 3 },
  { name: 'Sam3', id: 4 },
  { name: 'Srigar4', id: 5 },
  { name: 'Sam5', id: 6 },
  { name: 'Srigar6', id: 7 },
  { name: 'Sam7', id: 8 },
  { name: 'Srigar8', id: 9 },
  { name: 'Sam9', id: 10 },
  { name: 'Srigar10', id: 11 },
  { name: 'Sam11', id: 12 },
  { name: 'Srigar12', id: 13 },
  { name: 'Sam13', id: 14 },
  { name: 'Srigar14', id: 15 },
  { name: 'Sam15', id: 16 },
  { name: 'Srigar16', id: 17 },
  { name: 'Sam17', id: 18 },
];

const CreateRepactuation = () => {
  const history = useHistory();

  const handleCancel = useCallback(() => history.push('/repactuation/list'), [
    history,
  ]);

  return (
    <Atom.Container variant="main">
      <Organisms.HeaderCreate
        title="Repactuações"
        searchLabel="Buscar empresas..."
        onClickCancel={handleCancel}
      />
      <Atom.Container variant="lineCreate">
        <Molecules.Picker
          percent={48.75}
          label="Contrato"
          options={contracts}
        />
        <Molecules.Picker percent={48.75} label="Cargo" options={cargo} />
      </Atom.Container>
      <Atom.Container variant="lineCreate">
        <Molecules.Picker
          group
          percent={100}
          label="Colaboradores"
          options={Colaborators}
          placeholderDependency="Selecione os colaboradores"
        />
      </Atom.Container>
      <Atom.Container variant="lineCreate">
        <Molecules.CalendarForm percent={48.75} label="Mês" />
        <Molecules.InputForm percent={48.75} label="Proporcionalidade" />
      </Atom.Container>
      <Atom.Container variant="lineCreate" mt="SMALL">
        <Molecules.Button text="CALCULAR" width="30%" />
        <Atom.Container />
      </Atom.Container>
      <Atom.Container variant="divider" />
      <Atom.Container variant="lineCreate">
        <Molecules.TextForm
          percent={18}
          label="13º Salario(%)"
          infoText="0.00%"
        />
        <Molecules.TextForm
          percent={18}
          label="Férias + 1/3(%)"
          infoText="0.00%"
        />
        <Molecules.TextForm
          percent={18}
          label="Multa do FTGS (%)"
          infoText="0.00%"
        />
        <Molecules.TextForm percent={18} label="Encargos" infoText="0.00%" />
        <Molecules.TextForm percent={18} label="Total" infoText="0.00%" />
      </Atom.Container>

      <Atom.Container
        variant="create"
        justifyContent="flex-start"
        mt="MEDIUM"
      />
    </Atom.Container>
  );
};

export default CreateRepactuation;
