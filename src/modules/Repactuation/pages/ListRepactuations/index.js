import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import useStore from '../../../../global/modalDelete/store';
import { useTable } from '../../../../hooks';
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const label = [
  {
    value: 'ID',
    width: '5%',
  },
  {
    value: 'Contrato',
    width: '23%',
  },
  {
    value: 'Mês ',
    width: '22%',
  },
  {
    value: 'Proporcionalidade',
    width: '20%',
  },
  {
    value: 'Valor',
    width: '20%',
  },
];

const data = [
  {
    selected: false,
    columns: [
      {
        value: '01',
        width: '5%',
        type: 'id',
      },
      {
        value: 'Contrato',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Julho 2020',
        width: '22%',
        type: 'text',
      },
      {
        value: 'Proporcionalidade',
        width: '20%',
        type: 'text',
      },
      {
        value: 'Valor',
        width: '20%',
        type: 'text',
      },
    ],
  },
  {
    selected: true,
    columns: [
      {
        value: '01',
        width: '5%',
        type: 'id',
      },
      {
        value: 'Contrato',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Agosto 2020',
        width: '22%',
        type: 'text',
      },
      {
        value: 'Proporcionalidade',
        width: '20%',
        type: 'text',
      },
      {
        value: 'Valor',
        width: '20%',
        type: 'text',
      },
    ],
  },
  {
    selected: true,
    columns: [
      {
        value: '01',
        width: '5%',
        type: 'id',
      },
      {
        value: 'Junho 2020',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Março 2020',
        width: '22%',
        type: 'text',
      },
      {
        value: 'Proporcionalidade',
        width: '20%',
        type: 'text',
      },
      {
        value: 'Valor',
        width: '20%',
        type: 'text',
      },
    ],
  },
  {
    selected: false,
    columns: [
      {
        value: '01',
        width: '5%',
        type: 'id',
      },
      {
        value: 'Contrato',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Dezembro 2020',
        width: '22%',
        type: 'text',
      },
      {
        value: 'Proporcionalidade',
        width: '20%',
        type: 'text',
      },
      {
        value: 'Valor',
        width: '20%',
        type: 'text',
      },
    ],
  },
  {
    selected: true,
    columns: [
      {
        value: '01',
        width: '5%',
        type: 'id',
      },
      {
        value: 'Contrato',
        width: '23%',
        type: 'text',
      },
      {
        value: 'Janeiro 2020',
        width: '22%',
        type: 'text',
      },
      {
        value: 'Proporcionalidade',
        width: '20%',
        type: 'text',
      },
      {
        value: 'Valor',
        width: '20%',
        type: 'text',
      },
    ],
  },
];

const ListRepactuation = () => {
  const history = useHistory();
  const {
    dataTable,
    selectAll,
    handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(data);

  const { modalDelete, reset } = useStore();

  const handleAdd = useCallback(() => history.push('/repactuation/create'), [
    history,
  ]);

  return (
    <>
      {modalDelete && (
        <Molecules.Modal
          show={modalDelete}
          img="ALERT"
          alert
          variant="modalAlert"
          title="Deseja realmente apagar"
          handleCloseModal={() => reset()}
          handleDelete={() => reset()}
        />
      )}
      <Atom.Container variant="main">
        <Organisms.HeaderList
          title="Repactuações"
          searchLabel="Buscar repactuação..."
          onClickAdd={handleAdd}
          onClickExclude={handleDelete}
        />
        <Organisms.Tables
          labels={label}
          data={dataTable}
          selectAll={selectAll}
          onClickSelectAll={handleSelectAll}
          onClickSelectItem={handleSelectItem}
        />
        <Organisms.FooterList pages={[1, 1, 1, 1, 1]} currentPage={3} />
      </Atom.Container>
    </>
  );
};

export default ListRepactuation;
