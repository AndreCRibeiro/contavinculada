import * as yup from 'yup';

export const schema = yup.object().shape({
  nome_cargo: yup.string().required('Nome do cargo é obrigatorio'),
  remuneracao: yup.string().required('Remuneração é obrigatorio'),
  descricao: yup.string(),
  contrato: yup.number().required('Contrato é obrigatório'),
  id_empresa: yup.number().required('Empresa é obrigatório'),
});
