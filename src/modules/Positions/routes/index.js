import React from 'react';
import { Route } from 'react-router-dom';
import { WrapContainer } from '../../../components/Organisms';

import ListPositions from '../pages/ListPositions';
import CreatePosition from '../pages/CreatePosition';

const Routes = () => {
  return (
    <>
      <Route
        path="/position/list"
        exact
        component={() => (
          <WrapContainer>
            <ListPositions />
          </WrapContainer>
        )}
      />
      <Route
        path="/position/create"
        exact
        component={() => (
          <WrapContainer>
            <CreatePosition />
          </WrapContainer>
        )}
      />
    </>
  );
};

export default Routes;
