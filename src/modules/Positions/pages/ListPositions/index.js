/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import useStore from '../../../../global/modalDelete/store';
import useStoreVisualize from '../../../../global/modalVisualize/store';
import { useTable, useListFetch, useAllData } from '../../../../hooks';
import api from '../../../../services/api';
import { label } from '../../mocks';

// logic
import parser from '../../functions/parser';

// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const ListPositions = () => {
  const [inputText, setInputText] = useState('');
  const [filteredList, setFilteredList] = useState([]);
  const history = useHistory();
  const {
    data,
    error,
    offset,
    loading,
    numberPages,
    nextPage,
    prevPage,
    selectPage,
  } = useListFetch('/cargos');

  const {
    selectAll,
    handleDelete,
    handleSelectAll,
    handleSelectItem,
  } = useTable(parser(data?.data) || [], offset);

  const { allDataTable } = useAllData(parser(data?.all) || [], offset);

  const { dataZustand, modalDelete, reset, edit } = useStore();
  const {
    dataZustandVisualize,
    modalVisualize,
    resetVisualize,
  } = useStoreVisualize();

  const handleAdd = useCallback(() => history.push('/position/create'), [
    history,
  ]);

  const handleEdit = (dataEdit) => {
    history.push('/position/create', { data: dataEdit });
  };

  useEffect(() => {
    if (edit) {
      handleEdit(dataZustand);
      reset();
    }
  }, [edit, dataZustand]);

  const handleDeleteItem = async (item) => {
    const response = await api.delete(`/cargos/${item.id}`);
    reset();
    if (response.status === 200) {
      toast.success('Cargo deletado com sucesso', {
        position: toast.POSITION.TOP_CENTER,
      });
    } else {
      toast.error('Erro ao deletar cargo', {
        position: toast.POSITION.TOP_CENTER,
      });
    }
  };

  useEffect(() => {
    if (inputText !== '') {
      const teste = allDataTable.filter((input) => {
        if (input.data.cargo.toLowerCase().includes(inputText.toLowerCase())) {
          return true;
        }
        return false;
      });
      setFilteredList(teste);
    } else {
      setFilteredList([]);
    }
  }, [inputText]);

  return (
    <>
      {modalDelete && (
        <Molecules.Modal
          show={modalDelete}
          img="ALERT"
          alert
          variant="modalAlert"
          title={`Deseja realmente apagar o cargo ${dataZustand.cargo} relacionado a empresa ${dataZustand.name}`}
          handleCloseModal={() => reset()}
          handleDelete={() => handleDeleteItem(dataZustand)}
        />
      )}
      {modalVisualize && (
        <Molecules.Modal
          show={modalVisualize}
          positionModal
          variant="modalText"
          textTitle1="Empresa"
          text1={dataZustandVisualize.dadosEmpresa.name}
          textTitle2="Cargo"
          text2={dataZustandVisualize.cargo}
          textTitle3="Remuneração"
          text3={`R$ ${parseFloat(dataZustandVisualize.remuneracao, 1.0)}`}
          textTitle4="Descrição"
          text4={dataZustandVisualize.descricao}
          handleCloseModal={() => resetVisualize()}
        />
      )}
      <Atom.Container variant="main">
        <Organisms.HeaderList
          title="Cargos"
          searchLabel="Buscar cargo..."
          onClickAdd={handleAdd}
          onClickExclude={handleDelete}
          onInputChange={(text) => setInputText(text)}
        />
        {loading ? (
          <Atom.Loader size={35} width="100%" />
        ) : inputText !== '' && filteredList.length === 0 ? (
          <Atom.Container variant="filterError">
            <Atom.Text variant="filterError">Nenhum cargo encontrado</Atom.Text>
          </Atom.Container>
        ) : (
          <Organisms.Error error={error}>
            <Organisms.Tables
              labels={label}
              data={
                filteredList.length !== 0 ? filteredList : parser(data?.data)
              }
              selectAll={selectAll}
              onClickSelectAll={handleSelectAll}
              onClickSelectItem={handleSelectItem}
              noSelect
            />
          </Organisms.Error>
        )}
        <Organisms.FooterList
          pages={numberPages}
          currentPage={offset}
          onNextPage={nextPage}
          onPrevPage={prevPage}
          onSelectPage={selectPage}
        />
      </Atom.Container>
    </>
  );
};

export default ListPositions;
