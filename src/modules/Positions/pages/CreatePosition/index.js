/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers';
import { schema } from '../../mocks';
import { formatReal } from '../../../../utils';
// logic
import PositionEndpoints from '../../api';
import { useLazyFetch, useListPickerFetch } from '../../../../hooks';
// ui
import * as Atom from '../../../../components/Atoms';
import * as Molecules from '../../../../components/Molecules';
import * as Organisms from '../../../../components/Organisms';

const CreatePosition = () => {
  const history = useHistory();

  const [companiesOptions, setcompaniesOptions] = useState();
  const [contractOptions, setContractOptions] = useState();
  const [modal, setModal] = useState(false);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [update, setUpdate] = useState(false);

  const [fetch, { response }] = useLazyFetch(PositionEndpoints.postPosition);
  const [updateReq, { response: updateResponse }] = useLazyFetch(
    PositionEndpoints.updatePosition
  );

  console.log(fetch);

  const { register, handleSubmit, errors, reset, watch, setValue } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      id_empresa: history.location?.state
        ? history.location?.state.data.id_empresa
        : null,
      contrato: history.location?.state
        ? history.location?.state.data.contrato
        : null,
      nome_cargo: history.location?.state
        ? history.location?.state.data.nome_cargo
        : null,
      remuneracao: history.location?.state
        ? formatReal(history.location?.state.data.remuneracao)
        : null,
      descricao: history.location?.state
        ? history.location?.state.data.descricao
        : null,
    },
  });

  const { data: dataCompanies } = useListPickerFetch('/listEmpresas');
  const { data: dataContracts } = useListPickerFetch(
    `/contratos?empresa=${watch('id_empresa')}`
  );

  useEffect(() => {
    if (dataCompanies) {
      setcompaniesOptions(dataCompanies);
    }
  }, [dataCompanies]);

  useEffect(() => {
    if (dataContracts) {
      setContractOptions(dataContracts);
    }
  }, [dataContracts]);

  const onSubmit = (form) => {
    fetch({
      ...form,
    });
  };

  const onUpdateSubmit = (form, { id = history.location.state.data.id }) => {
    updateReq({
      ...form,
      id,
      remuneracao: parseFloat(form.remuneracao),
    });
  };

  console.log('Teste', watch('remuneracao'));
  console.log(watch('nome_cargo'));

  const handleChangeFormatUpdate = (e, a, b) => {
    const teste = formatReal(history.location?.state.data.remuneracao);
    setValue('remuneracao', teste);
  };

  const handleChange = (e) => {
    const teste = e.target.value
      .replace(/R\$/g, '')
      .replace(/\./g, '')
      .replace(/,/g, '.');
    const transformIntoNumber = (parseFloat(teste) * 10).toFixed(2);
    setValue('remuneracao', transformIntoNumber);
  };

  useEffect(() => {
    register('remuneracao'); // custom register Antd input
  }, [register]);

  useEffect(() => {
    if (history.location?.state) {
      console.log('entrei');
      handleChangeFormatUpdate();
      setUpdate(true);
    }
  }, []);

  useEffect(() => {
    if (response) {
      setModal(true);
      reset(
        {},
        {
          errors: false,
        }
      );
    }
    if (updateResponse) {
      setModalUpdate(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response, updateResponse]);

  const handleCancel = useCallback(() => history.push('/position/list'), [
    history,
  ]);

  return (
    <>
      <Molecules.Modal
        show={modal}
        img="SUCCESS"
        confirm
        blackButton
        variant="modalSuccess"
        title="Cargo adicionado com sucesso"
        textBlueButton="ADICIONAR NOVO COLABORADOR"
        textBlackButton="ADICIONAR NOVO CARGO"
        textBack="VOLTAR PARA CARGOS"
        handleCloseModal={() => setModal(false)}
        handleNavigate={() => history.push('/collaborator/create')}
        handleNavigateToList={() => history.push('/position/list')}
      />
      <Molecules.Modal
        show={modalUpdate}
        img="SUCCESS"
        confirm
        variant="modalSuccess"
        title="Cargo alterado com sucesso"
        textBlueButton="VOLTAR PARA CARGOS"
        textBack="ALTERAR NOVAMENTE"
        handleNavigate={() => history.push('/position/list')}
        handleNavigateToList={() => setModalUpdate(false)}
      />
      <Atom.Container variant="main">
        <Atom.Form
          onSubmit={
            update ? handleSubmit(onUpdateSubmit) : handleSubmit(onSubmit)
          }
        >
          <Organisms.HeaderCreate
            title={update ? 'Atualizar Cargo' : 'Cargos'}
            searchLabel="Buscar empresas..."
            onClickCancel={handleCancel}
          />
          <Atom.Container
            variant="create"
            justifyContent="flex-start"
            mt="MEDIUM"
          >
            <Atom.Container variant="lineCreate">
              <Molecules.Picker
                percent={45}
                label="Empresa"
                name="id_empresa"
                register={register}
                options={companiesOptions}
                placeholderDependency="Carregando..."
              />
              <Molecules.Picker
                percent={50}
                label="Contrato"
                name="contrato"
                register={register}
                options={contractOptions?.contratos}
                placeholderDependency={
                  contractOptions ? 'Carregando . . .' : 'Selecione uma empresa'
                }
              />
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                percent={62.5}
                label="Cargo"
                name="nome_cargo"
                register={register}
                placeholder="Nome do cargo"
                error={errors?.nome_cargo?.message}
              />
              {update ? (
                <Molecules.InputReal
                  name="remuneracao"
                  label="Remuneração"
                  error={errors?.remuneracao?.message}
                  percent={35}
                  placeholder="Teste"
                  onChange={handleChange}
                  value={formatReal(history.location?.state.data.remuneracao)}
                />
              ) : (
                <Molecules.InputReal
                  name="remuneracao"
                  label="Remuneração"
                  error={errors?.remuneracao?.message}
                  percent={35}
                  placeholder="Teste"
                  onChange={handleChange}
                />
              )}
            </Atom.Container>
            <Atom.Container variant="lineCreate">
              <Molecules.InputForm
                percent={100}
                multiLine
                label="Descrição"
                name="descricao"
                register={register}
                placeholder="Descrição do cargo"
                error={errors?.descricao?.message}
              />
            </Atom.Container>
          </Atom.Container>
        </Atom.Form>
      </Atom.Container>
    </>
  );
};

export default CreatePosition;
