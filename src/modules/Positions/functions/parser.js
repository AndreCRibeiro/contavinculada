import { formatReal } from '../../../utils';

export default (data) => {
  return data?.map((item) => ({
    selected: false,
    data: {
      ...item,
      name: `${item.dadosEmpresa.name ? item.dadosEmpresa.name : 'Não informado'
        }`,
      cargo: item.nome_cargo,
    },
    columns: [
      {
        value: item.nome_cargo,
        width: '15%',
        type: 'text',
      },
      {
        value: `${item.dadosEmpresa.name ? item.dadosEmpresa.name : 'Não informado'
          } `,
        width: '22%',
        type: 'text',
      },

      {
        value: item.descricao,
        width: '44%',
        type: 'text',
      },
      {
        value: `R$ ${formatReal(item.remuneracao)}`,
        width: '10%',
        type: 'text',
      },
    ],
  }));
};
