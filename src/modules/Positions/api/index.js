import api from '../../../services/api';

const PositionEndpoints = {
  postPosition: (data) => api.post('/cargos', data),
  updatePosition: (data) => api.put(`/cargos/${data.id}`, data),
  getCompanies: () => api.get('/listEmpresas'),
  getContracts: () => api.get('/listContratos'),
};

export default PositionEndpoints;
