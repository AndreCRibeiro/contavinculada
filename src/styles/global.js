import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    background: #252525;
    font-family: 'Montserrat-Regular';
    -webkit-font-smoothing: antialiased;
  }
`;
