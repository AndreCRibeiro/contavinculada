import EMPRESA_BLACK from './Icons/empresa_black.png';
import EMPRESA_WHITE from './Icons/empresa_white.png';
import ALIQUOTA_BLACK from './Icons/aliquota_black.png';
import ALIQUOTA_WHITE from './Icons/aliquota_white.png';
import CARGOS_BLACK from './Icons/cargos_black.png';
import CARGOS_WHITE from './Icons/cargos_white.png';
import COLABORADOR_BLACK from './Icons/colaborador_black.png';
import COLABORADOR_WHITE from './Icons/colaborador_white.png';
import CONTRATOS_BLACK from './Icons/contratos_black.png';
import CONTRATOS_WHITE from './Icons/contratos_white.png';
import DEPOSITO_BLACK from './Icons/deposito_black.png';
import DEPOSITO_WHITE from './Icons/deposito_white.png';
import MOVIMENTACAO_BLACK from './Icons/movimentacao_black.png';
import MOVIMENTACAO_WHITE from './Icons/movimentacao_white.png';
import REPACTUACAO_BLACK from './Icons/repactuacao_black.png';
import REPACTUACAO_WHITE from './Icons/repactuacao_white.png';
import RETIRADA_BLACK from './Icons/retirada_black.png';
import RETIRADA_WHITE from './Icons/retirada_white.png';
import SETTIGNS_BLACK from './Icons/settigns_black.png';
import SETTIGNS_WHITE from './Icons/settings_white.png';
import LOGOUT from './Icons/logout.png';
import DOWN_ARROW from './Icons/down-arrow.png';

import DONE_WHITE from './Icons/done_white.png';
import OPTIONS from './Icons/optionspng.png';
import ARROW_WHITE from './Icons/arrow_left.png';
import ARROW_BLACK from './Icons/arrow_black.png';
import EDIT from './Icons/edit.png';
import DELETE from './Icons/delete.png';
import VISIBILITY from './Icons/visibility.png';

import DEFAULT_PERFIL from './Img/profile_default.png';
import ALERT from './Img/alert.png';
import SUCCESS from './Img/success.png';

export const Icons = {
  EMPRESA_BLACK,
  EMPRESA_WHITE,
  ALIQUOTA_BLACK,
  ALIQUOTA_WHITE,
  CARGOS_BLACK,
  CARGOS_WHITE,
  COLABORADOR_BLACK,
  COLABORADOR_WHITE,
  CONTRATOS_BLACK,
  CONTRATOS_WHITE,
  DEPOSITO_BLACK,
  DEPOSITO_WHITE,
  MOVIMENTACAO_BLACK,
  MOVIMENTACAO_WHITE,
  REPACTUACAO_BLACK,
  REPACTUACAO_WHITE,
  RETIRADA_BLACK,
  RETIRADA_WHITE,
  SETTIGNS_BLACK,
  SETTIGNS_WHITE,
  DONE_WHITE,
  OPTIONS,
  ARROW_BLACK,
  ARROW_WHITE,
  EDIT,
  DELETE,
  VISIBILITY,
  LOGOUT,
  DOWN_ARROW,
};

export const Imgs = {
  DEFAULT_PERFIL,
  ALERT,
  SUCCESS,
};
