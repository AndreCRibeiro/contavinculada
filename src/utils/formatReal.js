export function formatReal(valor) {
  let inteiro = null;
  let decimal = null;
  let c = null;
  let j = null;
  const aux = [];
  valor = `${valor}`;
  c = valor.indexOf('.', 0);
  // encontrou o ponto na string
  if (c > 0) {
    // separa as partes em inteiro e decimal
    inteiro = valor.substring(0, c);
    decimal = valor.substring(c + 1, valor.length);
  } else {
    inteiro = valor;
  }

  // pega a parte inteiro de 3 em 3 partes
  for (j = inteiro.length, c = 0; j > 0; j -= 3, c++) {
    aux[c] = inteiro.substring(j - 3, j);
  }

  // percorre a string acrescentando os pontos
  inteiro = '';
  for (c = aux.length - 1; c >= 0; c--) {
    inteiro += `${aux[c]}.`;
  }
  // retirando o ultimo ponto e finalizando a parte inteiro

  inteiro = inteiro.substring(0, inteiro.length - 1);

  decimal = parseFloat(decimal);
  if (isNaN(decimal)) {
    decimal = '00';
  } else {
    decimal = `${decimal}`;
    if (decimal.length === 1) {
      decimal += '0';
    }
  }
  valor = `${inteiro},${decimal}`;
  return valor;
}

export function formatReal2(numero) {
  const num = numero.toFixed(2).split('.');
  num[0] = num[0].split(/(?=(?:...)*$)/).join('.');
  return num.join(',');
}

export const formated = (value, cifrao) => {
  const currency = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
  });

  const c = currency.format(value);

  if (cifrao) {
    return value >= 0 ? c : `-${c}`;
  }

  const str = c.split('$');

  return value >= 0 ? str[1] : `-${str[1]}`;
};

export const formatedFloat = (value) => {
  const containVirgula = /([,])/g;
  const c = containVirgula.test(value);
  if (c) {
    const decimal = parseFloat(
      value.replace('.', '').replace(',', '.')
    ).toFixed(2);
    return decimal;
  }
  const decimal = parseFloat(value).toFixed(2);
  return decimal;
};
