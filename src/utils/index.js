export * from './formatReal';
export * from './formatRubrica';

export const testNumber = (number) => (isNaN(number) ? 0.0 : number);
