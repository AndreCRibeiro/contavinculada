export function formatString(string) {
  switch (string) {
    case 'rescisao':
      return 'Rescisão';
    case '13salario':
      return '13º Salário';
    case 'ferias':
      return 'Férias';
    default:
      return '---';
  }
}
